package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class TableUserApplyCreditEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private long buyIn;
    private long maxBuyIn;
    private String masterId;

    public TableUserApplyCreditEvent(String tableId, String userId, long buyIn, long maxBuyIn, String masterId) {
        this.tableId = tableId;
        this.userId = userId;
        this.buyIn = buyIn;
        this.maxBuyIn = maxBuyIn;
        this.masterId = masterId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    public String getTableId() {
        return tableId;
    }

    public String getUserId() {
        return userId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public long getMaxBuyIn() {
        return maxBuyIn;
    }

    public String getMasterId() {
        return masterId;
    }

    @Override
    public String event() {
        return "userApplyBuyIn";
    }

    public static class Response implements Serializable {
        private int status;
        private String message;

        public Response(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
