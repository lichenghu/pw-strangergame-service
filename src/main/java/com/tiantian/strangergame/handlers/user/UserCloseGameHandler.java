package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.user.TableUserCloseGameEvent;
import com.tiantian.strangergame.akka.event.GameOver;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.manager.model.UserChips;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
/**
 *
 */
public class UserCloseGameHandler implements EventRetHandler<TableUserCloseGameEvent> {

    @Override
    public Object handler(TableUserCloseGameEvent event, ActorRef self,
                        UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus != null) {
            if (StringUtils.isNotBlank(tableStatus.getStatus()) && !GameStatus.READY.name().equalsIgnoreCase(tableStatus.getStatus())) {
                return new TableUserCloseGameEvent.Response(-1, "游戏正在进行中不能关闭");
            }
        }
        Set<String> everSitUsers = null;
        Collection<String> onlineUsers = null;
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser != null) {
            Map<String, String> joinTableUserMap = tableAllUser.getJoinTableUserMap();
            if (joinTableUserMap != null && joinTableUserMap.size() >= 2) {
                return new TableUserCloseGameEvent.Response(-2, "游戏里面有玩家在游戏中不能关闭");
            }
            onlineUsers = tableAllUser.getOnlineTableUserIds();
            everSitUsers = tableAllUser.userEverJoinSet();
            tableAllUser.delSelf();
        }
        if (tableStatus != null) {
            tableStatus.delSelf();
        }

        // 清理UserChips等数据
        if (everSitUsers != null && everSitUsers.size() > 0) {
            for (String userId : everSitUsers) {
                UserChips userChips = UserChips.load(userId, tableId);
                if (userChips != null) {
                    userChips.delSelf(tableId);
                }
                TableUser tableUser = TableUser.load(userId);
                if (tableUser != null) {
                    tableUser.delSelf(tableId);
                }
            }
        }
        JSONObject $object = new JSONObject();
        $object.put("message", "房主关闭了房间");
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers($object, GameEventType.CLOSE_GAME, onlineUsers, id, tableId);
        // 发送结束信息 销毁actor
        context.parent().tell(new GameOver(tableId), ActorRef.noSender());
        return new TableUserCloseGameEvent.Response(0, "关闭成功");
    }
}
