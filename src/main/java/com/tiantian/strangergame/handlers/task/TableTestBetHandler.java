package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.*;

/**
 *
 */
public class TableTestBetHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        String sitNum = jsonObject.getString("sitNum");
        String userId = jsonObject.getString("userId");
        String operateCount = jsonObject.getString("operateCount");
        String pwdParam = jsonObject.getString("pwd");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        if (!tableStatus.checkPwdAndSit(pwdParam, sitNum)) {
            return;
        }
        //  牌局已经不是当前牌局
        if (!inningId.equals(tableStatus.getInningId())) {
            return;
        }
        //  牌局已经结束
        if (GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }

        String currentBet = tableStatus.getCurrentBet();
        // 已经当前需要下注的不是任务中的人
        if (currentBet == null || !currentBet.equals(sitNum)) {
            return;
        }
        // 判断是否弃牌
        Map<String, String> allNotFolds = tableStatus.allNotFoldSitNumsAndCards();
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        String status = null;
        // 没有弃牌
        if (allNotFolds != null && allNotFolds.containsKey(sitNum)) {
            String tableUserId = tableAllUser.getGamingSitUserMap().get(sitNum);
            // 位置上的人不是当前桌子座位玩家，可能任务的玩家已经退出了
            if (tableUserId == null || !tableUserId.equals(userId)) {
                return;
            }
            TableUser tableUser = TableUser.load(userId);
            String userTableId = tableUser.getTableId();
            String userSitNum = tableUser.getSitNum();
            String userOperateCount = tableUser.getOperateCount();
            if (StringUtils.isBlank(userOperateCount)) {
                tableUser.setOperateCount("1");
            }
            if (!tableId.equals(userTableId)) {
                return;
            }
            if (!sitNum.equals(userSitNum)) {
                return;
            }
            // 玩家当前操作序号大于检测时候的版本号说明已经玩家操作过一次，该次检测跳过
            if (userOperateCount != null && (Integer.parseInt(userOperateCount) > Integer.parseInt(operateCount))) {
                return;
            }
            // 检测是否需要延时
            String hasAddTimes = tableUser.getHasAddTimes();
            if ("1".equalsIgnoreCase(hasAddTimes)) {
                tableUser.setHasAddTimes("0");
                tableUser.save();
                // 重新提交任务
                try {
                    RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), GameConstants.BET_DELAYER_TIME);
                } catch (TException e) {
                    e.printStackTrace();
                }
                return;
            }

            // 增加玩家的操作计数
            tableUser.addOneOperateCount();
            // 增加玩家的强制操作（当强制玩家2次后，玩家自动站起）
            String oldNotOpOperateCount = tableUser.getNotOperateCount();

            UserChips currUserChips = UserChips.load(userId, tableUser.getTableId());
            String[] currOps = tableStatus.getUserCanOps(userId, userSitNum);

            char op = currOps[0].charAt(1);
            // 可以让牌
            if ('1' == op) {
                status = GameEventType.CHECK;
            } else {
                status = GameEventType.FOLD;
            }
            tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);

            if (StringUtils.isBlank(oldNotOpOperateCount)) {
                oldNotOpOperateCount = "0";
            }
            // 增加玩家的操作计数
            tableUser.setNotOperateCount((Integer.parseInt(oldNotOpOperateCount) + 1) + "");
            tableUser.setBetStatus(status);
            tableUser.initTotalSecs(); // 重置
            if (Integer.parseInt(tableUser.getNotOperateCount()) >= GameConstants.MAX_NOT_OPERATE_COUNT) {
                // 玩家弃牌
                tableStatus.setSitBetStatusIfExistsNotFlush(sitNum, GameEventType.FOLD);
                status = GameEventType.FOLD;
                // 强制玩家站起
                forceStandup(userId, tableId, sitNum, tableAllUser, tableStatus.getInningId(), tableStatus.getIndexCount());
            } else {
                // 通知其他人 该玩家的下注状态  看牌, 弃牌
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(sitNum));
                object.put("left_chips", currUserChips.getChips());
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableId);
            }
            // 刷新setProperty 数据
            tableUser.save();
        }

        //测试游戏状态: 是否进行下流程: flop turn river fish
        Set<String> canBetSits = tableStatus.canBetSits();
        String maxBetSitNum = tableStatus.getMaxBetSitNum();

        String currentStatus = tableStatus.getStatus();


        GameRecord gameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (gameRecord == null) {
            gameRecord = new GameRecord();
        }
        List<GameRecord.Progress> progresses = gameRecord.getProgresses();
        progresses.add(GameRecord.Progress.create(status, sitNum,  "nil",
                System.currentTimeMillis() - gameRecord.getStartTime()));
        RecordUtils.restLastedRecord(gameRecord, tableId);

        GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, currentStatus, maxBetSitNum, sitNum);
        if (nextStatus != null) {
            //完成一轮的下注结算，分池
            tableStatus.roundBetEnd();
            boolean hasSafe = false;
            if (status != null && status.equalsIgnoreCase(GameEventType.FOLD)) {
                boolean needShow = tableStatus.needShowAllCards();
                if (needShow) { // 需要显示所有的手牌
                    List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                    JSONObject object2 = new JSONObject();
                    object2.put("inner_id", tableStatus.getInningId());
                    object2.put("inner_cnt", tableStatus.getIndexCount());
                    object2.put("all_cards", cardsList);
                    String id2 = UUID.randomUUID().toString().replace("-", "");
                    GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                    // 保险
                    hasSafe = GameUtils.checkSafe(tableStatus, tableAllUser);
                }
            }
            // 保存数据
            tableStatus.save();
            // 通知玩家的池信息
            GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
            // 触发下一轮的发牌事件
            GameUtils.triggerNextStatusTask(nextStatus, tableId, inningId, hasSafe);
            return;
        }

        // 触发下一个玩家的30s定时检测下注任务
        // 获取下个下注的玩家座位号
        String nextBetSitNum = GameUtils.getNextBetSitNum(canBetSits, sitNum, maxBetSitNum);
        String nextBetUserId = tableAllUser.getGamingSitUserMap().get(nextBetSitNum);
        TableUser nextTableUser = TableUser.load(nextBetUserId);

        String nextUserOperateCount = nextTableUser.getOperateCount();
        if (StringUtils.isBlank(nextUserOperateCount)) {
            nextUserOperateCount = "1";
            nextTableUser.setOperateCount(nextUserOperateCount);
            nextTableUser.save();
        }
        // 设置当前需要下注的人的座位号
        tableStatus.setCurrentBet(nextBetSitNum);
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");
        String pwd = tableStatus.randomPwd();
        tableStatus.save();

        GameRecord mttGameRecord2 = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (mttGameRecord2 == null) {
            mttGameRecord2 = new GameRecord();
        }
        List<GameRecord.Progress> progresses2 = mttGameRecord2.getProgresses();
        progresses2.add(GameRecord.Progress.create("bet", sitNum,  "nil",
                System.currentTimeMillis() - mttGameRecord2.getStartTime()));
        RecordUtils.restLastedRecord(mttGameRecord2, tableId);

        // 通知玩家下注
        JSONObject nextObject = new JSONObject();
        nextObject.put("inner_id", tableStatus.getInningId());
        nextObject.put("inner_cnt", tableStatus.getIndexCount());
        nextObject.put("uid", nextBetUserId);
        nextObject.put("sn", Integer.parseInt(nextBetSitNum));
        nextObject.put("t", GameConstants.BET_DELAYER_TIME / 1000);
        String[] ops = tableStatus.getUserCanOps(nextBetUserId, nextBetSitNum);
        nextObject.put("c_b", StringUtils.join(ops, ",")); // 玩家可以操作的选项
        nextObject.put("pwd", pwd);
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUser(nextObject, GameEventType.BET, nextBetUserId, tableId);
        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", tableStatus.getIndexCount());
        otherNextObject.put("uid", nextBetUserId);
        otherNextObject.put("sn", Integer.parseInt(nextBetSitNum));
        otherNextObject.put("t", GameConstants.BET_DELAYER_TIME / 1000);
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", pwd);
        userIds.remove(nextBetUserId);
        // 如果不捕获异常则会终端下一个任务
        String newId = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableId);

        int newOperateCount = Integer.parseInt(nextUserOperateCount);
        // 触发30s的检测任务 大盲注下一位的状态 如果已经操作则跳过, preFlop
        GameUtils.triggerBetTestTask(tableId, inningId, nextBetSitNum, nextBetUserId, newOperateCount, pwd);
    }

    // 强制站起
    private void forceStandup(String userId, String tableId, String sitNum, TableAllUser tableAllUser,
                              String innerId, String innerCnt) {
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        TableUser tableUser = TableUser.load(userId);
        if (!tableId.equalsIgnoreCase(tableUser.getTableId())) {
            return;
        }

        tableAllUser.userStandUp(tableUser.getSitNum(), tableUser.getUserId());
        tableUser.forceStandUp();

        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("uid", userId);
        object.put("sn", Integer.parseInt(sitNum));
        String id = UUID.randomUUID().toString().replace("-", "");

        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUsers(object, GameEventType.FOLD, userIds, id, tableId);

        // 通知玩家离开
        JSONObject object1 = new JSONObject();
        object1.put("inner_id", innerId);
        object1.put("inner_cnt", innerCnt);
        object1.put("uid", userId);
        object1.put("sn", Integer.parseInt(sitNum));
        object1.put("reason",  "");
        String id1 = UUID.randomUUID().toString().replace("-", "");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUsers(object1, GameEventType.STAND_UP, userIds, id1, tableId);

    }
}
