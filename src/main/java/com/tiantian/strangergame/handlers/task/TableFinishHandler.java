package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.akka.event.GameOver;
import com.tiantian.strangergame.akka.event.TableDbUpdateCntEvent;
import com.tiantian.strangergame.akka.event.UserUpdateStaticesEvent;
import com.tiantian.strangergame.data.mongodb.MGDatabase;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.manager.texas.Poker;
import com.tiantian.strangergame.manager.texas.PokerManager;
import com.tiantian.strangergame.manager.texas.PokerOuts;
import com.tiantian.strangergame.statistic.UserStatisticManager;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

/**
 *
 */
public class TableFinishHandler  implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableFinishHandler.class);
    private static final String GROUPS_TABLE = "stranger_groups";
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        // 回合ID
        //获取桌子玩家人数
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null || tableStatus.isNull()) {
            return;
        }
        if (StringUtils.isBlank(tableStatus.getInningId())) {
            // 玩家在下注的时候站起或退出导致该情况
            LOG.info("TableStatus is NULL" + JSON.toJSONString(tableStatus));
            return;
        }
        if (!tableStatus.getInningId().equalsIgnoreCase(inningId)) {
            return;
        }
        if(!GameStatus.D_AND_B.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.PRE_FLOP.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.FLOP.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.TURN.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                !GameStatus.RIVER.name().equalsIgnoreCase(tableStatus.getStatus())) {
            // 不是正常流程里面进入finish的
            return;
        }
        List<UserBetLog> userBetLogs = tableStatus.getUsersBetsLogList();
        //TODO 获取日志
        Map<String, String> sitBetUserIdMap = tableStatus.getSitBetUserIdMap();
        if (sitBetUserIdMap == null) {
            sitBetUserIdMap = new HashMap<>();
        }
        //初始化日志统计
        Map<String, UserStatisticManager.UserStatistic> userStatisticMap = initrStatistic(sitBetUserIdMap.values());

        Map<String, String> allUserCards = tableStatus.getUsersCards();

        Map<String, Long> allWinBetMap = new HashMap<>();
        Map<String, String> userBetRaiseLogMap = tableStatus.userBetRaiseLogMap();
        if (userBetLogs != null) {
            for (UserBetLog userBetLog : userBetLogs) {
                allWinBetMap.put(userBetLog.getUserId(), (userBetLog.getRoundChips() + userBetLog.getTotalChips()) * -1);
                /** 更新入局统计 **/
                String sitNum = null;
                // 判断是不是大盲注位，如果是则没有入局
                Set<Map.Entry<String, String>> entrySet = sitBetUserIdMap.entrySet();
                for(Map.Entry<String, String> entry : entrySet) {
                    String sit = entry.getKey();
                    String userId = entry.getValue();
                    if (userId.equalsIgnoreCase(userBetLog.getUserId())) {
                        sitNum = sit;
                        break;
                    }
                }

                if (userBetLog.getTotalChips() > 0) {
                    // 玩家筹码小于等于1个大盲
                    if(userBetLog.getTotalChips() < Long.parseLong(tableStatus.getBigBlindMoney())) {
                        continue;
                    }
                    else if (userBetLog.getTotalChips() == Long.parseLong(tableStatus.getBigBlindMoney())) {
                        // 是大盲注位
                        if (tableStatus.getBigBlindNum().equalsIgnoreCase(sitNum)) {
                            continue;
                        }
                    }
                    // 玩家没有弃牌
                    if (tableStatus.notFoldUserIds().contains(userBetLog.getUserId())) {
                        //更新摊牌
                        updateGameEndInf(userBetLog.getUserId(), userStatisticMap);
                    }
                    /** 更新入局值和牌型和平均加注 **/
                    UserStatisticManager.UserStatistic userStatistic = userStatisticMap.get(userBetLog.getUserId());
                    if (userStatistic != null) {
                        if (sitNum != null) {
                            // 大盲位不统计牌型比率
                            if(!tableStatus.getBigBlindNum().equalsIgnoreCase(sitNum)) {
                                String cards = allUserCards.get(sitNum);
                                if (cards != null) {
                                    userStatistic.setCardsType(cards);
                                }
                            }
                            if (userBetRaiseLogMap != null) {
                                String raiseStr = userBetRaiseLogMap.get(sitNum);
                                try {
                                    if (raiseStr != null) {
                                        String[] raiseList = raiseStr.split(",");
                                        double totalRaise = 0;
                                        for (String raise : raiseList) {
                                            totalRaise += Math.min(2.0d, Double.parseDouble(raise));
                                        }
                                        userStatistic.setAvgRaise(totalRaise / (double) raiseList.length);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        userStatistic.setGamingCnt(1);
                    }
                }
            }
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        // 比较牌的大小
        List<Map<String, String>> poolMaps = tableStatus.getBetPoolMapList();
        List<Map<String, Long>> resultList = new ArrayList<>();
        List<String> foldSitNumList = tableStatus.allFoldSitNumList();
        List<String> deskCards = tableStatus.getDeskCardList();
        if (poolMaps != null && poolMaps.size() > 0) {
            for (Map<String, String> poolMap : poolMaps) {
                if (poolMap.size() == 1) {
                    Map<String, Long> sitAndBetsMap = new HashMap<>();
                    Set<Map.Entry<String,String>> poolEntrySet = poolMap.entrySet();
                    Map.Entry<String,String> poolEntry = (Map.Entry<String,String>)poolEntrySet.toArray()[0];
                    long bet = Long.parseLong(poolEntry.getValue());
                    if (bet <= 0) {
                        continue;
                    }
                    sitAndBetsMap.put(poolEntry.getKey(), bet);
                    resultList.add(sitAndBetsMap);
                } else {
                    // 每个池子的下注总额
                    long bet = perTotalPoolBets(poolMap.values());
                    // 单个池子里面下注的人
                    Set<String> poolUsers = poolMap.keySet();

                    // 过滤掉弃牌的玩家
                    Map<String, String> userCards = eliminateFold(allUserCards, poolUsers, foldSitNumList);

                    // 从庄位起始到最后一位，倒序排列，排列后最后一个分的剩下的
                    List<Map.Entry<String, PokerOuts>> outsList = PokerManager.getWinnerPokerOutsList(userCards, deskCards);

                    List<String> winnerSitNum = sortWinnerList(outsList, tableStatus.getButton());

                    Map<String, Long> sitAndBetsMap = getUserBetsMap(winnerSitNum, bet, Long.parseLong(tableStatus.getSmallBlindMoney()));
                    resultList.add(sitAndBetsMap);
                }
            }
        }

        Map<String, String> notFoldCards = tableStatus.allNotFoldSitNumsAndCards();

        // 10大抽1小 规则
        String rule = null;
        // 玩家结算时获取的筹码
        Map<String, Long> userAllMap = userAllBetMap(resultList);
        // 进行玩家的结算
        balanceBets(userAllMap, sitBetUserIdMap, tableId, allWinBetMap);
        // 更新mongo日志
        updateUserWinLogInfo(tableStatus, allWinBetMap, self);

        String innerId = tableStatus.getInningId();
        String innerCount = tableStatus.getIndexCount();
        String masterId = tableStatus.getMasterId();
        float delaySec = 0.5f;
        if (StringUtils.isNotBlank(tableStatus.getNeedDelay())) {
            if("1".equalsIgnoreCase(tableStatus.getNeedDelay())) {
                delaySec += 3f;
            } else if ("2".equalsIgnoreCase(tableStatus.getNeedDelay())) {
                delaySec += 5f;
            }
            else if ("3".equalsIgnoreCase(tableStatus.getNeedDelay())) {
                delaySec += 8f;
            }
        }
        tableStatus.setCurrentBetTimes("");
        tableStatus.setStatus(GameStatus.FINISH.name());
        tableStatus.saveNotAddCnt();

        GameRecord gameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (gameRecord == null) {
            gameRecord = new GameRecord();
        }
        List<GameRecord.Progress> progresses = gameRecord.getProgresses();

        boolean gameOver = false;
        // 判断游戏时间是否结束
        if (System.currentTimeMillis() >= Long.parseLong(tableStatus.getEndTimes())) {
            gameOver = true;
        }
        /**更新统计数据**/
        UserStatisticManager.getInstance().statistic(userStatisticMap.values());

        // 通知玩家信息
        List<Map<String, Object>> userWinList = notifyUserInfo(userAllMap, resultList, tableAllUser.getOnlineTableUserIds(),
                notFoldCards, deskCards, innerId, innerCount, tableStatus.getUsersCards(),
                tableAllUser.getGamingSitUserMap(), gameOver, delaySec,userStatisticMap, sitBetUserIdMap,
                tableStatus.userSafeWinMap(), tableId ,progresses, gameRecord.getStartTime(), gameRecord);

        progresses.add(GameRecord.Progress.create("finished", "nil", "nil",
                System.currentTimeMillis() - gameRecord.getStartTime()));


        // 计算盈利
        List<UserBetLog> betLogList = tableStatus.getUsersBetsLogList();
        Map<String, Long> userSitWinMap = new HashMap<>();
        for (Map<String, Object> map : userWinList) {
            Integer sn = (Integer)map.get("sn");
            Long win = (Long) map.get("win");
            userSitWinMap.put(sn.toString(), win);
        }
        for (UserBetLog userBetLog : betLogList) {
            String sn = userBetLog.getSitNum();
            Long betChips = userBetLog.getRoundChips() + userBetLog.getTotalChips();
            Long userWin = userSitWinMap.get(sn);
            if (userWin == null) {
                userWin = 0l;
            }
            Long win = userWin - betChips;
            progresses.add(GameRecord.Progress.create("win", sn, win.toString(),
                    System.currentTimeMillis() - gameRecord.getStartTime()));
        }
        RecordUtils.restLastedRecord(gameRecord, tableId);
        // 判断游戏时间是否结束
        if (gameOver) {
            Set<String> everSitUsers = tableAllUser.userEverJoinSet();
            tableStatus.delSelf();
            tableAllUser.delSelf();
            // 清理UserChips等数据
            if (everSitUsers != null && everSitUsers.size() > 0) {
                for (String userId : everSitUsers) {
                    UserChips userChips = UserChips.load(userId, tableId);
                    if (userChips != null) {
                        userChips.delSelf(tableId);
                    }
                    TableUser tableUser = TableUser.load(userId);
                    if (tableUser != null) {
                        tableUser.delSelf(tableId);
                    }
                }
            }

            // 发送结束信息 销毁actor
            context.parent().tell(new GameOver(tableId), ActorRef.noSender());
            //
            JSONObject $object = new JSONObject();
            $object.put("group_id", tableId);
            // 发送其他玩家的消息
            GameUtils.notifyUser($object, GameEventType.LEADER_GAME_OVER, masterId, tableId);
            try {
                FriendCoreIface.instance().iface().strangerGameOver(tableId);
            } catch (TException e) {
                e.printStackTrace();
            }
            return;
        }
        if (userWinList.size() > 0) {

            delaySec += userWinList.size() * 1.5f + 2f;
        }

        long delayMills = Math.max((long)(delaySec * 1000), 5500) - GameConstants.AFTER_CHECK_CHIPS_DELAYER_TIME;
        // 发送一个延迟3s的开始, 确定庄家大小盲注任务
        jsonObject.put(GameConstants.TASK_EVENT, GameConstants.CHECK_CHIPS);
        jsonObject.put("tableId", tableId);
        jsonObject.put("masterId", tableStatus.getMasterId());
        jsonObject.put("innerId", innerId);
        jsonObject.put("innerCount", innerCount);
        try {
            RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), delayMills);
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    private  Map<String, String> eliminateFold(Map<String ,String> allUserCards, Set<String> poolUserSet, List<String> foldList) {
        Map<String, String> poolUserCards = new HashMap<>();
        for (String userSit : poolUserSet) {
            if (allUserCards.containsKey(userSit)) {
                poolUserCards.put(userSit, allUserCards.get(userSit));
            }
        }

        // 筛选出没有弃牌的 玩家座位号
        for (String foldSit : foldList) {
            poolUserCards.remove(foldSit);
        }
        return poolUserCards;
    }

    // 每个池子总下注
    private long perTotalPoolBets( Collection<String> values) {
        // 每个池子的下注总饿
        long bet = 0;
        for (String val : values) {
            bet += Long.parseLong(val);
        }
        return bet;
    }

    private  Map<String, Long> getUserBetsMap(List<String> winnerSitNum ,long bet, long smallBlindMoney) {
        int size = winnerSitNum.size();
        Map<String, Long> sitAndBetsMap = new HashMap<>();
        if(size == 1) {
            sitAndBetsMap.put(winnerSitNum.get(0), bet);
        } else if (size > 1) {
            // 小盲注的个数
            long averageSmallBlindCnt = bet / smallBlindMoney;
            // 剩下的筹码
            long remCnt = bet % smallBlindMoney;
            // 每个玩家分的的小盲注个数
            long average = averageSmallBlindCnt / size;
            // 剩余的小盲注
            long cnt = averageSmallBlindCnt % size;
            for(int i = winnerSitNum.size() - 1; i >= 0; i--) {
                String sitNum = winnerSitNum.get(i);
                long money = average * smallBlindMoney;
                long leftSmall = (cnt > 0 ? 1 : 0) * smallBlindMoney;
                sitAndBetsMap.put(sitNum, (money  + leftSmall +  remCnt));
                remCnt = 0;
                cnt --;
            }
        }
        return sitAndBetsMap;
    }

    // 排序
    private List<String> sortWinnerList(List<Map.Entry<String, PokerOuts>> outsList, String buttonSitNum) {

        List<String> winnerSits = new ArrayList<>();
        for (Map.Entry<String, PokerOuts> entry : outsList) {
            winnerSits.add(entry.getKey());
        }
        // 进行排序
        boolean hasBtnSit = winnerSits.contains(buttonSitNum);
        if (!hasBtnSit) {
            winnerSits.add(buttonSitNum);
        }
        Collections.sort(winnerSits);
        SitCycQueue sitCycQueue = new SitCycQueue(winnerSits.size());
        for (String str : winnerSits) {
            sitCycQueue.addRear(Integer.parseInt(str), buttonSitNum.equals(str));
        }
        int[] returns = sitCycQueue.returnAllByButton();
        List<String> result = new ArrayList<>();
        for (int index : returns) {
            //
            if (!hasBtnSit && index == Integer.parseInt(buttonSitNum)) {
                continue;
            }
            result.add(index + "");

        }
        return result;
    }

    private Map<String, Long> userAllBetMap(List<Map<String, Long>> userBetList) {
        Map<String, Long> betMap = new HashMap<>();
        for (Map<String, Long> userBet : userBetList) {
            Set<Map.Entry<String, Long>> entrySet = userBet.entrySet();
            for (Map.Entry<String, Long> entry : entrySet) {
                String key = entry.getKey();
                Long value = entry.getValue();
                Long oldVal = betMap.get(key);
                if (oldVal != null) {
                    value += oldVal;
                }
                betMap.put(key, value);
            }
        }
        return betMap;
    }

    // 结算
    private void balanceBets(Map<String, Long> winBet, Map<String, String> tableSitUserIdMap, String tableId,
                             Map<String, Long> allWinBetMap) {
        Set<Map.Entry<String, Long>> winBetEntrySet = winBet.entrySet();
        for (Map.Entry<String, Long> entry : winBetEntrySet) {
             String key = entry.getKey();
             Long value = entry.getValue();
             String userId = tableSitUserIdMap.get(key);
             if(userId != null) {
                if (allWinBetMap.containsKey(userId)) {
                    Long userWinBet = allWinBetMap.get(userId);
                    if (userWinBet == null) {
                        userWinBet = 0L;
                    }
                    allWinBetMap.put(userId, userWinBet.intValue() + value);
                }
                UserChips userChips = UserChips.load(userId, tableId);
                if (userChips != null) {
                    userChips.addAndFlushChips(value);
                }
             }
        }
    }

    private List<Map<String, Object>> notifyUserInfo(Map<String, Long> userAllMap, List<Map<String, Long>> resultList,
                                                     Collection<String> onlineTableUserIds, Map<String, String> userCardsMap,
                                                     List<String> deskCards,String innerId, String innerCount,
                                                     Map<String, String> allUserCardsMap, Map<String, String> gamingSitUserMap,
                                                     boolean gameOver, float delaySec, Map<String, UserStatisticManager.UserStatistic> userStatisticMap,
                                                     Map<String, String> sitBetUserId, Map<String, Integer> userSafeWinMap,
                                                     String tableId, List<GameRecord.Progress> progresses, long startTime,
                                                     GameRecord gameRecord) {
        List<Map<String, Object>> userWinMap = new ArrayList<>();
        Set<Map.Entry<String, Long>> mapSet = userAllMap.entrySet();
        for (Map.Entry<String, Long> entry : mapSet) {
            String sitNum = entry.getKey();
            Long bet = entry.getValue();
            Map<String, Object> map = new HashMap<>();
            if(gamingSitUserMap.containsKey(sitNum)) { // 还在桌子上的玩家
                map.put("sn", Integer.parseInt(sitNum));
                map.put("win", bet);
                userWinMap.add(map);
            }
        }
        List<Map<String, Object>> userSafeWinMapList = new ArrayList<>();
        if (!userSafeWinMap.isEmpty()) {
            Set<Map.Entry<String, Integer>> mapSafeWinSet = userSafeWinMap.entrySet();
            Map<String, String> gamingUserIAndSit = Maps.newHashMap();
            for(Map.Entry<String, String> entry :  gamingSitUserMap.entrySet()) {
                gamingUserIAndSit.put(entry.getValue(), entry.getKey());
            }
            for (Map.Entry<String, Integer> entry : mapSafeWinSet) {
                 String userId = entry.getKey();
                 Integer safeWin = entry.getValue();
                 if (!gamingUserIAndSit.containsKey(userId)) {
                      continue;
                 }
                 String sn = gamingUserIAndSit.get(userId);
                 if (StringUtils.isBlank(sn)) {
                    continue;
                 }
                 Map<String, Object> map = new HashMap<>();
                 map.put("userId", userId);
                 map.put("win", safeWin.intValue());
                 map.put("sn",  Integer.valueOf(sn));
                 userSafeWinMapList.add(map);
            }
        }

        List<Object> poolWinInfList = new ArrayList<>();
        // 池序号
        int poolNum = 0;
        for (Map<String, Long> poolMap : resultList) {
            Map<String, Object> poolWinMap = new HashMap<>();
            poolNum ++;
            Set<Map.Entry<String, Long>> poolMapSet = poolMap.entrySet();
            List<Map<String, Object>> perPoolList = new ArrayList<>();
            for (Map.Entry<String, Long> entry : poolMapSet) {
                String sitNum = entry.getKey();
                if(gamingSitUserMap.containsKey(sitNum)) { // 还在桌子上的玩家
                    Long bet = entry.getValue();
                    Map<String, Object> map = new HashMap<>();
                    map.put("sn", Integer.parseInt(sitNum));
                    map.put("win", bet);
                    perPoolList.add(map);
                }
            }
            poolWinMap.put("pool_num", poolNum);
            poolWinMap.put("pool_inf", perPoolList);
            poolWinInfList.add(poolWinMap);
        }
        Map<String, PokerOuts> pokerOutsMap = PokerManager.getUsersPokerOutsList(userCardsMap, deskCards);
        Set<Map.Entry<String, PokerOuts>> pokerSet = pokerOutsMap.entrySet();
        List<Map.Entry<String, PokerOuts>> pokerOutList = new ArrayList<>(pokerSet);
        Collections.sort(pokerOutList, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
        List<Map<String, Object>> userCardsResult = new ArrayList<>();
        int size = pokerOutList.size();

        // 比牌的玩家人数大于1必须亮牌
        if (size > 1) {
            PokerOuts maxPokerOuts = null;
            for (Map.Entry<String, PokerOuts> entry : pokerOutList) {
                String sitNum = entry.getKey();
                // 移除掉已经亮牌的玩家，最后判断需要亮牌的玩家
                allUserCardsMap.remove(sitNum);
                PokerOuts pokerOuts = entry.getValue();
                long level = pokerOuts.getLevel();
                List<Poker> pokerList = pokerOuts.getOutList();
                String pokers = "";
                for (Poker poker : pokerList) {
                    pokers += (poker.getShortPoker() + ",");
                }
                String newPokers = pokers.substring(0, pokers.length() - 1);
                Map<String, Object> oneUserCards = new HashMap<>();
                oneUserCards.put("sn", Integer.parseInt(sitNum));
                oneUserCards.put("uid", sitBetUserId.get(sitNum));
                oneUserCards.put("level", level);
                oneUserCards.put("hand_cards", userCardsMap.get(sitNum));
                oneUserCards.put("cards", newPokers);

                gameRecord.addSitShowCardSit(sitNum, userCardsMap.get(sitNum));
                progresses.add(GameRecord.Progress.create("show_card", sitNum,userCardsMap.get(sitNum),
                        System.currentTimeMillis() - startTime));

                String winUId = null;
                if (maxPokerOuts == null) {
                    oneUserCards.put("is_max", "1");
                    maxPokerOuts = pokerOuts;
                    winUId = sitBetUserId.get(sitNum);
                } else if (pokerOuts.compareTo(maxPokerOuts) >= 0) {
                    oneUserCards.put("is_max", "1");
                    maxPokerOuts = pokerOuts;
                    winUId = sitBetUserId.get(sitNum);
                } else {
                    oneUserCards.put("is_max", "0");
                }
                userCardsResult.add(oneUserCards);
                if(winUId != null) {
                    UserStatisticManager.UserStatistic userStatistic = userStatisticMap.get(winUId);
                    if (userStatistic != null) {
                        userStatistic.setWinCnt(1);
                    }
                }
            }
        }
        addShowCards(allUserCardsMap, userCardsResult, gamingSitUserMap, deskCards);

        List<Map<String, Object>> userWinMapList = sortWinOrder(userWinMap, userCardsResult);

        if (userWinMapList.size() > 0) {
            delaySec += userWinMapList.size() * 1.5 + 2f;
        }
        long delayMills =  Math.max((long)(delaySec * 1000), GameConstants.DANDB_DELAYER_TIME) - GameConstants.AFTER_CHECK_CHIPS_DELAYER_TIME;
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCount);
        object.put("pool_win_inf", poolWinInfList); // 池子里面赢得的
        object.put("user_win_inf", userWinMapList); // 玩家总共赢得的
        object.put("user_safe_win_inf", userSafeWinMapList); // 玩家总共赢得的
        object.put("user_cards_inf", userCardsResult); //玩家的牌
        object.put("game_over", gameOver ? 1 : 0);
        object.put("delay_ses", delayMills/1000);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.FINISHED, onlineTableUserIds, id, tableId);
        return userWinMapList;
    }

    private void addShowCards(Map<String, String> allUserCardsMap,
                              List<Map<String, Object>> userCardsResult,
                              Map<String, String> gamingSitUserMap, List<String> deskCards) {
        Map<String, String> userCardsMap = new HashMap<>();
        if (allUserCardsMap != null && allUserCardsMap.size() > 0) {
            Iterator<Map.Entry<String, String>> iterator =  allUserCardsMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                String sitNum = entry.getKey();
                String userId = gamingSitUserMap.get(sitNum);
                if (StringUtils.isBlank(userId)) {
                    continue;
                }
                TableUser tableUser = TableUser.load(userId);
                // 移除不亮牌
                if (tableUser != null && !"1" .equalsIgnoreCase(tableUser.getStatus())) {
                    iterator.remove();
                }
            }
        }
        if(allUserCardsMap != null && allUserCardsMap.size() > 0) {
            Map<String, PokerOuts> pokerOutsMap = PokerManager.getUsersPokerOutsList(userCardsMap, deskCards);
            Set<Map.Entry<String, PokerOuts>> pokerSet = pokerOutsMap.entrySet();
            List<Map.Entry<String, PokerOuts>> pokerOutList = new ArrayList<>(pokerSet);
            for (Map.Entry<String, PokerOuts> entry : pokerOutList) {
                String sitNum = entry.getKey();
                // 移除掉已经亮牌的玩家，最后判断需要亮牌的玩家
                allUserCardsMap.remove(sitNum);
                PokerOuts pokerOuts = entry.getValue();
                long level = pokerOuts.getLevel();
                List<Poker> pokerList = pokerOuts.getOutList();
                String pokers = "";
                for (Poker poker : pokerList) {
                    pokers += (poker.getShortPoker() + ",");
                }
                String newPokers = pokers.substring(0, pokers.length() - 1);
                Map<String, Object> oneUserCards = new HashMap<>();
                oneUserCards.put("sn", Integer.parseInt(sitNum));
                oneUserCards.put("level", level);
                oneUserCards.put("hand_cards", userCardsMap.get(sitNum));
                oneUserCards.put("cards", newPokers);
                oneUserCards.put("is_max", "0");
                userCardsResult.add(oneUserCards);
            }
        }

    }

    public static List<Map<String, Object>> sortWinOrder(List<Map<String, Object>> userWinMapList,
                                                         List<Map<String, Object>> userCardsResult) {
        List<Map<String, Object>> newMapList = new ArrayList<>();
        for (Map<String, Object> userCardsMap : userCardsResult) {
            Integer sn = (Integer) userCardsMap.get("sn");
            for (int i = userWinMapList.size() - 1; i >= 0; i--) {
                Map<String, Object> userWinMap = userWinMapList.get(i);
                Integer sn2 = (Integer) userWinMap.get("sn");
                if (sn.intValue() == sn2.intValue()) {
                    newMapList.add(userWinMap);
                    userWinMapList.remove(i);
                }
            }
        }
        if (userWinMapList.size() > 0) {
            newMapList.addAll(userWinMapList);
        }
        return newMapList;
    }

    // 更新玩家的局记录
    private void updateUserWinLogInfo(TableStatus tableStatus,
                                      Map<String, Long> userIdsAndWinMap,
                                      ActorRef self) {
        updateStatices(tableStatus.getTableId(), 1);
        //增加局次数
        // self.tell(new TableDbUpdateCntEvent(tableStatus.getTableId(), 1), ActorRef.noSender());

        Map<String, String> allSitBet = tableStatus.getSitBetStatusMap();
        if (allSitBet != null) {
            // 所有参加游戏的人的座位号
            Set<String> sitNums = allSitBet.keySet();
            Map<String, String> sitAndUserIds = tableStatus.getSitBetUserIdMap();
            if(sitAndUserIds == null || userIdsAndWinMap == null) {
               return;
            }
            for (String sit : sitNums) {
                 String userId = sitAndUserIds.get(sit);
                 Long addChips = userIdsAndWinMap.get(userId);
                 long leftChips = 0;
                 UserChips userChips = UserChips.load(userId, tableStatus.getTableId());
                 if (userChips != null) {
                     leftChips = userChips.getChips();
                 }
                 if (addChips == null) {
                     addChips = 0L;
                 }
                try {
                    FriendCoreIface.instance().iface().updateStrangerWinLoseLogs(tableStatus.getTableId(), userId, addChips, leftChips);
                } catch (TException e) {
                    e.printStackTrace();
                    throw new RuntimeException("更新玩家结算信息失败" + JSON.toJSONString(tableStatus));
                }
//                // 异步统计信息 有bug 有时候已经关闭了actor 发送失败
//                 self.tell(new UserUpdateStaticesEvent(userId, tableStatus.getTableId(),addChips.longValue(), leftChips),
//                        ActorRef.noSender());
            }
        }
    }

    private Map<String, UserStatisticManager.UserStatistic> initrStatistic(Collection<String> userIds) {
        Map<String, UserStatisticManager.UserStatistic> userStatisticMap = new HashMap<>();
        for (String userId : userIds) {
            UserStatisticManager.UserStatistic userStatistic = new UserStatisticManager.UserStatistic();
            userStatistic.setUserId(userId);
            userStatistic.setJoinCnt(1);
            userStatisticMap.put(userId, userStatistic);
        }
        return userStatisticMap;
    }

    private void updateGameEndInf(String userId,  Map<String, UserStatisticManager.UserStatistic> map) {
        UserStatisticManager.UserStatistic userStatistic = map.get(userId);
        if (userStatistic != null) {
            userStatistic.setGamingEndCnt(1);
        }
    }

    private boolean updateStatices(String groupId,int cnt) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        // 手数+1
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("totalPlayCnt", cnt);

        BasicDBObject updateSetValue = new BasicDBObject("$inc", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }
}
