package com.tiantian.strangergame.akka.user;

import akka.actor.ActorRef;
import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 * 玩家坐下之前刷新买入成功事件
 */
public class TableUserAddBuyInOkEvent extends TableUserEvent {
    private String userId;
    private String groupId;
    private long totalBuyIn;
    private int userSitNum;
    private long smallBlind;
    private long bigBlind;
    private Object rootEvent;
    private ActorRef clientRef;

    public TableUserAddBuyInOkEvent(String userId, String groupId, long totalBuyIn, int userSitNum, long smallBlind,
                                    long bigBlind, Object rootEvent, ActorRef clientRef) {
        this.userId = userId;
        this.groupId = groupId;
        this.totalBuyIn = totalBuyIn;
        this.userSitNum = userSitNum;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        this.rootEvent = rootEvent;
        this.clientRef = clientRef;
    }

    @Override
    public String tableId() {
        return groupId;
    }

    @Override
    public String event() { // 买入成功
        return "addBuyInOknBeforeSitDown";
    }

    public String getUserId() {
        return userId;
    }

    public long getTotalBuyIn() {
        return totalBuyIn;
    }

    public Object getRootEvent() {
        return rootEvent;
    }

    public ActorRef getClientRef() {
        return clientRef;
    }

    public void setClientRef(ActorRef clientRef) {
        this.clientRef = clientRef;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setTotalBuyIn(long totalBuyIn) {
        this.totalBuyIn = totalBuyIn;
    }

    public int getUserSitNum() {
        return userSitNum;
    }

    public void setUserSitNum(int userSitNum) {
        this.userSitNum = userSitNum;
    }

    public long getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(long smallBlind) {
        this.smallBlind = smallBlind;
    }

    public long getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(long bigBlind) {
        this.bigBlind = bigBlind;
    }

    public void setRootEvent(Object rootEvent) {
        this.rootEvent = rootEvent;
    }
}
