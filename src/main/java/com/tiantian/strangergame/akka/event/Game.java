package com.tiantian.strangergame.akka.event;

import java.io.Serializable;

/**
 *
 */
public interface Game extends Serializable {
    String tableId();
}
