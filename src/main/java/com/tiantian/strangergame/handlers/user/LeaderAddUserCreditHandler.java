package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.user.LeaderAddMoneyEvent;
import com.tiantian.strangergame.akka.event.LeaderAddUserCreditEvent;
import com.tiantian.strangergame.handlers.EventHandler;

/**
 *
 */
public class LeaderAddUserCreditHandler implements EventHandler<LeaderAddMoneyEvent> {
    @Override
    public void handler(LeaderAddMoneyEvent event, ActorRef self, UntypedActorContext context,
                        ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        int buyInCnt = event.getAddBuyInCnt();
        long oneBuyIn = event.getBuyIn();
        // 直接发送到UserActor中处理
        self.tell(new LeaderAddUserCreditEvent(tableId, buyInCnt, oneBuyIn, userId, sender), ActorRef.noSender());
    }
}
