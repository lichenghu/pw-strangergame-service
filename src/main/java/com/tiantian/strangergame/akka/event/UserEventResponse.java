package com.tiantian.strangergame.akka.event;

/**
 *
 */
public interface UserEventResponse {
    String userId();
}
