package com.tiantian.strangergame.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by lichenghu on 15-5-6.
 */
public class FileUtil {
    public static String getFileContent(String file) throws IOException {
        InputStream inStream = null;
        try {
            inStream =
                    FileUtil.class.getClassLoader().getResourceAsStream(file);
            return convertStream2Str(inStream);
        }
        finally {
            if (inStream != null) {
                inStream.close();
            }
        }
    }

    private static String convertStream2Str(InputStream inputStream)
    {
        String str = "";
        // ByteArrayOutputStream相当于内存输出流
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        // 将输入流转移到内存输出流中
        try
        {
            while ((len = inputStream.read(buffer, 0, buffer.length)) != -1)
            {
                out.write(buffer, 0, len);
            }
            // 将内存流转换为字符串
            str = new String(out.toByteArray());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return str;
    }
}
