package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.fc.thrift.core.FcResponse;
import com.tiantian.fc.thrift.core.StrangerWinLoseLog;
import com.tiantian.stranger.akka.user.TableUserBuySafeEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 *
 */
public class UserBuySafeHandler implements EventHandler<TableUserBuySafeEvent> {
    static Logger LOG = LoggerFactory.getLogger(UserBuySafeHandler.class);
    @Override
    public void handler(TableUserBuySafeEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        String userId = event.getUserId();
        int buyIndex = event.getBuyIndex();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null || tableStatus.isNull()) {
            sender.tell(new TableUserBuySafeEvent.Response(-1, "买入信息不正确"), ActorRef.noSender());
            LOG.error("========buy error 1==========");
            return;
        }
        TableUser tableUser = TableUser.load(userId);
        if (tableUser == null || StringUtils.isBlank(tableUser.getSitNum())) {
            sender.tell(new TableUserBuySafeEvent.Response(-1, "买入信息不正确"), ActorRef.noSender());
            LOG.error("========buy error 2==========");
            return;
        }
        Map<String, Map<String, Object>> outsMap = tableStatus.getOutsMap();
        if (outsMap == null) {
            sender.tell(new TableUserBuySafeEvent.Response(-1, "买入信息不正确"), ActorRef.noSender());
            LOG.error("========buy error 3==========");
            return;
        }
        Map<String, Object> outResult = outsMap.get(tableUser.getSitNum());
        if (outResult == null || outResult.isEmpty()) {
            sender.tell(new TableUserBuySafeEvent.Response(-1, "买入信息不正确"), ActorRef.noSender());
            LOG.error("========buy error 4==========");
            return;
        }
        if (buyIndex <= 0) {
            sender.tell(new TableUserBuySafeEvent.Response(1, "取消成功"), ActorRef.noSender());
            outResult.put("user_op", "1");
            tableStatus.setOutsMapStr(JSON.toJSONString(outsMap));
            tableStatus.saveNotAddCnt(); // 不需要增加
            checkNext(outsMap, tableStatus, tableId, self, context, sender);
            return;
        }
        if (buyIndex > 3) {
            sender.tell(new TableUserBuySafeEvent.Response(-1, "买入信息不正确"), ActorRef.noSender());
            LOG.error("========buy error 5==========");
            return;
        }
        String id = (String) outResult.get("id");
        String gameStatus = (String) outResult.get("game_status");
        if (!tableStatus.getStatus().equalsIgnoreCase(gameStatus)) {
            sender.tell(new TableUserBuySafeEvent.Response(-1, "买入信息不正确"), ActorRef.noSender());
            LOG.error("========buy error 6==========");
            return;
        }

        String buyStatus = (String) outResult.get("buy_status");
        if (!"0".equalsIgnoreCase(buyStatus)) { // 已经购买
            sender.tell(new TableUserBuySafeEvent.Response(-2, "已经购买"), ActorRef.noSender());
            return;
        }

        JSONArray prices = (JSONArray) outResult.get("price");
        String price = ((String)prices.get(buyIndex - 1));

        String[] oddsAndVal = price.split(",");
        long buy = Long.parseLong(oddsAndVal[0]);
        long value = Long.parseLong(oddsAndVal[1]);
        // 判断玩家保险金
        StrangerWinLoseLog strangerWinLoseLog = null;
        try {
            strangerWinLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(tableId, userId);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (strangerWinLoseLog == null) {
            sender.tell(new TableUserBuySafeEvent.Response(-3, "保险证不足"), ActorRef.noSender());
            return;
        }
//        if (strangerWinLoseLog.getMoneyLeft() < buy) {
//            sender.tell(new TableUserBuySafeEvent.Response(-3, "保险证不足"), ActorRef.noSender());
//            return;
//        }
        // 扣除保险金
        FcResponse response = null;
        try {
            response = FriendCoreIface.instance().iface().reduceStrangerMoney(strangerWinLoseLog.getLogId(), buy);
        } catch (TException e) {
            e.printStackTrace();
        }
        if (response != null && "0".equalsIgnoreCase(response.getStatus())) {
            // 成功
            // 进行保存保险记录
            try {
                FriendCoreIface.instance().iface().saveSafeBuyLog(id, userId, buy, value, tableId, tableStatus.getInningId());
            } catch (TException e) {
                e.printStackTrace();
            }
        }
        //修改玩家保险数据
        outResult.put("buy_status", buyIndex + "");
        outResult.put("user_op", "1");
        tableStatus.setOutsMapStr(JSON.toJSONString(outsMap));
        tableStatus.saveNotAddCnt(); // 不需要增加
        sender.tell(new TableUserBuySafeEvent.Response(0, "买入成功"), ActorRef.noSender());
        System.out.println("tableStatus======" + JSON.toJSONString(tableStatus));
        checkNext(outsMap, tableStatus, tableId, self, context, sender);
    }

    private void checkNext(Map<String, Map<String, Object>> outsMap, TableStatus tableStatus, String tableId,
                           ActorRef self, UntypedActorContext context, ActorRef sender) {
        // 判断是否全部已经操作了
        boolean needNextStatus = true;
        for (Map<String, Object> mapVal : outsMap.values()) {
            String userOp = (String) mapVal.get("user_op");
            if ("0".equalsIgnoreCase(userOp)) {
                needNextStatus = false;
            }
        }
        System.out.println("needNextStatus=========" + needNextStatus);
        if (needNextStatus) {
            GameStatus nextStatus = GameUtils.nextGameStatus(tableStatus.getStatus());
            System.out.println("nextStatus=========" + nextStatus.name());
            // 触发下一轮的发牌事件
            GameUtils.nextStatusTask(nextStatus, tableId, tableStatus.getInningId(), self, context, sender);
        }
    }
}
