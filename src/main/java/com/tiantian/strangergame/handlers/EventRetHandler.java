package com.tiantian.strangergame.handlers;


import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;

/**
 *
 */
public interface EventRetHandler <T>{
    Object handler(T event, ActorRef self, UntypedActorContext context, ActorRef sender);
}
