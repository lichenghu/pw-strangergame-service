package com.tiantian.strangergame.manager.model;

import com.alibaba.fastjson.JSON;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.manager.constants.GameConstants;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 玩家筹码
 */
public class UserChips {
    private String userId;
    private String tableId;
    private long chips;

    public static UserChips load(String userId, String tableId) {
        String key = GameConstants.USER_GROUP_GMAE_KEY + userId;
        String value = RedisUtil.getFromMap(key, tableId);
        if (StringUtils.isNotBlank(value)) {
            RedisUtil.expire(key, 3600 * 24); // 1天有效时间
            return JSON.parseObject(value, UserChips.class);
        }
        return null;
    }

    public static UserChips init(String userId, String tableId, long chips) {
        UserChips userChips = new UserChips();
        userChips.setUserId(userId);
        userChips.setTableId(tableId);
        userChips.setChips(chips);
        String userStr = JSON.toJSONString(userChips);
        RedisUtil.setMap(GameConstants.USER_GROUP_GMAE_KEY + userId, tableId, userStr);
        return userChips;
    }

    public void delSelf(String inGroupId) {
        if (inGroupId != null && inGroupId.equalsIgnoreCase(this.tableId)) {
            String key = GameConstants.USER_GROUP_GMAE_KEY + this.userId;
            RedisUtil.delMapField(key, tableId);
        }
    }
    // 增加玩家的筹码
    public void addAndFlushChips(long addChips) {
        addChips = Math.abs(addChips);
        this.chips += addChips;
        String key = GameConstants.USER_GROUP_GMAE_KEY + userId;
        RedisUtil.setMap(key, tableId, JSON.toJSONString(this));
    }

    // 减少玩家的筹码
    public boolean reduceAndFlushChips(long reduceChips) {
        reduceChips = Math.abs(reduceChips);
        if (this.chips - reduceChips < 0) {
            return false;
        }
        this.chips -= reduceChips;
        String key = GameConstants.USER_GROUP_GMAE_KEY + userId;
        RedisUtil.setMap(key, tableId, JSON.toJSONString(this));
        return true;
    }

    public static Map<String, UserChips> loadAll(String userId) {
        Map<String ,UserChips> resultMap = new HashMap<>();
        String key = GameConstants.USER_GROUP_GMAE_KEY + userId;
        Map<String, String> allMap = RedisUtil.getMap(key);
        if (allMap != null) {
            Set<Map.Entry<String, String>> entrySet = allMap.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                 String value = entry.getValue();
                 try {
                     if (StringUtils.isNotBlank(value)) {
                         UserChips userChips = JSON.parseObject(value, UserChips.class);
                         resultMap.put(entry.getKey(), userChips);
                     }
                 }
                 catch (Exception e) {
                        e.printStackTrace();
                 }
            }
        }
        return resultMap;
    }

    public UserChips() {

    }

    public UserChips(String userId, String groupId, long chips) {
        this.userId = userId;
        this.tableId = groupId;
        this.chips = chips;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getChips() {
        return chips;
    }

    public void setChips(long chips) {
        this.chips = chips;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
