package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.akka.event.UserWinSafeEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.GameRecord;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.texas.Poker;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class TableFlopHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        // 回合ID
        String inningId = jsonObject.getString("inningId");
        //从redis中获取三张牌（先烧一张） 发给玩家 然后再把剩下的牌保存到redis
        //三张牌
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null || tableStatus.isNull() || StringUtils.isBlank(tableStatus.getCards())) {
            return;
        }
        if (!tableStatus.getInningId().equalsIgnoreCase(inningId) || !GameStatus.PRE_FLOP.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }
        // 已经发过牌了
        if (tableStatus.getDeskCardList() != null && tableStatus.getDeskCardList().size() > 0) {
            return;
        }
        List<Poker> pokerList = JSON.parseArray(tableStatus.getCards(), Poker.class);
        pokerList.remove(0);
        List<String> threeCards = new ArrayList<>();
        Poker poker1 = pokerList.remove(0);
        threeCards.add(poker1.getShortPoker());
        Poker poker2 = pokerList.remove(0);
        threeCards.add(poker2.getShortPoker());
        Poker poker3 = pokerList.remove(0);
        threeCards.add(poker3.getShortPoker());

        tableStatus.setCards(JSON.toJSONString(pokerList));
        // 设置当局的牌状态为flop
        tableStatus.setStatus(GameStatus.FLOP.name());
        tableStatus.setDeskCards(JSON.toJSONString(threeCards));
        tableStatus.checkDelay();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        //发送三张牌的信息给玩家

        //发送三张牌的信息给玩家
        GameRecord gameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (gameRecord == null) {
            gameRecord = new GameRecord();
        }


        JSONObject object = new JSONObject();
        String flopCards = StringUtils.join(threeCards, ",");
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("flop_cards", flopCards);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.FLOP_CARDS, tableAllUser.getOnlineTableUserIds(), id, tableId);
        GameUtils.noticeCardsLevel(tableStatus, tableAllUser);
        // 判断是否有人买保险
        checkSafe(tableStatus, tableAllUser, self, threeCards);

        List<GameRecord.Progress> progresses = gameRecord.getProgresses();
        progresses.add(GameRecord.Progress.create("flop", "nil", flopCards,
                System.currentTimeMillis() - gameRecord.getStartTime()));

        RecordUtils.restLastedRecord(gameRecord, tableId);

        // 校验是否直接进行下一个发牌
        GameStatus nextStatus = GameUtils.beforeCheckNextStatus(GameStatus.FLOP.name(), tableStatus);
        if (nextStatus != null) {
            tableStatus.roundBetEnd();
            boolean hasSafe = GameUtils.checkSafe(tableStatus, tableAllUser);
            tableStatus.save();
            // 通知玩家的池信息
            GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
            if (hasSafe) {
                // 触发下一轮的发牌事件
                GameUtils.triggerNextStatusTask(nextStatus, tableId, tableStatus.getInningId(), true);
            }
            else {
                // 触发下一轮的发牌事件
                GameUtils.nextStatusTask(nextStatus, tableId, inningId, self, context, sender);
            }
            return;
        }

        //通知下一个玩家下注
        GameUtils.noticeNextUserBet(tableId, tableStatus, null, true, null, tableAllUser,
                self, context, sender);
    }

    private void checkSafe(TableStatus tableStatus, TableAllUser tableAllUser, ActorRef self, List<String> threeCards) {
        Map<String, Map<String, Object>> outMap = tableStatus.getOutsMap();
        if (outMap == null || outMap.isEmpty()) {
            return;
        }
        Set<Map.Entry<String, Map<String, Object>>> entries = outMap.entrySet();
        for (Map.Entry<String, Map<String, Object>> entry : entries) {
             String sitNum = entry.getKey();
             Map<String, Object> mapVal = entry.getValue();
             String buyStatus = (String) mapVal.get("buy_status");
             if (!"0".equals(buyStatus)) { // 买过了保险 判断是否中了
                 String id = (String) mapVal.get("id");
                 JSONArray outs = (JSONArray) mapVal.get("outs");
                 if(outs == null || outs.size() == 0) {
                    continue;
                 }
                 boolean hasSafe = false;
                 for (String card : threeCards) {
                      System.out.println("FLOP ===three= card:" + card + ", outs" + JSON.toJSONString(outs));
                      if (outs.contains(card)) { //中保险
                          hasSafe = true;
                          break;
                      }
                 }
                 if(!hasSafe) {
                    System.out.println("FLOP====no safe====");
                    continue;
                 }

                 JSONArray allPrices = (JSONArray) mapVal.get("price");
                 String[] prices = ((String)allPrices.get(Integer.parseInt(buyStatus) - 1)).split(",");
                 long win = Long.parseLong(prices[1]);

                 String userId = tableAllUser.getGamingSitUserMap().get(sitNum);
                 if (StringUtils.isBlank(userId)) {
                     continue;
                 }

                 self.tell(new UserWinSafeEvent(userId, id, tableStatus.getTableId(), win), ActorRef.noSender());
                 tableStatus.addUserSafeWin(userId, (int) win);
                 //发送通知
                 JSONObject object = new JSONObject();
                 object.put("inner_id", tableStatus.getInningId());
                 object.put("inner_cnt", tableStatus.getIndexCount());
                 object.put("win", win);
                 GameUtils.notifyUser(object, GameEventType.WIN_SAFE, userId, tableStatus.getTableId());
             }
        }
        // 清空掉数据
        tableStatus.setOutsMapStr("");
    }
}
