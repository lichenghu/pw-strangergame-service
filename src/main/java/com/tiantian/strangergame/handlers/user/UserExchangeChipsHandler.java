package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.user.TableUserExchangeChipsEvent;
import com.tiantian.strangergame.akka.event.UserExchangeChipsEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.manager.model.UserChips;
import org.apache.commons.lang.StringUtils;

/**
 * 玩家把保证金兑换成筹码
 */
public class UserExchangeChipsHandler implements EventHandler<TableUserExchangeChipsEvent> {

    @Override
    public void handler(TableUserExchangeChipsEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        long buyInCnt = event.getBuyInCnt();
        long oneBuyIn = event.getOneBuyIn();

        // 直接发送到UserActor中处理
        self.tell(new UserExchangeChipsEvent(userId, tableId, buyInCnt, oneBuyIn, sender), ActorRef.noSender());
    }
}
