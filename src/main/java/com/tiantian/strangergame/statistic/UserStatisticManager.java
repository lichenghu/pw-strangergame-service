package com.tiantian.strangergame.statistic;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.strangergame.data.mongodb.MGDatabase;
import org.apache.commons.lang.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import reactor.core.Environment;
import reactor.core.Reactor;
import reactor.core.configuration.PropertiesConfigurationReader;
import reactor.core.spec.Reactors;
import reactor.event.Event;
import reactor.event.selector.Selectors;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 *
 */
public class UserStatisticManager {
    private static int TOTAL_NUM = 169; // 总牌型 数量为169
    private static String GAME_STATISTIC = "user_friend_game_statistic";

    private Reactor reactor;

    private static class UserStatisticManagerHolder {
        private final static UserStatisticManager instance = new UserStatisticManager();
    }
    public static UserStatisticManager getInstance() {
        return UserStatisticManagerHolder.instance;
    }

    private UserStatisticManager() {
        Environment env = new Environment(new PropertiesConfigurationReader("reactor_log"));
        reactor = Reactors.reactor()
                .env(env)
                .dispatcher(Environment.WORK_QUEUE)
                .get();

        eventRegister();
    }

    private void eventRegister() {
        // 注册事件处理
        reactor.on(Selectors.$("statistic"), new UserStatisticConsumer());
    }

    public void statistic(Collection<UserStatistic> userStatisticList) {
        reactor.notify("statistic", Event.wrap(userStatisticList));
    }

    public static class UserStatistic {
        private String userId;
        private long joinCnt;
        private long gamingCnt;
        private long gamingEndCnt;
        private long winCnt;
        private double avgRaise;
        private String cardsType;
        public UserStatistic(){

        }
        public UserStatistic(String userId, long joinCnt, long gamingCnt, long gamingEndCnt, long winCnt,
                             double avgRaise, String cardsType) {
            this.userId = userId;
            this.joinCnt = joinCnt;
            this.gamingCnt = gamingCnt;
            this.gamingEndCnt = gamingEndCnt;
            this.winCnt = winCnt;
            this.avgRaise = avgRaise;
            this.cardsType = cardsType;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public long getJoinCnt() {
            return joinCnt;
        }

        public void setJoinCnt(long joinCnt) {
            this.joinCnt = joinCnt;
        }

        public long getGamingCnt() {
            return gamingCnt;
        }

        public void setGamingCnt(long gamingCnt) {
            this.gamingCnt = gamingCnt;
        }

        public long getGamingEndCnt() {
            return gamingEndCnt;
        }

        public void setGamingEndCnt(long gamingEndCnt) {
            this.gamingEndCnt = gamingEndCnt;
        }

        public long getWinCnt() {
            return winCnt;
        }

        public void setWinCnt(long winCnt) {
            this.winCnt = winCnt;
        }

        public double getAvgRaise() {
            return avgRaise;
        }

        public void setAvgRaise(double avgRaise) {
            this.avgRaise = avgRaise;
        }

        public String getCardsType() {
            return cardsType;
        }

        public void setCardsType(String cardsType) {
            this.cardsType = cardsType;
        }
    }

    private class UserStatisticConsumer implements reactor.function.Consumer<Event<Collection<UserStatistic>>>{
        @Override
        public void accept(Event<Collection<UserStatistic>> userStatisticEvent) {
            Collection<UserStatistic> userStatisticList = userStatisticEvent.getData();
            if (userStatisticList != null) {
                try {
                    for (UserStatistic userStatistic : userStatisticList) {
                        updateUserStatistic(userStatistic.userId, userStatistic.joinCnt, userStatistic.gamingCnt,
                                userStatistic.gamingEndCnt, userStatistic.winCnt, userStatistic.avgRaise,
                                userStatistic.cardsType);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 更新统计
     */
    private boolean updateUserStatistic(String userId,long joinCnt, long gamingCnt, long gamingEndCnt,
                                              long winCnt, double avgRaise, String cardsType) {
        UserFriendStatistic getUserStatistic = getUserStatistic(userId);
        if (joinCnt > 0) {
            long oldJoinCnt = getUserStatistic.getJoinCnt();
            getUserStatistic.setJoinCnt(oldJoinCnt + joinCnt);
        }
        if (gamingCnt > 0) {
            long oldGamingCnt = getUserStatistic.getGamingCnt();
            getUserStatistic.setGamingCnt(oldGamingCnt + gamingCnt);
        }
        if (gamingEndCnt > 0) {
            long oldGamingEndCnt = getUserStatistic.getGamingEndCnt();
            getUserStatistic.setGamingEndCnt(oldGamingEndCnt + gamingEndCnt);
        }
        if (winCnt > 0) {
            long oldWinCnt = getUserStatistic.getWinCnt();
            getUserStatistic.setWinCnt(oldWinCnt + winCnt);
        }
        if (avgRaise > 0) {
            double oldAvgRaise = getUserStatistic.getAvgRaise();
            getUserStatistic.setAvgRaise(Math.min(2.0d, (oldAvgRaise + avgRaise)/ 2));
        }
        // 转换牌型
        if (StringUtils.isNotBlank(cardsType)) {
            // S12,D10
            String[] cards = cardsType.split(",");
            String sit1 = cards[0].substring(0, 1);
            String number1 = cards[0].substring(1);
            String sit2 = cards[1].substring(0, 1);
            String number2 = cards[1].substring(1);
            String ret = number1 + ":" + number2;
            if (Integer.parseInt(number2) > Integer.parseInt(number1)) {
                ret = number2 + ":" + number1;
            }
            if (sit1.equalsIgnoreCase(sit2)) {
                ret += ("|S");
            } else {
                ret += ("|O");
            }
            if (!getUserStatistic.getCardTypes().contains(ret)) {
                getUserStatistic.getCardTypes().add(ret);
            }
        }
        MongoCollection<Document> statisticCollection = MGDatabase.getInstance().getDB().getCollection(GAME_STATISTIC);

        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(getUserStatistic.getId()));
        // 更新筹码
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("joinCnt", getUserStatistic.getJoinCnt());
        updatedValue.put("gamingCnt", getUserStatistic.getGamingCnt());
        updatedValue.put("gamingEndCnt", getUserStatistic.getGamingEndCnt());
        updatedValue.put("winCnt", getUserStatistic.getWinCnt());
        updatedValue.put("avgRaise", getUserStatistic.getAvgRaise());
        updatedValue.put("cardTypes", getUserStatistic.getCardTypes());
        updatedValue.put("updateTime", new Date());
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        UpdateResult result = statisticCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }



    public UserFriendStatistic getUserStatistic(String userId) {
        MongoCollection<Document> statisticCollection = MGDatabase.getInstance().getDB().getCollection(GAME_STATISTIC);
        FindIterable<Document> limitIter = statisticCollection.find(new BasicDBObject("userId", userId));
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        UserFriendStatistic userFriendStatistic = null;
        if (cursor == null || !cursor.hasNext()) {
            // 保存一条
            return saveUserStatistic(userId);
        }
        while (cursor.hasNext()) {
            doc = cursor.next();
            userFriendStatistic = new UserFriendStatistic();
            ObjectId objectId = doc.getObjectId("_id");
            userFriendStatistic.setId(objectId.toString());
            String uId = doc.getString("userId");
            userFriendStatistic.setUserId(uId);
            Long joinCnt = doc.getLong("joinCnt");
            if (joinCnt != null) {
                userFriendStatistic.setJoinCnt(joinCnt);
            }
            Long gamingCnt = doc.getLong("gamingCnt");
            if (gamingCnt != null) {
                userFriendStatistic.setGamingCnt(gamingCnt);
            }
            Long gamingEndCnt = doc.getLong("gamingEndCnt");
            if (gamingEndCnt != null) {
                userFriendStatistic.setGamingEndCnt(gamingEndCnt);
            }
            Long winCnt = doc.getLong("winCnt");
            if (winCnt != null) {
                userFriendStatistic.setWinCnt(winCnt);
            }
            Double avgRaise = doc.getDouble("avgRaise");
            if (avgRaise != null) {
                userFriendStatistic.setAvgRaise(avgRaise);
            }
            ArrayList<String> cardTypes = (ArrayList)doc.get("cardTypes");
            if (cardTypes != null) {
                userFriendStatistic.setCardTypes(cardTypes);
            }
            Date createDate = doc.getDate("createDate");
            if (createDate != null) {
                userFriendStatistic.setCreateTime(createDate);
            }
            Date updateTime = doc.getDate("updateTime");
            if (updateTime != null) {
                userFriendStatistic.setUpdateTime(updateTime);
            }
        }
        return userFriendStatistic;
    }

    private UserFriendStatistic saveUserStatistic(String userId) {
        MongoCollection<Document> statisticCollection = MGDatabase.getInstance().getDB().getCollection(GAME_STATISTIC);
        Document document = new Document();
        document.put("userId", userId);
        document.put("joinCnt", 0l);
        document.put("gamingCnt", 0l);
        document.put("gamingEndCnt", 0l);
        document.put("winCnt", 0L);
        document.put("avgRaise", 0.0d);
        document.put("cardTypes", new HashSet<String>());
        document.put("createTime", new Date());
        document.put("createTime", new Date());
        statisticCollection.insertOne(document);
        ObjectId objectId = document.getObjectId("_id");
        return new UserFriendStatistic(objectId.toString(), userId,  0l,  0l,  0l, 0l, 0.0d , new ArrayList<String>(),
                new Date(), new Date());
    }

}
