package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.user.TableUserSitDownEvent;
import com.tiantian.strangergame.akka.user.TableUserAddBuyInOkEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.manager.model.UserChips;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.*;

/**
 *
 */
public class UserAfterBuyInSitDownHandler implements EventHandler<TableUserAddBuyInOkEvent> {
    @Override
    public void handler(TableUserAddBuyInOkEvent event, ActorRef self,
                        UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        long totalBuyIn = event.getTotalBuyIn();
        int userSitNum = event.getUserSitNum();
        long bigBlind = event.getBigBlind();
        long smallBlind = event.getSmallBlind();
        Object rootEvent = event.getRootEvent();
        ActorRef clientSender = event.getClientRef();
        // 补充筹码
        UserChips userChips = UserChips.load(userId, tableId);
        if (userChips == null) {
            userChips = UserChips.init(userId, tableId, totalBuyIn);
        }
        else  {
            userChips.addAndFlushChips(totalBuyIn);
        }

        TableAllUser tableAllUser = TableAllUser.load(tableId);
        int ret = UserInfHandlerHelper.checkSitDown(tableAllUser, userId, userSitNum);
        if (ret < 0) {
            clientSender.tell(new TableUserSitDownEvent.Response(ret, "", -1), ActorRef.noSender());
            return;
        }

        TableUser tableUser = TableUser.load(userId);
        tableUser.setBuyInCnt("");
        tableUser.save();

        Map<String, String> joinUsers = tableAllUser.getJoinTableUserMap();
        String joinStatus = null;
        if (joinUsers != null && joinUsers.size() > 2) { // 大于3个人默认等待大盲入局
            joinStatus = "wait_blind";
        }
        // 坐下操作
        UserInfHandlerHelper.addUser(tableAllUser, tableUser, userId, tableId, userSitNum + "", joinStatus);
        // 推送增加筹码
        if(totalBuyIn > 0) {
           sendAddChips(userId, userSitNum, tableUser.getNickName(), totalBuyIn, tableAllUser, tableId);
        }
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            tableStatus = TableStatus.newNull();
        }
        String innerId = tableStatus.getInningId() == null ? "" : tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount() == null ? "0" : tableStatus.getIndexCount();
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", indexCount);
        object.put("user_id", userId);
        object.put("room_id", "stranger_room");
        object.put("table_id", tableId);
        object.put("gender", tableUser.getGender() == null ? "0" : tableUser.getGender());
        object.put("sit_num", Integer.parseInt(tableUser.getSitNum()));
        object.put("avatar_url", tableUser.getAvatarUrl() == null ? "" : tableUser.getAvatarUrl());
        object.put("nick_name", tableUser.getNickName());
        object.put("money", userChips.getChips());
        object.put("max_people", 9);
        object.put("sb", smallBlind);
        object.put("bb", bigBlind);
        String id = UUID.randomUUID().toString().replace("-", "");
        List<String> toUsers = new ArrayList<>();

        if (tableAllUser.getOnlineTableUserIds() != null) {
            Collection<String> tableUsers = tableAllUser.getOnlineTableUserIds();
            if (tableUsers.size() > 0) {
                for (String uId : tableUsers) {
                    // 不发给自己
                    if (uId.equals(userId)) {
                        continue;
                    }
                    toUsers.add(uId);
                }
            }
            if (toUsers.size() > 0) {
                // 发送加入游戏消息给其他玩家
                GameUtils.notifyUsers(object, GameEventType.JOIN_ROOM, toUsers, id, tableId);
            }
            UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableId, userId, tableUser.getNickName(),
                    tableUser.getAvatarUrl(), tableUser.getGender());
        }
        //获取桌子的状态
        String status = tableStatus.getStatus();
        // 游戏已经开始过但现在是未开始状态
        if (StringUtils.isNotBlank(status) && GameStatus.READY.name().equalsIgnoreCase(status)) {
            if (joinUsers != null && joinUsers.size() == 2) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
                jsonObject.put("tableId", tableId);
                try {
                    RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), GameConstants.BEGIN_DELAYER_TIME_SECS);
                } catch (TException e) {
                    e.printStackTrace();
                }
            }
        }

        //TODO 判断开始按钮 发送
        if (StringUtils.isBlank(tableStatus.getStatus()) &&
                tableAllUser.getTableSitDownUsers().size() == 2) {
            //TODO 发送给团长
        }
        clientSender.tell(new TableUserSitDownEvent.Response(0, "", userSitNum), ActorRef.noSender());
    }

    private void sendAddChips(String userId, int sitNum, String nickName, long totalBuyIn, TableAllUser tableAllUser,
                              String tableId) {
        // 通知玩家购买
        JSONObject object1 = new JSONObject();
        object1.put("uid", userId);
        object1.put("sn", sitNum);
        object1.put("nick_name", nickName);
        object1.put("buy", totalBuyIn);
        String id = UUID.randomUUID().toString().replace("-", "");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUsers(object1, GameEventType.ADD_CHIPS, tableAllUser.getOnlineTableUserIds(), id, tableId);
    }
}
