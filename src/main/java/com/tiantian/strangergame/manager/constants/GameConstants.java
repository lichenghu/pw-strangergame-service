package com.tiantian.strangergame.manager.constants;

/**
 *
 */
public interface GameConstants {
    String TASK_EVENT = "strangerTaskEvent";
    long BEGIN_DELAYER_TIME_SECS = 2000;

    long  DANDB_DELAYER_TIME = 3000;
    // 下注的超时时间
    long  BET_DELAYER_TIME = 15000;

    // 下注的超时时间
    long  BUY_SAFE_DELAYER_TIME = 49000; // 45 + 4

    // 桌子状态超时时间
    long TABLE_TIMEOUT_MILL = 120000;
    //  下一个状态任务延时
    long STATUS_DELAYER_TIME = 700;

    // 桌子状态检查延迟任务
    long TABLE_STATUS_DELAYER_TIME = 10000;

    // 桌子状态玩家筹码后开始dandb延迟任务
    long AFTER_CHECK_CHIPS_DELAYER_TIME =  0; // 1000;

    // 桌子上在线玩家状态检查延迟任务
    long TABLE_ONLINE_USER_DELAYER_TIME = 120000;

    int MAX_NOT_OPERATE_COUNT = 2;

    String GROUP_TABLE_STATUS_KEY = "stranger_table_status:";
    // 局桌子玩家
    String GROUP_TABLE_USER_KEY = "stranger_table_user_key:";
    String ROUTER_KEY = "router_key:";
    // 桌子上玩家
    String USER_GROUP_TABLE_KEY = "user_stranger_table_key:";
    String USER_GROUP_GAME_KEY = "user_stranger_games_key:";
    // 桌子在线玩家
    String GROUP_TABLE_USER_ONLINE_KEY = "stranger_table_user_online_key:";
    // 局桌子坐下过的玩家
    String GROUP_TABLE_USER_SIT_DOWN_KEY = "stranger_table_user_sit_down_key:";

    String USER_GROUP_GMAE_KEY = "user_stranger_games_key:";

    String USER_EVER_JOIN_KEY = "user_stranger_ever_join_key:";

    // map key
    String DATA_STR = "dataStr";
    String EVENT = "event";
    String SERVER_ID = "serverId";
    String TO = "to";
    // 消息来源
    String MSG_SOURCE = "msg_source";
    String UNIQUE_ID = "unique_id";
    // 事件
    String USER_JOIN = "userJoin";
    String USER_RAISE = "userRaise";
    String USER_FOLD = "userFold";
    String USER_ALLIN = "userAllin";
    String USER_CHECK = "userCheck";
    String USER_STAND_UP = "userStandUp";
    String USER_CALL = "userCall";
    String USER_EXIT = "userExit";
    String USER_FAST_RAISE = "userFastRaise";
    String SHOW_CARDS = "showCards";
    String ADD_TIMES = "addTimes"; // 蓄时
    String TABLE_INFO = "tableInfo";
    String USER_SIT_DOWN = "userSitDown";
    String START_GAME = "startGame";
    String APPLY_BUY_IN = "userApplyBuyIn";
    String CHANGE_SIT_DOWN_TYPE = "userChangeSitDownType";
    String AGREE_BUY_IN = "agreeCreditMoney";
    String USER_STATUS = "userStatus";
    String USER_BUY_SAFE = "userBuySafe";
    // 任务
    String TEST_BET = "testBet"; // 检测下注
    //
    String TEST_STATUS = "testStatus"; // 检测下个任务

    //
    String TEST_LAST_USER = "testLastUer"; // 检测桌子上最后一个玩家

    //
    String TABLE_STATUS_CHECK = "tableStatusCheck";

    // 检测桌子上在线玩家的状态，掉线需要清楚掉
    String TABLE_ONLINE_USER_CHECK = "tableOnlineUserCheck";

    // 玩家状态
    String USER_STATUS_GAMING = "gaming";

    // 筹码校验
    String CHECK_CHIPS = "checkChips";

    String CLOSE_GAME = "closeGame";

    String USER_CHANGE_CHIPS = "userChangeChips";

    String LEADER_ADD_MONEY = "leaderAddMoney";

    String USER_CHANGE_CHIPS_OK = "userChangeChipsOk";
    String EMOJI = "emoji";
}
