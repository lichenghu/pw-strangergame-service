package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.user.TableUserAgreeCreditMoneyEvent;
import com.tiantian.strangergame.akka.event.UserAgreeCreditMoneyEvent;
import com.tiantian.strangergame.handlers.EventHandler;
/**
 *
 */
public class UserAgreeCreditMoneyHandler implements EventHandler<TableUserAgreeCreditMoneyEvent> {
    @Override
    public void handler(TableUserAgreeCreditMoneyEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        // 直接发送到UserActor中处理
        self.tell(new UserAgreeCreditMoneyEvent(userId, tableId, sender), ActorRef.noSender());
    }
}
