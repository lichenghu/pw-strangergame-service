package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.common.collect.Lists;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.fc.thrift.core.StrangerWinLoseLog;
import com.tiantian.stranger.akka.result.*;
import com.tiantian.stranger.akka.user.TableUserTableInfEvent;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.manager.texas.PokerManager;
import com.tiantian.strangergame.manager.texas.PokerOuts;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class UserTableInfoHandler implements EventRetHandler<TableUserTableInfEvent> {
    @Override
    public Object handler(TableUserTableInfEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        TableInfoResult tableInfo = new TableInfoResult();
        TableUser tableUser = TableUser.load(userId);
        if (tableUser.isNull()) {
            return tableInfo;
        }
        String userTableId = tableUser.getTableId();
        if (StringUtils.isBlank(userTableId)) {
            return tableInfo;
        }
        TableStatus tableStatus = TableStatus.load(userTableId);
        if (tableStatus == null) {
            tableStatus = TableStatus.newNull();
        }
        int btn = -1;
        if (StringUtils.isNotBlank(tableStatus.getButton())) {
            btn = Integer.parseInt(tableStatus.getButton());
        }
        tableInfo.setButton(btn);
        int small = -1;
        if (StringUtils.isNotBlank(tableStatus.getSmallBlindNum())) {
            small = Integer.parseInt(tableStatus.getSmallBlindNum());
        }
        tableInfo.setSmallBtn(small);
        int smallMoney = 0;
        if (StringUtils.isNotBlank(tableStatus.getSmallBlindMoney())) {
            smallMoney = Integer.parseInt(tableStatus.getSmallBlindMoney());
        }
        tableInfo.setSmallBtnMoney(smallMoney);
        int big = -1;
        if (StringUtils.isNotBlank(tableStatus.getBigBlindNum())) {
            big = Integer.parseInt(tableStatus.getBigBlindNum());
        }
        tableInfo.setBigBtn(big);
        int bigMoney = 0;
        if (StringUtils.isNotBlank(tableStatus.getBigBlindMoney())) {
            bigMoney = Integer.parseInt(tableStatus.getBigBlindMoney());
        }
        tableInfo.setBigBtnMoney(bigMoney);
        TableAllUser tableAllUser = TableAllUser.load(userTableId);
        // 判断当前玩家是否正在游戏里面
        List<UserInfoResult> userInfoList = getUserInfos(tableAllUser.getJoinTableUserMap(), userTableId, userId);
        tableInfo.setUsers(userInfoList);
        List<Long> poolList = tableStatus.betPoolList();
        tableInfo.setPool(StringUtils.join(poolList, ","));

        int currentBet = 0;
        String betUserId = null;
        Map<String, String> gamingMap = tableAllUser.getJoinTableUserMap();
        if (StringUtils.isNotBlank(tableStatus.getCurrentBet())) {
            currentBet = Integer.valueOf(tableStatus.getCurrentBet());
            betUserId = gamingMap.get(tableStatus.getCurrentBet());
        }
        TableUser betTableUser = null;
        if (StringUtils.isNotBlank(betUserId)) {
            betTableUser = TableUser.load(betUserId);
        }
        tableInfo.setCurBetSit(currentBet);

        String currentBetTimes = tableStatus.getCurrentBetTimes();
        long leftSecs = 0;
        if (StringUtils.isNotBlank(currentBetTimes)) {
            String status = tableStatus.getSitBetStatusBySitNum(tableStatus.getCurrentBet());
            if (status != null && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                long times = System.currentTimeMillis() - Long.parseLong(currentBetTimes);
                long total = GameConstants.BET_DELAYER_TIME;
                if (betTableUser != null && StringUtils.isNotBlank(betTableUser.getTotalSecs())) {
                    total = Long.parseLong(betTableUser.getTotalSecs()) * 1000;
                }
                leftSecs = Math.max(0, total - times);
            }
        }
        tableInfo.setLeftSecs(leftSecs/1000);

        String usersBetsLog = tableStatus.getUsersBetsLog();
        List<UserBetResult> userBets = new ArrayList<>();
        if (StringUtils.isNotBlank(usersBetsLog)) {
            List<UserBetLog> userBetLogs = JSON.parseArray(usersBetsLog, UserBetLog.class);
            for (UserBetLog userBetLog : userBetLogs) {
                if (!gamingMap.containsKey(userBetLog.getSitNum())) {
                    continue;
                }
                UserBetResult userBet = new UserBetResult();
                userBet.setSn(Integer.valueOf(userBetLog.getSitNum()));
                userBet.setBet(userBetLog.getRoundChips());
                userBets.add(userBet);
            }
        }
        tableInfo.setUserBets(userBets);
        long totalSec = 0;
        if (betTableUser != null) {
            if (!betTableUser.isNull()) {
                String totalSecStr = betTableUser.getTotalSecs();
                if (StringUtils.isBlank(totalSecStr)) {
                    totalSec = GameConstants.BET_DELAYER_TIME / 1000l;
                }
                else {
                    totalSec = Long.parseLong(totalSecStr);
                }
            }
        }
        tableInfo.setTotalSecs(totalSec);
        // 牌局信息
        if (StringUtils.isNotBlank(tableStatus.getDeskCards())) {
            List<String> deskCards = JSON.parseObject(tableStatus.getDeskCards(), List.class);
            tableInfo.setDeskCards(StringUtils.join(deskCards, ","));
        }
        List<Map<String, Object>> sitBetStatusMap = tableStatus.allUserBetStatus();
        List<UserStatusResult> userStatuses = new ArrayList<>();

        // 设置大盲注状态，开始时候默认设置了大盲注状态
        UserBetLog bigUserBetLog = tableStatus.getUserBetLog(tableStatus.getBigBlindNum());
        if(bigUserBetLog != null) {
            long chips = bigUserBetLog.getRoundChips() + bigUserBetLog.getTotalChips();
            if (chips != Long.parseLong(tableStatus.getBigBlindMoney())) { // 玩家没有下过注
                String bigStatus = tableStatus.getSitBetStatusBySitNum(tableStatus.getBigBlindNum());
                // 状态为初始状态
                if (GameEventType.CALL.equalsIgnoreCase(bigStatus)) {
                    for (Map<String, Object> map : sitBetStatusMap) {
                        if (map.containsKey(tableStatus.getBigBlindNum())) {
                            map.put(tableStatus.getBigBlindNum(), "");
                            break;
                        }
                    }
                }
            }
        }

        tableInfo.setUserStatus(userStatuses);
        String userCards = "";
        Map<String, String> userCardsMap = tableStatus.getUsersCards();
        String userCanOps = "";
        String cardLevel = "";
        String sitNum = tableUser.getSitNum();
        if (userCardsMap != null) {
            if (StringUtils.isNotBlank(sitNum)) {
                userCards = userCardsMap.get(sitNum);
                if (sitNum.equalsIgnoreCase(tableStatus.getCurrentBet())) {
                    String[] ops = tableStatus.getUserCanOps(tableUser.getUserId(), sitNum);
                    userCanOps = StringUtils.join(ops, ",");
                }
                PokerOuts pokerOuts = PokerManager.getPokerOuts(userCards, tableStatus.getDeskCardList());
                if (pokerOuts != null) {
                    cardLevel = pokerOuts.getLevel() + "";
                }
            }
        }
        tableInfo.setHandCards(userCards == null ? "" : userCards);
        tableInfo.setCardLevel(cardLevel);
        tableInfo.setUserCanOps(userCanOps);
        String waitStatus = "";
        if ("wait_blind".equalsIgnoreCase(tableUser.getStatus()) ||
                "bet_blind".equalsIgnoreCase(tableUser.getStatus())) {
            waitStatus = tableUser.getStatus();
        }
        tableInfo.setUserWaitStatus(waitStatus);
        tableInfo.setPwd(StringUtils.isBlank(userCanOps) || tableStatus.getPwd() == null  ? "" : tableStatus.getPwd());
        String innerId = tableStatus.getInningId() == null ? "" : tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount() == null ? "0" : tableStatus.getIndexCount();
        tableInfo.setInnerId(innerId);
        tableInfo.setInnerCnt(indexCount);
        tableInfo.setGameStatus(StringUtils.isBlank(tableStatus.getStatus()) ? "" : tableStatus.getStatus().toLowerCase());
        tableInfo.setSafeWaitSec(tableStatus.hasSafe() ? Math.min(GameConstants.BUY_SAFE_DELAYER_TIME / 1000 - 4, 60l) : -1);

        Map<String, Map<String, Object>> outsMap = tableStatus.getOutsMap();
        List<UserSafeResult> userSafeResults = Lists.newArrayList();
        List<String> userSafeSits = Lists.newArrayList();
        if (outsMap != null) {
            Set<Map.Entry<String, Map<String, Object>>> entries = outsMap.entrySet();
            for (Map.Entry<String, Map<String, Object>> entry : entries) {
                 String outSitNum = entry.getKey();
                 Map<String, Object> mapVal = entry.getValue();
                 String userOp = (String) mapVal.get("user_op");
                 long times = (long) mapVal.get("times");
                 long leftSafeSecs = tableInfo.getSafeWaitSec() - (System.currentTimeMillis() - times) / 1000;
                 // 没有操作过,并且没有超时
                 if ("0".equalsIgnoreCase (userOp) && leftSafeSecs > 0 ) {
                     JSONArray prices = (JSONArray) mapVal.get("price");
                     JSONArray outsArray = (JSONArray) mapVal.get("outs");
                     UserSafeResult userSafeResult = new UserSafeResult();
                     userSafeResult.setLeftSecs(leftSafeSecs);
                     List<String> safePrices = Lists.newArrayList();
                     if (outSitNum.equalsIgnoreCase(sitNum)) {
                         for (Object price : prices) {
                              String priceStr = (String)price;
                              safePrices.add(priceStr);
                         }
                     }
                     userSafeResult.setPrice(safePrices);
                     List<String> safeOuts = Lists.newArrayList();
                     if (outSitNum.equalsIgnoreCase(sitNum)) {
                         for (Object outs : outsArray) {
                             String outsStr = (String) outs;
                             safeOuts.add(outsStr);
                         }
                     }
                     userSafeResult.setOuts(safeOuts);
                     userSafeResult.setSitNum(Integer.valueOf(outSitNum));
                     //
                     long leftSafe = 0;
                     try {
                         if (outSitNum.equalsIgnoreCase(sitNum)) {
                             StrangerWinLoseLog strangerWinLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(tableStatus.getTableId(), userId);
                             if (strangerWinLoseLog != null) {
                                 leftSafe = strangerWinLoseLog.getMoneyLeft();
                             }
                         }
                     }
                     catch (Exception e) {
                         e.printStackTrace();
                     }
                         userSafeResult.setLeftSafe(leftSafe);
                     userSafeResults.add(userSafeResult);
                     userSafeSits.add(outSitNum);
                 }
            }
            if (!userSafeResults.isEmpty()) {
                tableInfo.setUserSafes(userSafeResults);
            }
        }
        boolean needShow = tableStatus.needShowAllCards();
        if (sitBetStatusMap != null) {
            for (Map<String, Object> statusMap : sitBetStatusMap) {
                UserStatusResult userStatus = new UserStatusResult();
                Integer sn = (Integer)statusMap.get("sn");
                String status = (String)statusMap.get("status");
                // 在游戏中
                if (gamingMap.containsKey(sn.toString())) {
                    userStatus.setSn(sn);
                    String otherUserCards = "";
                    if (needShow && StringUtils.isNotBlank(status) && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                        otherUserCards = tableStatus.getUserHandCards(sn.toString());
                    }
                    userStatus.setHandCards(otherUserCards);
                    // 判断当前玩家是否在购买保险
                    if (userSafeSits.contains(sn.toString())) {
                        status = "buy_safe";
                    }
                    userStatus.setStatus(status);
                    userStatuses.add(userStatus);
                }
            }
        }
        return tableInfo;
    }

    private List<UserInfoResult> getUserInfos(Map<String, String> gamingSitUserMap, String tableId,
                                              String userId) {
        List<UserInfoResult> userInfos = new ArrayList<>();
        boolean isStandUp = true;
        if (gamingSitUserMap != null) {
            Set<Map.Entry<String, String>> entries = gamingSitUserMap.entrySet();
            for (Map.Entry<String, String> entry : entries) {
                UserInfoResult userInfo = new UserInfoResult();
                String $sitNum = entry.getKey();
                String $userId = entry.getValue();
                if ($userId.equalsIgnoreCase(userId)) {
                    isStandUp = false;
                }
                TableUser tableUser = TableUser.load($userId);
                userInfo.setUserId($userId);
                userInfo.setSitNum(Integer.parseInt($sitNum));
                userInfo.setNickName(tableUser.getNickName());
                userInfo.setAvatarUrl(tableUser.getAvatarUrl() == null ? "" : tableUser.getAvatarUrl());
                userInfo.setPlaying(1);
                userInfo.setGender(tableUser.getGender() == null ? "0" : tableUser.getGender());
                UserChips $userChips = UserChips.load($userId, tableId);
                if ($userChips != null) {
                    userInfo.setMoney($userChips.getChips());
                }
                userInfos.add(userInfo);
            }
        }
        // 玩家是站起状态
        if (isStandUp) {
            TableUser tableUser = TableUser.load(userId);
            UserInfoResult userInfo = new UserInfoResult();
            userInfo.setUserId(userId);
            userInfo.setSitNum(-1);
            userInfo.setNickName(tableUser.getNickName());
            userInfo.setAvatarUrl(tableUser.getAvatarUrl() == null ? "" : tableUser.getAvatarUrl());
            userInfo.setGender(tableUser.getGender() == null ? "0" : tableUser.getGender());
            userInfo.setPlaying(0);
            UserChips $userChips = UserChips.load(userId, tableId);
            if ($userChips != null) {
                userInfo.setMoney($userChips.getChips());
            }
            userInfos.add(userInfo);
        }
        return userInfos;
    }
}
