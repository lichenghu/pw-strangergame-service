package com.tiantian.strangergame.akka.actor;

import akka.actor.UntypedActor;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.strangergame.akka.event.TableDbUpdateCntEvent;
import com.tiantian.strangergame.data.mongodb.MGDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TableDbActor  extends UntypedActor {
    static Logger LOG = LoggerFactory.getLogger(TableDbActor.class);
    private static final String GROUPS_TABLE = "stranger_groups";
    private String tableId;

    public TableDbActor(String tableId) {
        this.tableId = tableId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if(message instanceof TableDbUpdateCntEvent) {
            String tableId = ((TableDbUpdateCntEvent) message).tableId();
            int cnt = ((TableDbUpdateCntEvent) message).getCnt();
            boolean ret = updateStatices(tableId, cnt);
            if (!ret) {
                LOG.error("update group totalPlayCnt fail, groupId is {}", tableId);
            }
        }
        else {
            unhandled(message);
        }
    }

    public void postStop() {
        LOG.info("TableDb Actor Closed, table_id is {}", tableId);
    }

    private boolean updateStatices(String groupId,int cnt) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(GROUPS_TABLE);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(groupId));
        // 手数+1
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("totalPlayCnt", cnt);

        BasicDBObject updateSetValue = new BasicDBObject("$inc", updatedValue);
        UpdateResult result = groupsCollection.updateOne(updateCondition, updateSetValue);
        return (result.getModifiedCount() > 0);
    }
}
