package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.akka.event.GameOver;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 *
 */
public class TableCheckStatusHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableCheckStatusHandler.class);

    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        try {
            TableStatus tableStatus = TableStatus.load(tableId);
            if (tableStatus == null) {
                return;
            }
            // 不是需要检测局的任务
            if (StringUtils.isNotBlank(tableStatus.getInningId())
                    && !tableStatus.getInningId().equalsIgnoreCase(inningId)) {
                return;
            }
            // 判断游戏是否结束
            if (StringUtils.isNotBlank(tableStatus.getStatus())
                             && GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus()) &&
                    (System.currentTimeMillis() > Long.parseLong(tableStatus.getEndTimes()) + 600000)) {
                // 发送结束信息 销毁actor
                context.parent().tell(new GameOver(tableId), ActorRef.noSender());
                FriendCoreIface.instance().iface().strangerGameOver(tableId);
                return;
            }
            // 判断桌子是否超时
            if (StringUtils.isNotBlank(tableStatus.getStatus()) && tableStatus.getLastUpdateTimes() != null && !GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())
                    && Long.parseLong(tableStatus.getLastUpdateTimes())
                    < (System.currentTimeMillis() - GameConstants.TABLE_TIMEOUT_MILL)) {
                //TODO 根据房间开启条件判断是否需要执行任务
                //获取桌子玩家人数
                TableAllUser tableAllUser = TableAllUser.load(tableId);
                if(tableAllUser.getJoinTableUserMap() != null && tableAllUser.getJoinTableUserMap().size() < 2) {
                    LOG.info("user not enough");
                    // 人数不齐
                    return;
                }
                //返还筹码
                tableStatus.rebatesChips();
                tableStatus.setStatus(GameStatus.READY.name()); // 设置游戏准备然后重新开始
                tableStatus.saveNotAddCnt();

                // 发送通知
                sendErrMsg(tableId, tableStatus.getInningId(), tableStatus.getIndexCount());
                //重新开始
                JSONObject newJsonObj = event.getParams();
                newJsonObj.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
                newJsonObj.put("tableId", tableId);
                RedisTaskIface.instance().iface().pushTask(newJsonObj.toJSONString(), 0); // 直接重新开始
                LOG.error("status error, TableStatus :" + JSON.toJSONString(tableStatus));
                return;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        GameUtils.tableStatusTask(inningId, tableId);
    }
    private void sendErrMsg(String tableId, String inningId, String indexCount) {
        JSONObject object = new JSONObject();
        object.put("inner_id", inningId);
        object.put("inner_cnt", indexCount);
        String id = UUID.randomUUID().toString().replace("-", "");
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        GameUtils.notifyUsers(object, GameEventType.ERROR_INFO, tableAllUser.getOnlineTableUserIds(), id, tableId);
    }
}
