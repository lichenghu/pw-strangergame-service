package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.user.TableUserStartGameEvent;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 *
 */
public class UserStartGameHandler implements EventRetHandler<TableUserStartGameEvent> {
    @Override
    public Object handler(TableUserStartGameEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        long buyIn = event.getBuyIn();
        long smallBlind = event.getSmallBlind();
        long bigBlind = event.getBigBlind();
        long endTimes = event.getEndTimes();
        String masterId = event.getMasterId();
        long maxUsers = event.getMaxUsers();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Set<String> sitDowns = tableAllUser.getTableSitDownUsers();
        if (sitDowns == null || sitDowns.size() < 2) { // 达到2最少2个人
            return "-1"; // 人数不足
        }

        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus != null && StringUtils.isNotBlank(tableStatus.getStatus())) { // 已经开始了
           return "-2"; // 已经开启过
        }
        if (tableStatus == null || tableStatus.isNull()) {
            tableStatus = TableStatus.init(tableId);
        }
        tableStatus.setBuyIn(buyIn + "");
        tableStatus.setSmallBlindMoney(smallBlind + "");
        tableStatus.setBigBlindMoney(bigBlind + "");

        tableStatus.setStatus(GameStatus.READY.name());
        // 设置结束时间
        tableStatus.setEndTimes(endTimes + "");
        tableStatus.setMaxUsers(maxUsers + "");
        tableStatus.saveNotAddCnt();

        // 发送一个延迟2s的确定庄家大小盲注任务
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
        jsonObject.put("tableId", tableId);
        jsonObject.put("masterId", masterId);
        List<String> tableSitList = new ArrayList<>(sitDowns);
        Random random = new Random();
        int randomIndex = random.nextInt(tableSitList.size());
        jsonObject.put("randomBtn", tableSitList.get(randomIndex));
        try {
            RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), GameConstants.BEGIN_DELAYER_TIME_SECS);
        } catch (TException e) {
            e.printStackTrace();
        }
        return "0"; //成功
    }
}
