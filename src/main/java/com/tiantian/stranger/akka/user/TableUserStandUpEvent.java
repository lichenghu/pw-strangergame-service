package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserStandUpEvent extends TableUserEvent {
    private String userId;
    private String tableId;

    public TableUserStandUpEvent(String userId, String tableId) {
        super(true);
        this.userId = userId;
        this.tableId = tableId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userStandUp";
    }

    public String getUserId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }
}
