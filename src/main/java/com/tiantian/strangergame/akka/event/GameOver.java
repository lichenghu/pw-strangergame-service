package com.tiantian.strangergame.akka.event;

/**
 *
 */
public class GameOver implements Game {
    private String tableId;

    public GameOver(String tableId) {
        this.tableId = tableId;
    }

    public String tableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }
}
