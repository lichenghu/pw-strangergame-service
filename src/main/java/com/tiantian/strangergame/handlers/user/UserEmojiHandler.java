package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.stranger.akka.user.TableUserEmojiEvent;

import java.util.UUID;

/**
 *
 */
public class UserEmojiHandler implements EventHandler<TableUserEmojiEvent> {
    @Override
    public void handler(TableUserEmojiEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String fromUserId = event.getUserId();
        String toUserId = event.getToUserId();
        String emoji = event.getEmoji();
        String tableId = event.getTableId();
        String innerId = "";
        String innerCnt = "";
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        //通知玩家操作成功
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("from_user_id", fromUserId);
        object.put("to_user_id", toUserId);
        object.put("emoji", emoji);
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser == null) {
            return;
        }
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.EMOJI, tableAllUser.getOnlineTableUserIds(), id
                , tableStatus.getTableId());
    }
}
