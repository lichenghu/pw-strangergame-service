package com.tiantian.strangergame.akka.event;

/**
 *
 */
public class UserWinSafeEvent implements UserEvent {
    private String userId;
    private String id;
    private String tableId;
    private long winSafe;

    public UserWinSafeEvent(String userId, String id, String tableId, long winSafe) {
        this.userId = userId;
        this.id = id;
        this.tableId = tableId;
        this.winSafe = winSafe;
    }

    @Override
    public String userId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }

    public String getId() {
        return id;
    }

    public long getWinSafe() {
        return winSafe;
    }

    public void setWinSafe(long winSafe) {
        this.winSafe = winSafe;
    }
}
