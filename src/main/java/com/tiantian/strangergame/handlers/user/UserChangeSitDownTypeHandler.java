package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.user.TableUserChangeSitDowTypeEvent;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.model.TableUser;

/**
 *
 */
public class UserChangeSitDownTypeHandler implements EventRetHandler<TableUserChangeSitDowTypeEvent> {
    @Override
    public Object handler(TableUserChangeSitDowTypeEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String type = event.getType();
        TableUser tableUser = TableUser.load(userId);
        if (tableUser == null || tableUser.isNull()) {
            return new TableUserChangeSitDowTypeEvent.Response(-1, "玩家已经退出局");
        }
        String status = "wait_blind";
        if ("1".equalsIgnoreCase(type)) {
            status = "bet_blind";
        }
        String userStatus = tableUser.getStatus();
        if ("wait_blind".equalsIgnoreCase(userStatus)
                || "bet_blind".equalsIgnoreCase(userStatus)) {
            tableUser.setStatus(status);
            tableUser.save();
        }
        else {
            System.out.println("user status is not right" + tableUser.getUserId() + ":" + tableUser.getStatus());
        }
        return new TableUserChangeSitDowTypeEvent.Response(0, "设置成功");
    }
}
