package com.tiantian.stranger.akka.result;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public class UserSafeResult implements Serializable {
    private int sitNum;
    private long leftSecs;
    private List<String> price;
    private List<String> outs;
    private long leftSafe;

    public int getSitNum() {
        return sitNum;
    }

    public void setSitNum(int sitNum) {
        this.sitNum = sitNum;
    }

    public long getLeftSecs() {
        return leftSecs;
    }

    public void setLeftSecs(long leftSecs) {
        this.leftSecs = leftSecs;
    }

    public List<String> getPrice() {
        return price;
    }

    public void setPrice(List<String> price) {
        this.price = price;
    }

    public List<String> getOuts() {
        return outs;
    }

    public void setOuts(List<String> outs) {
        this.outs = outs;
    }

    public long getLeftSafe() {
        return leftSafe;
    }

    public void setLeftSafe(long leftSafe) {
        this.leftSafe = leftSafe;
    }
}
