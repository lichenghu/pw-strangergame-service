package com.tiantian.strangergame.akka.event;

import akka.actor.ActorRef;

/**
 *
 */
public class UserAgreeCreditMoneyEvent implements UserEvent {
    private String userId;
    private String tableId;
    private  ActorRef clientSender;

    public UserAgreeCreditMoneyEvent(String userId, String tableId, ActorRef clientSender) {
        this.userId = userId;
        this.tableId = tableId;
        this.clientSender = clientSender;
    }

    @Override
    public String userId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }

    public ActorRef getClientSender() {
        return clientSender;
    }
}
