package com.tiantian.strangergame.data.mongodb;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import com.tiantian.strangergame.settings.MGConfig;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MGDatabase {
    static Logger LOG = LoggerFactory.getLogger(MGDatabase.class);
    private MongoClient mongoClient;
    private MGDatabase() {
    }
    private static class MGDatabaseHolder{
        private final static MGDatabase instance = new MGDatabase();
    }

    public static MGDatabase getInstance() {
        return MGDatabaseHolder.instance;
    }

    public void init() {

        // 从配置文件中获取属性值
        String ip = MGConfig.getInstance().getHost();
        int port = MGConfig.getInstance().getPort();

        // or, to connect to a replica set, with auto-discovery of the primary, supply a seed list of members
        //  List<ServerAddress> listHost = Arrays.asList(new ServerAddress("localhost", 27017), new ServerAddress("localhost", 27018));
        // mongoClient = new MongoClient(listHost);

        // 大部分用户使用mongodb都在安全内网下，但如果将mongodb设为安全验证模式，就需要在客户端提供用户名和密码：
        // boolean auth = db.authenticate(myUserName, myPassword);
        MongoClientOptions.Builder options = new MongoClientOptions.Builder();
        // options.autoConnectRetry(true);// 自动重连true
        // options.maxAutoConnectRetryTime(10); // the maximum auto connect retry time
        options.connectionsPerHost(300);// 连接池设置为300个连接,默认为100
        options.connectTimeout(15000);// 连接超时，推荐>3000毫秒
        options.maxWaitTime(5000); //
        options.socketTimeout(0);// 套接字超时时间，0无限制
        options.threadsAllowedToBlockForConnectionMultiplier(5000);// 线程队列数，如果连接线程排满了队列就会抛出“Out of semaphores to get db”错误。
        options.writeConcern(WriteConcern.SAFE);//
        MongoClientOptions mongoOptions = options.build();
        try {
            //数据库连接实例
            mongoClient = new MongoClient(new ServerAddress(ip, port), mongoOptions);
        } catch (MongoException e){
            e.printStackTrace();
        }
    }

    public MongoDatabase getDB() {
        String dbName = MGConfig.getInstance().getDb();
        if (StringUtils.isNotBlank(dbName)) {
            MongoDatabase database = mongoClient.getDatabase(dbName);
            return database;
        }
        return null;
    }

}
