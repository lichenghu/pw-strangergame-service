package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.user.LeaderAddMoneyEvent;
import com.tiantian.stranger.akka.user.TableUserExchangeChipsEvent;
import com.tiantian.stranger.akka.user.TableUserExchangeChipsOkEvent;
import com.tiantian.strangergame.akka.event.UserExchangeChipsEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.manager.model.UserChips;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 玩家把保证金兑换成筹码
 */
public class UserExchangeChipsOkHandler implements EventHandler<TableUserExchangeChipsOkEvent> {

    @Override
    public void handler(TableUserExchangeChipsOkEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        long buyInCnt = event.getBuyInCnt();
        long onBuyIn = event.getOneBuyIn();
        ActorRef clientSender = event.getClientRef();
        System.out.println("onBuyIN===" + onBuyIn);
        System.out.println("buyInCnt===" + buyInCnt);
        TableUser tableUser = TableUser.load(userId);
        if (!tableUser.isNull() && StringUtils.isNotBlank(tableUser.getSitNum())) {
            System.out.println("tableUser===not null");
            int oldBuy = 0;
            String oldBuyInCnt = tableUser.getBuyInCnt();
            if (StringUtils.isNotBlank(oldBuyInCnt)) {
                oldBuy = Integer.parseInt(oldBuyInCnt);
            }
            tableUser.setBuyInCnt((buyInCnt + oldBuy) + "");
            tableUser.save();
        }
        else {
            //玩家已经退出或者还没有坐下则需要直接加上
            UserChips userChips = UserChips.load(userId, tableId);
            System.out.println("UserChips== " + userId + "----" + tableId);
            if (userChips == null) {
                System.out.println("UserChips===is null");
                UserChips.init(userId, tableId, buyInCnt * onBuyIn);
            }
            else {
                System.out.println("UserChips===is not null");
                System.out.println("UserChips===is not null" + buyInCnt * onBuyIn);
                userChips.addAndFlushChips(buyInCnt * onBuyIn);
            }
        }

        clientSender.tell(new TableUserExchangeChipsEvent.Response(0, "增加成功"), ActorRef.noSender());
    }
}
