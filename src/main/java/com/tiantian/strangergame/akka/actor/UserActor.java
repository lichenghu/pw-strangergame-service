package com.tiantian.strangergame.akka.actor;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.fc.thrift.core.FcResponse;
import com.tiantian.fc.thrift.core.StrangerWinLoseLog;
import com.tiantian.stranger.akka.user.*;
import com.tiantian.strangergame.akka.event.*;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.utils.GameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class UserActor extends UntypedActor {
    static Logger LOG = LoggerFactory.getLogger(UserActor.class);
    private String userId;

    public UserActor(String userId) {
        this.userId = userId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof UserUpdateStaticesEvent) {
            // 更新统计信息
            String tableId = ((UserUpdateStaticesEvent) message).getTableId();
            long addChips = ((UserUpdateStaticesEvent) message).getAddChips();
            long leftChips = ((UserUpdateStaticesEvent) message).getLeftChips();
            FriendCoreIface.instance().iface().updateStrangerWinLoseLogs(tableId, userId, addChips, leftChips);
        }
        else if (message instanceof UserAgreeCreditMoneyEvent) { // 团长同意玩家买入额度, 更新mongo买入逻辑
            // 更新玩家买入
            String tableId = ((UserAgreeCreditMoneyEvent) message).getTableId();
            String userId = ((UserAgreeCreditMoneyEvent) message).userId();
            ActorRef clientSender = ((UserAgreeCreditMoneyEvent) message).getClientSender();
            StrangerWinLoseLog strangerWinLoseLog =FriendCoreIface.instance().iface()
                                                    .getUserStrangerGroupWinLoseLog(tableId, userId);
            if (strangerWinLoseLog == null || strangerWinLoseLog.getAppMoneyBuyIn() <= 0) {
                //没有买入
                clientSender.tell(new TableUserAgreeCreditMoneyEvent.Response(-1, "没有买入信息"), ActorRef.noSender());
                return;
            }
            FcResponse fcResponse = FriendCoreIface.instance().iface().agreeStrangerDeposit(tableId, userId);
            if (!"0".equalsIgnoreCase(fcResponse.getStatus())) {
                clientSender.tell(new TableUserAgreeCreditMoneyEvent.Response(-1, "更新买入信息失败"), ActorRef.noSender());
                return;
            }
            clientSender.tell( new TableUserAgreeCreditMoneyEvent.Response(0, "增加成功"), ActorRef.noSender());
            // 通知玩家操作成功
            JSONObject object1 = new JSONObject();
            object1.put("num",  strangerWinLoseLog.getAppMoneyBuyIn());
            GameUtils.notifyUser(object1, "agree_ok", userId, tableId);
        }
        else if (message instanceof UserApplyCreditEvent) { // 玩家申请买入保证金
            String userId = ((UserApplyCreditEvent) message).getUserId();
            String tableId = ((UserApplyCreditEvent) message).getTableId();
            long maxBuyIn = ((UserApplyCreditEvent) message).getMaxBuyIn();
            long buyIn = ((UserApplyCreditEvent) message).getBuyIn();
            ActorRef clientRef = ((UserApplyCreditEvent) message).getClientSender();
            String masterId = ((UserApplyCreditEvent) message).getMasterId();
            FcResponse response = FriendCoreIface.instance().iface().applicationStrangerDeposit(tableId, userId, buyIn, maxBuyIn);
            int status = response == null ? -1 : Integer.parseInt(response.getStatus());
            String msg = response == null ? "" : response.getErrMsg();
            int retStatus = status;
            if (status == -1) {
                retStatus = -3;
                msg = "已经申请过";
            }
            if (status == -2) {
                retStatus = -4;
                msg = "没有申请相关记录";
            }
            if (status == -3) {
                retStatus = -5;
                msg = "申请失败";
            }
            clientRef.tell(new TableUserApplyCreditEvent.Response(retStatus, msg), ActorRef.noSender());
            if(retStatus >= 0) { // 成功
                JSONObject $object = new JSONObject();
                $object.put("group_id", tableId);
                $object.put("user_id", userId);
                $object.put("buy_in", buyIn);
                // 发送其他玩家的消息
                GameUtils.notifyUser($object, GameEventType.USER_APPLY_CREDIT, masterId, tableId);
            }
        }
        else if (message instanceof UserWinSafeEvent) {  // 玩家赢取了保险
            String userId = ((UserWinSafeEvent) message).userId();
            String tableId = ((UserWinSafeEvent) message).getTableId();
            String id = ((UserWinSafeEvent) message).getId();
            long win = ((UserWinSafeEvent) message).getWinSafe();
            FcResponse fcResponse =FriendCoreIface.instance().iface().addUserSafeWin(tableId, userId, win);
            if ("0".equalsIgnoreCase(fcResponse.getStatus())) {
                FcResponse fcResponse2 = FriendCoreIface.instance().iface().updateSafeBuyLogWin(id);
                if (!"0".equalsIgnoreCase(fcResponse2.getStatus())) {
                    LOG.error("updateSafeBuyLogWin fail ,params: userId {}, tableId {}, id {} win {}", userId, tableId,
                            id, win);
                }
            } else {
                LOG.error("addUserSafeWin fail ,params: userId {}, tableId {}, id {} win {}", userId, tableId, id, win);
            }

        } else if (message instanceof LeaderAddUserCreditEvent) { // 团长直接增加保证金
            String userId = ((LeaderAddUserCreditEvent) message).userId();
            String tableId = ((LeaderAddUserCreditEvent) message).getTableId();
            int buyInCnt = ((LeaderAddUserCreditEvent) message).getAddBuyInCnt();
            long buyIn = ((LeaderAddUserCreditEvent) message).getBuyIn();
            ActorRef clientRef = ((LeaderAddUserCreditEvent) message).getClientSender();
            StrangerWinLoseLog strangerWinLoseLog =
                    FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(tableId, userId);
            if (strangerWinLoseLog == null) {
                //没有买入
                clientRef.tell(new LeaderAddMoneyEvent.Response(-1, "增加错误"), ActorRef.noSender());
                return;
            }
            boolean ret = FriendCoreIface.instance().iface().addStrangerUserDeposit(tableId, userId, buyInCnt * buyIn);
            if (ret) {
                clientRef.tell(new LeaderAddMoneyEvent.Response(0, "增加成功"), ActorRef.noSender());
            }
            else {
                //没有买入
                clientRef.tell(new LeaderAddMoneyEvent.Response(-1, "增加额度失败"), ActorRef.noSender());
                return;
            }

            // 通知玩家操作成功
            JSONObject object1 = new JSONObject();
            object1.put("num", buyInCnt * buyIn);
            GameUtils.notifyUser(object1, "leader_add_ok", userId, tableId);
        }
        else if (message instanceof UserExchangeChipsEvent) {
            String userId = ((UserExchangeChipsEvent) message).userId();
            String tableId = ((UserExchangeChipsEvent) message).getTableId();
            long buyInCnt = ((UserExchangeChipsEvent) message).getBuyInCnt();
            long oneBuyIn = ((UserExchangeChipsEvent) message).getOneBuyIn();
            ActorRef clientRef = ((UserExchangeChipsEvent) message).getClientSender();
            boolean ret = FriendCoreIface.instance().iface().exChangeStrangerChips(tableId, userId, buyInCnt * oneBuyIn);
            if (!ret) {
                clientRef.tell(new TableUserExchangeChipsEvent.Response(-1, "额度不足"), ActorRef.noSender());
                return;
            }
            sender().tell(new TableUserExchangeChipsOkEvent(userId, tableId, buyInCnt, oneBuyIn, clientRef), ActorRef.noSender());
        }
        else {
            unhandled(message);
        }
    }

    public void postStop() {
        LOG.info("User Actor Closed, user_id is {}", userId);
    }

}
