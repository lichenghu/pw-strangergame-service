package com.tiantian.strangergame.utils;

/**
 *
 */
public class DateUtils {

    public static String getFitTimes(long secs) {
        if (secs <= 0) {
            return null;
        }
        if (secs < 60) {
            return secs + "秒";
        }
        if (secs < 3600) {
            return secs / 60 + "分钟";
        }
        return secs/ 3600 + "小时";
    }

    public static long getNextFitTime(long endDate, long lastDate) {
        long dateTimes = (endDate - lastDate)/1000;
        if (dateTimes <= 0) {
            return 0;
        }
        if (dateTimes < 60) { // 1分钟
            return endDate - 10000; // 最后10s后通知
        }
        else if (dateTimes < 180) {// 3分钟
            return endDate - 60000; // 最后1分钟后通知
        }
        else if (dateTimes < 300) {// 5分钟
            return endDate - 60000; // 最后3分钟后通知
        }
        else if (dateTimes < 600) {// 10分钟
            return endDate - 300000; // 最后5分钟后通知
        }
        else if (dateTimes < 1800) {// 30分钟
            return endDate - 600000; // 最后10分钟后通知
        }
        else {// 60分钟
            return endDate - 1800000; // 最后30分钟后通知
        }
    }
}
