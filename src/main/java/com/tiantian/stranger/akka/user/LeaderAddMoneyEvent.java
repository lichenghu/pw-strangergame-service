package com.tiantian.stranger.akka.user;

import akka.actor.ActorRef;
import com.tiantian.stranger.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class LeaderAddMoneyEvent extends TableUserEvent {
    private String tableId;
    private int addBuyInCnt;
    private long buyIn;
    private String userId;

    public LeaderAddMoneyEvent(String tableId, int addBuyInCnt, long buyIn, String userId) {
        this.tableId = tableId;
        this.addBuyInCnt = addBuyInCnt;
        this.buyIn = buyIn;
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "leaderAddMoney";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getAddBuyInCnt() {
        return addBuyInCnt;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;

        public Response(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
