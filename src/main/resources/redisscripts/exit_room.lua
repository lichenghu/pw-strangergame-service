local keyLen = #KEYS
if keyLen ~= 6 then
    return 0
end

local userGroupTableKeyPre = KEYS[1]
local groupTableUserKeyPre = KEYS[2]
local userId = KEYS[3]
local groupTableUserOnlinePreKey = KEYS[4]
local groupTableStatusPreKey = KEYS[5]
local betStatus = KEYS[6]
local userGroupTableKey = userGroupTableKeyPre .. userId

local tableId = redis.pcall("hget", userGroupTableKey, "tableId")
local sitNum = redis.pcall("hget", userGroupTableKey, "sitNum")
local nickName =  redis.pcall("hget", userGroupTableKey, "nickName")

if tableId then
    if sitNum then
        local groupTableUserKey = groupTableUserKeyPre .. tableId
        redis.pcall("hdel", groupTableUserKey, sitNum)

        local groupTableStatusKey = groupTableStatusPreKey .. tableId
        local sitBetStatus = redis.pcall("hget", groupTableStatusKey, "sitBetStatus")
        if sitBetStatus and sitBetStatus ~= "" then
            local sitBetStatusJson = cjson.decode(sitBetStatus)
            if sitBetStatusJson[sitNum] then
                sitBetStatusJson[sitNum] = betStatus
                redis.pcall("hset", groupTableStatusKey, "sitBetStatus", cjson.encode(sitBetStatusJson))
            end
        end
    end
    local groupTableUserOnlineKey = groupTableUserOnlinePreKey .. tableId
    -- 删除桌子在线的玩家
    redis.pcall("hdel", groupTableUserOnlineKey, userId)
end

redis.pcall("del", userGroupTableKey)
return {tableId, sitNum, nickName}