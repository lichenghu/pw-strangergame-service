package com.tiantian.strangergame.cache;

import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class LocalCache {
    // 10分钟的有效时间
    private static final Cache<String, Optional<MaintainInfo>> STRANGER_STATUS_CACHE = CacheBuilder
            .newBuilder()
            .expireAfterWrite(600, TimeUnit.SECONDS)
            .build();

    private static final String MAINTAIN = "maintain";
    private static final String APP_NAME = "stranger";

    public static MaintainInfo getAppStatus() {
        try {
            Optional<MaintainInfo> optional = STRANGER_STATUS_CACHE.get(MAINTAIN, () -> {
                MaintainInfo maintainInfo = null;
                List<MaintainInfo> allMaintainInfos = CacheUtils.getAllMaintainInfo();
                if (allMaintainInfos != null && allMaintainInfos.size() > 0) {
                    for (MaintainInfo $maintainInfo : allMaintainInfos) {
                         if (APP_NAME.equalsIgnoreCase($maintainInfo.getAppName())) {
                             maintainInfo = $maintainInfo;
                             break;
                         }
                    }
                }
                return Optional.fromNullable(maintainInfo);
            });
            if (optional.isPresent()) {
                return optional.get();
            }
            else {
                STRANGER_STATUS_CACHE.invalidate(MAINTAIN);
                return null;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }


}
