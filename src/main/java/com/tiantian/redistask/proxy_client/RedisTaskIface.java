package com.tiantian.redistask.proxy_client;

import com.tiantian.framework.thrift.client.IFace;
import com.tiantian.redistask.settings.RedisTaskConfig;
import com.tiantian.redistask.thrift.task.RedisTaskService;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;
/**
 *
 */
public class RedisTaskIface extends IFace<RedisTaskService.Iface> {


    private static class RedisTaskIfaceHolder {
        private final static RedisTaskIface instance = new RedisTaskIface();
    }

    private RedisTaskIface() {
    }

    public static RedisTaskIface instance() {
        return RedisTaskIfaceHolder.instance;
    }

    @Override
    public TServiceClient createClient(TProtocol tProtocol) {
        return new RedisTaskService.Client(tProtocol);
    }

    @Override
    public com.tiantian.framework.thrift.client.ClientPool createPool() {
        return new com.tiantian.framework.thrift.client.ClientPool(RedisTaskConfig.getInstance().getHost(),
                RedisTaskConfig.getInstance().getPort());
    }

    @Override
    protected Class<RedisTaskService.Iface> faceClass() {
        return RedisTaskService.Iface.class;
    }
}
