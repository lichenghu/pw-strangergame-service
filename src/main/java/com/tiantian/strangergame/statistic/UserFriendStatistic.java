package com.tiantian.strangergame.statistic;

import java.util.ArrayList;
import java.util.Date;

/**
 * 朋友场统计
 */
public class UserFriendStatistic {
    private String id;
    private String userId;
    private long  joinCnt;  // 总参与次数
    private long  gamingCnt; // 总入局次数
    private long  gamingEndCnt; // 总比牌次数
    private long  winCnt; // 总获胜次数
    private double avgRaise; //平均加注
    private ArrayList<String> cardTypes; //牌型
    private Date createTime;
    private Date updateTime;

    public UserFriendStatistic() {
    }

    public UserFriendStatistic(String id, String userId, long joinCnt, long gamingCnt, long gamingEndCnt,
                               long winCnt, double avgRaise, ArrayList<String> cardTypes, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.joinCnt = joinCnt;
        this.gamingCnt = gamingCnt;
        this.gamingEndCnt = gamingEndCnt;
        this.winCnt = winCnt;
        this.avgRaise = avgRaise;
        this.cardTypes = cardTypes;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getJoinCnt() {
        return joinCnt;
    }

    public void setJoinCnt(long joinCnt) {
        this.joinCnt = joinCnt;
    }

    public long getGamingCnt() {
        return gamingCnt;
    }

    public void setGamingCnt(long gamingCnt) {
        this.gamingCnt = gamingCnt;
    }

    public long getGamingEndCnt() {
        return gamingEndCnt;
    }

    public void setGamingEndCnt(long gamingEndCnt) {
        this.gamingEndCnt = gamingEndCnt;
    }

    public long getWinCnt() {
        return winCnt;
    }

    public void setWinCnt(long winCnt) {
        this.winCnt = winCnt;
    }

    public double getAvgRaise() {
        return avgRaise;
    }

    public void setAvgRaise(double avgRaise) {
        this.avgRaise = avgRaise;
    }

    public ArrayList<String> getCardTypes() {
        return cardTypes;
    }

    public void setCardTypes(ArrayList<String> cardTypes) {
        this.cardTypes = cardTypes;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
