package com.tiantian.strangergame.akka.event;

import akka.actor.ActorRef;

import java.io.Serializable;

/**
 *
 */
public class LeaderAddUserCreditEvent implements UserEvent {

    private String tableId;
    private int addBuyInCnt;
    private long buyIn;
    private String userId;
    private ActorRef clientSender;

    public LeaderAddUserCreditEvent(String tableId, int addBuyInCnt, long buyIn, String userId, ActorRef clientSender) {
        this.tableId = tableId;
        this.addBuyInCnt = addBuyInCnt;
        this.buyIn = buyIn;
        this.userId = userId;
        this.clientSender = clientSender;
    }



    public String getTableId() {
        return tableId;
    }

    public int getAddBuyInCnt() {
        return addBuyInCnt;
    }

    @Override
    public String userId() {
        return userId;
    }

    public ActorRef getClientSender() {
        return clientSender;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;

        public Response(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
