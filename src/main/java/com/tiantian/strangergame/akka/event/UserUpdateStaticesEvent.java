package com.tiantian.strangergame.akka.event;

/**
 *
 */
public class UserUpdateStaticesEvent  implements UserEvent {
    private String userId;
    private String tableId;
    private long addChips;
    private long leftChips;

    public UserUpdateStaticesEvent(String userId, String tableId, long addChips, long leftChips) {
        this.userId = userId;
        this.tableId = tableId;
        this.addChips = addChips;
        this.leftChips = leftChips;
    }

    @Override
    public String userId() {
        return userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }

    public long getAddChips() {
        return addChips;
    }

    public long getLeftChips() {
        return leftChips;
    }
}
