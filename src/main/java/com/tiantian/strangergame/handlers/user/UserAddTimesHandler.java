package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.stranger.akka.user.TableUserAddTimesEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.Collection;
import java.util.UUID;

/**
 *
 */
public class UserAddTimesHandler implements EventHandler<TableUserAddTimesEvent> {
    @Override
    public void handler(TableUserAddTimesEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        if(StringUtils.isBlank(userId)) {
            return;
        }
        TableUser tableUser = TableUser.load(userId);
        if(tableUser == null || StringUtils.isBlank(tableUser.getTableId())) {
            return;
        }

        TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
        if (tableStatus == null || StringUtils.isBlank(tableStatus.getTableId())){
            return;
        }
        if (tableStatus.getCurrentBet() != null &&
                tableStatus.getCurrentBet().equalsIgnoreCase(tableUser.getSitNum())) {
            //  检验延时包是否足够
//            boolean ret = false;
//            try {
//                ret = AccountIface.instance().iface().reduceUserProps(userId, "shalou", 1);
//            } catch (TException e) {
//                e.printStackTrace();
//            }
            boolean ret = true;
            if (!ret) {
                JSONObject object2 = new JSONObject();
                object2.put("inner_id", tableStatus.getInningId());
                object2.put("inner_cnt", tableStatus.getIndexCount());
                object2.put("sn", -1);
                object2.put("secs", 0);
                object2.put("status", -1);
                object2.put("nick_name", "");
                GameUtils.notifyUser(object2, GameEventType.ADD_TMIES, userId, tableId);
                return;
            }
            tableUser.setHasAddTimes("1"); // 延时标记
            tableUser.addOneDelayTime(); // 增加计时, 玩家delay的时候用
            tableUser.save();
            tableStatus.saveNotAddCnt(); // 更新最后操作时间

            String nickName = tableUser.getNickName();
            //发送延时成功命令
            TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
            Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
            JSONObject object2 = new JSONObject();
            object2.put("inner_id", tableStatus.getInningId());
            object2.put("inner_cnt", tableStatus.getIndexCount());
            object2.put("sn", Integer.parseInt(tableUser.getSitNum()));
            object2.put("secs", GameConstants.BET_DELAYER_TIME/ 1000);
            object2.put("status", 0);
            object2.put("nick_name", nickName);
            String id2 = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object2, GameEventType.ADD_TMIES, userIds, id2, tableId);
        }
    }
}
