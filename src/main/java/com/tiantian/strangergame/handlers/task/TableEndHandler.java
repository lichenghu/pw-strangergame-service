package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameStatus;
import org.apache.thrift.TException;

/**
 *
 */
public class TableEndHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        JSONObject jsonObject = new JSONObject();
        // 发送一个延迟3s的开始, 确定庄家大小盲注任务
        jsonObject.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
        jsonObject.put("tableId", tableId);
        try {
            RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), GameConstants.DANDB_DELAYER_TIME);
        } catch (TException e) {
            e.printStackTrace();
        }
    }
}
