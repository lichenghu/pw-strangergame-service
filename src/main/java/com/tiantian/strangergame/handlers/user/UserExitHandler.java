package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tiantian.stranger.akka.user.TableUserExitEvent;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.manager.model.UserChips;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import java.util.*;

/**
 *
 */
public class UserExitHandler implements EventRetHandler<TableUserExitEvent> {

    @Override
    public Object handler(TableUserExitEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.getTableId();
        TableUser tableUser = TableUser.load(userId);
        if (tableId == null || tableUser == null ||
                !tableId.equalsIgnoreCase(tableUser.getTableId())) {
            return true;
        }
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return true;
        }
        String betStatus = tableUser.getBetStatus();
        if (GameEventType.ALLIN.equalsIgnoreCase(betStatus)) { // allin 不能站起
            return false;
        }
        if (tableStatus.needShowAllCards() &&
                StringUtils.isNotBlank(betStatus) && !GameEventType.FOLD.equals(betStatus)) { // 亮牌不能站起
            return false;
        }
        List<String> result = exit(tableStatus, tableUser);

        String sitNum = result.get(1);
        String nickName = result.get(2);
        if (StringUtils.isBlank(tableId)) {
            return true;
        }
        if (StringUtils.isBlank(sitNum)) {
            return true;
        }



        // 通知其他人 该玩家离开
        TableAllUser tableAllUsers = TableAllUser.load(tableId);
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("uid", userId);
        object.put("sn", Integer.parseInt(sitNum));
        object.put("nick_name", nickName);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.EXIT_ROOM, tableAllUsers.getOnlineTableUserIds(), id, tableId);

        // 当前操作的玩家是离开的玩家
        if (sitNum.equalsIgnoreCase(tableStatus.getCurrentBet())) {
            GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                    sitNum);
            boolean hasSafe = false;
            if (nextStatus != null) {
                tableStatus.roundBetEnd();
                boolean needShow = tableStatus.needShowAllCards();
                if (needShow) { // 需要显示所有的手牌
                    List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                    JSONObject object2 = new JSONObject();
                    object2.put("inner_id", tableStatus.getInningId());
                    object2.put("inner_cnt", tableStatus.getIndexCount());
                    object2.put("all_cards", cardsList);
                    String id2 = UUID.randomUUID().toString().replace("-", "");
                    GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, tableAllUsers.getOnlineTableUserIds(), id2, tableId);
                    hasSafe = GameUtils.checkSafe(tableStatus, tableAllUsers);
                }
                tableStatus.save();
                TableAllUser tableAllUser = TableAllUser.load(tableId);
                // 通知玩家的池信息
                GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                // 触发下一轮的发牌事件
                GameUtils.triggerNextStatusTask(nextStatus, tableId, tableStatus.getInningId(), hasSafe);
                return true;
            }
            TableAllUser tableAllUser = TableAllUser.load(tableId);
            //通知下一个玩家下注
            GameUtils.noticeNextUserBet(tableId, tableStatus, null, false, null, tableAllUser, self, context, sender);
        }
        else {
            //判断是只剩一人没有弃牌
            if(tableStatus.onlyOneNotFold()) {
                tableStatus.roundBetEnd();
                tableStatus.save();
                // 触发下一轮的发牌事件
                GameUtils.triggerNextStatusTask(GameStatus.FINISH, tableId, tableStatus.getInningId());
            } else {
                tableStatus.save();
            }
        }
        return true;
    }

    private List<String> exit(TableStatus tableStatus, TableUser tableUser) {
        String userId = tableUser.getUserId();
        String tableId = tableUser.getTableId();
        String sitNum = tableUser.getSitNum();
        String nickName = tableUser.getNickName();
        System.out.println("sitNum:" + sitNum);
        if (StringUtils.isNotBlank(tableId)) {
            TableAllUser tableAllUser = TableAllUser.load(tableId);
            if (StringUtils.isNotBlank(sitNum)) {
                if (tableAllUser.getJoinTableUserMap() != null &&
                        userId.equalsIgnoreCase(tableAllUser.getJoinTableUserMap().get(sitNum))) {
                    tableAllUser.delJoinSit(sitNum);
                }
                if (tableStatus != null) {
                    tableStatus.setSitBetStatusIfExistsNotFlush(sitNum, GameEventType.FOLD);
                }
            }
            tableAllUser.delJoinOnline(userId);
            tableAllUser.delSitDown(userId);
        }
        if (!tableUser.isNull()) {
            String buyInCnt = tableUser.getBuyInCnt();
            if (StringUtils.isNotBlank(buyInCnt) && tableStatus != null) {
                String buyInStr = tableStatus.getBuyIn();
                long buyIn = 0;
                if (StringUtils.isBlank(buyInStr)) {
                    buyIn = Long.parseLong(buyInStr);
                }
                UserChips userChips = UserChips.load(userId, tableId);
                if (userChips == null) {
                    UserChips.init(userId, tableId, buyIn * Long.parseLong(buyInCnt));
                } else {
                    userChips.addAndFlushChips(buyIn * Long.parseLong(buyInCnt));
                }
            }
            tableUser.delSelf(tableId);
        }
        return Lists.newArrayList(tableId, sitNum, nickName);
    }
}
