package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.user.TableUserExitEvent;
import com.tiantian.stranger.akka.user.TableUserJoinEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class UserJoinHandler implements EventHandler<TableUserJoinEvent> {
    static Logger LOG = LoggerFactory.getLogger(UserJoinHandler.class);
    @Override
    public void handler(TableUserJoinEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        String userId = event.getUserId();
        String avatarUrl = event.getAvatarUrl();
        String nickName = event.getNickName();
        String gender = event.getGender();
        long endDate = event.getEndDate();
        boolean isSafe = event.isSafe();
        long buyIn = event.getBuyIn();
        long big = event.getBigBlind();
        long small = event.getSmallBlind();
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null) {
           tableStatus = TableStatus.newNull();
        }
        // 查询出桌子里面的人
        TableAllUser tableAllUser = TableAllUser.load(tableId);

        TableUser tableUser = TableUser.load(userId);

        if (tableUser.isNull()) { // 没有进入过该房间,进入时站起,或者已经站起
            tableUser = TableUser.init(userId, tableId);
            tableUser.setAvatarUrl(avatarUrl);
            tableUser.setNickName(nickName);
            tableUser.setGender(gender);
            tableUser.save();
        }
        else {
            String userTableId = tableUser.getTableId();
            if (userTableId != null && !userTableId.equalsIgnoreCase(tableId)) {
                // 不是同一个房间
                // 退出操作
                Handlers.INSTANCE.executeRet(new TableUserExitEvent(userId, userTableId, null, null),
                        self, context, sender);
                TableUser $tableUser = TableUser.load(userId);
                if(!$tableUser.isNull()) { // 如果仍然不为空 则直接删除
                    System.out.println("exit fail delete user force");
                    $tableUser.delSelf($tableUser.getTableId());
                }
                tableUser = TableUser.init(userId, tableId);
                tableUser.setAvatarUrl(avatarUrl);
                tableUser.setNickName(nickName);
                tableUser.setGender(gender);
                tableUser.save();
            }
        }

        boolean isMaintain = GameUtils.checkMaintainInfoToOne(tableStatus, userId);
        if (isMaintain) {
            return;
        }
        //加入到游戏
        tableAllUser.joinOnline(userId);
        // 检测通知
        GameUtils.checkMaintainInfo(tableStatus, tableAllUser);

        String userOnlineTaskId = tableStatus.getUserOnlineTaskId();
        // 开启在线玩家状态检测
        if (StringUtils.isBlank(userOnlineTaskId)) {
            LOG.info("start online task");
            String taskId = UUID.randomUUID().toString().replace("-", "");
            if (tableStatus.isNull()) {
                tableStatus = TableStatus.init(tableId);
            }
            tableStatus.setEndTimes(endDate + "");
            tableStatus.setSafe(isSafe ? "1" : "0");
            tableStatus.setUserOnlineTaskId(taskId);
            tableStatus.setBuyIn(buyIn + "");
            tableStatus.setBigBlindMoney(big + "");
            tableStatus.setSmallBlindMoney(small + "");
            // 开启玩家在线状态监控任务
            GameUtils.triggerUserOnlineCheckTask(tableId, taskId);
        }
        tableStatus.saveNotAddCnt();

        UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableId, userId, nickName, avatarUrl, gender);
    }
}
