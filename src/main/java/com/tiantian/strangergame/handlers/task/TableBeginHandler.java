package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.akka.event.GameOver;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.*;

/**
 *
 */
public class TableBeginHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        // 玩家手动点击开始的时候才会有该值
        String masterId = jsonObject.getString("masterId");
        String randomBtn = jsonObject.getString("randomBtn");
        if (randomBtn == null) {
            randomBtn = "";
        }
        // 从redis中加载桌子状态数据
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        // 已经结束
        String endDate = tableStatus.getEndTimes();
        if (StringUtils.isNotBlank(endDate) && System.currentTimeMillis() > (Long.parseLong(endDate) + 30000)) { // 延时30s
            // 发送结束信息 销毁actor
            context.parent().tell(new GameOver(tableId), ActorRef.noSender());
            try {
                FriendCoreIface.instance().iface().strangerGameOver(tableId);
            } catch (TException e) {
                e.printStackTrace();
            }
            return;
        }
        // 如果不是准备则表示游戏已经在开始状态
        if (!GameStatus.READY.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }
        if (tableStatus.isStopping()) {
            return;
        }
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);

        // 检测是否有系统维护 必须在当前位置
        boolean isMaintain = GameUtils.checkMaintainInfo(tableStatus, tableAllUser);
        if (isMaintain) {
            return;
        }

        if(tableAllUser.getJoinTableUserMap() != null) {
            Set<Map.Entry<String, String>> entrySet = tableAllUser.getJoinTableUserMap().entrySet();
            Iterator<Map.Entry<String, String>> iterator = entrySet.iterator();
            List<String> removeSitList = new ArrayList<>();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                String userId = entry.getValue();
                String userRoomTableKey = GameConstants.USER_GROUP_TABLE_KEY + userId;
                String mapTableId = RedisUtil.getFromMap(userRoomTableKey, "tableId");
                if (mapTableId == null || !mapTableId.equalsIgnoreCase(tableId)) {
                    removeSitList.add(entry.getKey());
                    // 需要删除
                    iterator.remove();
                }
            }

            if (removeSitList.size() > 0) {
                tableAllUser.flushGamingSitUser(removeSitList);
            }
        }
        // 清除掉线站起的玩家
        GameUtils.clearStandUpOnlineUsers(tableAllUser, tableId);

        if (tableAllUser.getJoinTableUserMap() == null || tableAllUser.getJoinTableUserMap().size() < 2) {
            // 如果只有一个人
            if (tableAllUser.getJoinTableUserMap() != null && tableAllUser.getJoinTableUserMap().size() == 1) {
                // 检测最后一个人，判断玩家是否掉线,如果掉线踢出房间
                JSONObject $jsonObject = new JSONObject();
                $jsonObject.put("tableId", tableId);
                TableTaskEvent tableTaskEvent = new TableTaskEvent();
                tableTaskEvent.setEvent(GameConstants.TEST_LAST_USER);
                tableTaskEvent.setParams($jsonObject);
                Handlers.INSTANCE.execute(tableTaskEvent, self, context, sender);
            }
            // 再次发送begin 任务
            jsonObject.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
            jsonObject.put("tableId", tableId);
            try {
                RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), GameConstants.BEGIN_DELAYER_TIME_SECS);
            } catch (TException e) {
                e.printStackTrace();
            }
            return;
        }
        tableStatus.setStatus(GameStatus.BEGIN.name());
        tableStatus.setCanStart("0"); // 开始游戏了
        if(StringUtils.isNotBlank(masterId)) {
            tableStatus.setMasterId(masterId);
        }
        tableStatus.startGame();
        tableStatus.saveNotAddCnt();

        JSONObject data = new JSONObject();
        data.put(GameConstants.TASK_EVENT, GameStatus.D_AND_B.name());
        data.put("tableId", tableId);
        data.put("randomBtn", randomBtn);
        TableTaskEvent tableTaskEvent = new TableTaskEvent(GameStatus.D_AND_B.name(), data);
        Handlers.INSTANCE.execute(tableTaskEvent, self, context, sender);
    }
}
