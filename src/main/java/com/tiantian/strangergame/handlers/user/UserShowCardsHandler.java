package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.user.TableUserShowCardsEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.GameRecord;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.UUID;

/**
 *
 */
public class UserShowCardsHandler implements EventHandler<TableUserShowCardsEvent> {
    @Override
    public void handler(TableUserShowCardsEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        String hands = event.getData();
        TableUser tableUser = TableUser.load(userId);
        if (tableUser == null) {
            return;
        }
        TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
        // 是在结算的时候直接亮牌
        if (tableStatus != null && GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            String handCardsStr = tableStatus.getUserHandCards(tableUser.getSitNum());
            if (StringUtils.isBlank(handCardsStr)) {
                return;
            }
            String[] handCardStrs = handCardsStr.split(",");
            String handCards = handCardsStr;
            if (StringUtils.isNotBlank(hands)) {
                if ("1".equalsIgnoreCase(hands)) {
                    handCards =  handCardStrs[0] + ",";
                } else if ("2".equalsIgnoreCase(hands)) {
                    handCards = "," + handCardStrs[1];
                }
            }
            TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(tableUser.getSitNum()));
            object.put("hand_cards", handCards);
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.SHOW_CARDS, tableAllUser.getOnlineTableUserIds(), id, tableId);

            GameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
            if (mttGameRecord == null) {
                mttGameRecord = new GameRecord();
            }
            List<GameRecord.Progress> progresses = mttGameRecord.getProgresses();
            progresses.add(GameRecord.Progress.create("show_card", tableUser.getSitNum(),  handCards ,
                    System.currentTimeMillis() - mttGameRecord.getStartTime()));
            mttGameRecord.addSitShowCardSit(tableUser.getSitNum(), handCards);
            RecordUtils.restLastedRecord(mttGameRecord, tableId);
        }
    }
}
