package com.tiantian.strangergame.utils;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 *
 */
public class OddsUtils {
    private static final Map<String, Float> ODDS_MAP ;
    static {
        ODDS_MAP = Maps.newHashMap();
        ODDS_MAP.put("1", 30f);
        ODDS_MAP.put("2", 16f);
        ODDS_MAP.put("3", 11.0f);
        ODDS_MAP.put("4", 8.0f);
        ODDS_MAP.put("5", 7.0f);
        ODDS_MAP.put("6", 5.5f);
        ODDS_MAP.put("7", 4.5f);
        ODDS_MAP.put("8", 4.0f);
        ODDS_MAP.put("9", 3.8f);
        ODDS_MAP.put("10", 3.5f);
        ODDS_MAP.put("11", 3.3f);
        ODDS_MAP.put("12", 3f);
        ODDS_MAP.put("13", 2.8f);
        ODDS_MAP.put("14", 2.5f);
        ODDS_MAP.put("15", 2.3f);
    }

    public static float getOdds(String odds) {
        return ODDS_MAP.get(odds);
    }
}
