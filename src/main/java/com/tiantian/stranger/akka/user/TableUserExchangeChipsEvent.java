package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class TableUserExchangeChipsEvent extends TableUserEvent {
    private String userId;
    private String tableId;
    private long buyInCnt;
    private long oneBuyIn;

    public TableUserExchangeChipsEvent(String userId, String tableId, long buyInCnt, long oneBuyIn) {
        this.userId = userId;
        this.tableId = tableId;
        this.buyInCnt = buyInCnt;
        this.oneBuyIn = oneBuyIn;
    }

    public long getBuyInCnt() {
        return buyInCnt;
    }

    public long getOneBuyIn() {
        return oneBuyIn;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userChangeChips";
    }

    public String getUserId() {
        return userId;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;

        public Response(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
