package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.user.TableUserAllinEvent;
import com.tiantian.stranger.akka.user.TableUserCheckEvent;
import com.tiantian.stranger.akka.user.TableUserFoldEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class UserAllinHandler implements EventHandler<TableUserAllinEvent> {
    @Override
    public void handler(TableUserAllinEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String pwd = event.getPwd();
        String tableId = event.getTableId();
        TableUser tableUser = TableUser.load(userId);
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            // 判断口令是否有效
            if (tableStatus != null && tableStatus.checkPwdAndSit(pwd, tableUser.getSitNum())) {
                // 校验玩家能否allin，然后allin
                UserChips userChips = UserChips.load(userId, tableUser.getTableId());
                String[] ops = tableStatus.getUserCanOps(userId, tableUser.getSitNum());
                String status = null;
                char op = ops[0].charAt(4);
                // 能不能allin
                if ('0' == op) {
                    // 判断是否能够看牌
                    char checkOp = ops[0].charAt(1);
                    if ('0' == checkOp) {
                        status = GameEventType.FOLD;
                        Handlers.INSTANCE.execute(new TableUserFoldEvent(event.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    } else {
                        status = GameEventType.CHECK;
                        Handlers.INSTANCE.execute(new TableUserCheckEvent(event.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    }
                } else {
                    status = GameEventType.ALLIN;
                }
                // 增加玩家的操作计数
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                long leftChips = userChips.getChips();
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                // 如果能够allin
                if (status.equalsIgnoreCase(GameEventType.ALLIN)) {
                    userChips.reduceAndFlushChips(leftChips);
                    //设置allin的数值
                    object.put("val", leftChips);
                    tableStatus.userBet(tableUser.getSitNum(), leftChips);
                    // 更新平均加注日志
                    tableStatus.updateUserBetRaiseLog(tableUser.getSitNum(), leftChips);
                    object.put("bottom_pool", tableStatus.getTotalPoolMoney());
                }
                object.put("left_chips", userChips.getChips());
                //设置玩家加注
                tableUser.setBetStatus(status);
                tableUser.save();

                String maxBetSitNum = tableStatus.getMaxBetSitNum();
                String maxBetSitNumChips = tableStatus.getMaxBetSitNumChips();
                //通知其他在玩家该玩家allin
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableId);

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableId);

                GameRecord gameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
                if (gameRecord == null) {
                    gameRecord = new GameRecord();
                }
                List<GameRecord.Progress> progresses = gameRecord.getProgresses();
                progresses.add(GameRecord.Progress.create("all_in", tableUser.getSitNum(), leftChips + "",
                        System.currentTimeMillis() - gameRecord.getStartTime()));
                RecordUtils.restLastedRecord(gameRecord, tableId);


                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(),
                        maxBetSitNum, tableUser.getSitNum());
                boolean hasSafe = false;
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean needShow = tableStatus.needShowAllCards();
                    if (needShow) { // 需要显示所有的手牌
                        List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                        JSONObject object2 = new JSONObject();
                        object2.put("inner_id", tableStatus.getInningId());
                        object2.put("inner_cnt", tableStatus.getIndexCount());
                        object2.put("all_cards", cardsList);
                        String id2 = UUID.randomUUID().toString().replace("-", "");
                        GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                        // 保险
                        hasSafe = GameUtils.checkSafe(tableStatus, tableAllUser);
                    }
                    //需要保存数据到redis
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId(), hasSafe);
                    return;
                }

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, maxBetSitNum, false, maxBetSitNumChips,
                       tableAllUser, self, context, sender);
            }
        }
    }
}
