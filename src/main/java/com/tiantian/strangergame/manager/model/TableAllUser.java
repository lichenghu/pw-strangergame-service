package com.tiantian.strangergame.manager.model;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.manager.constants.GameConstants;
import org.apache.commons.lang.StringUtils;
import java.util.*;

/**
 *
 */
public class TableAllUser {

    private String tableId;
    // 座位号和userId
    private Map<String, String> gamingSitUserMap;
    // 桌子里面所有的玩家ID
    private Collection<String> onlineTableUserIds;

    private Map<String, String> joinTableUserMap;
    // 等待大盲注入局的玩家
    private Map<String, String> waitBlindTableUserMap;
    // 丢个大盲注入局的玩家
    private Map<String, String> blindTableUserMap;

    private Set<String> tableSitDownUsers;

    public static TableAllUser load(String tableId) {

        TableAllUser tableAllUser = new TableAllUser();
        tableAllUser.tableId = tableId;
        loadData(tableAllUser);
        return tableAllUser;
    }

    private static void loadData(TableAllUser tableAllUser) {
        String tbId = tableAllUser.tableId;
        Map<String, String> tableMap = RedisUtil.getMap(GameConstants.GROUP_TABLE_USER_KEY + tbId);
        if (tableMap != null) {
            // 筛选出在gaming状态的游戏玩家
            Map<String, String> sitUserMap = new HashMap<>();
            Map<String, String> waitBlindMap = new HashMap<>();
            Map<String, String> blindMap = new HashMap<>();
            for(Map.Entry<String, String> entry : tableMap.entrySet()) {
                String sitNum = entry.getKey();
                String userId = entry.getValue();
                TableUser tableUser = TableUser.load(userId);
                if (tableUser != null && tbId.equalsIgnoreCase(tableUser.getTableId())) {
                    if ("gaming".equalsIgnoreCase(tableUser.getStatus())) {
                        sitUserMap.put(sitNum, userId);
                    }
                    if("wait_blind".equalsIgnoreCase(tableUser.getStatus())) {
                        waitBlindMap.put(sitNum, userId);
                    }
                    if("bet_blind".equalsIgnoreCase(tableUser.getStatus())) {
                        blindMap.put(sitNum, userId);
                    }
                }
            }
            tableAllUser.gamingSitUserMap = sitUserMap;
            tableAllUser.waitBlindTableUserMap = waitBlindMap;
            tableAllUser.blindTableUserMap = blindMap;
            tableAllUser.joinTableUserMap = tableMap;
        }
        Map<String, String> tableAllUserMap = RedisUtil.getMap(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tbId);
        if (tableAllUserMap != null) {
            tableAllUser.onlineTableUserIds = tableAllUserMap.keySet();
        }

        Set<String> tableSitDownUserSet = RedisUtil.getMapKeys(GameConstants.GROUP_TABLE_USER_SIT_DOWN_KEY + tbId);
        if (tableSitDownUserSet != null) {
            tableAllUser.tableSitDownUsers = tableSitDownUserSet;
        }
    }

    //删除掉已经离开的玩家
    public void flushGamingSitUser(List<String> removeSitList) {
        for (String sitNum : removeSitList) {
            RedisUtil.delMapField(GameConstants.GROUP_TABLE_USER_KEY + tableId, sitNum);
        }
    }
    // 根据玩家ID判断玩家是否在游戏中，并返回座位号，返回值为空则表示不在
    public String checkUserGaming(String userId) {
        if(gamingSitUserMap == null) {
            return null;
        }
        Set<Map.Entry<String, String>> entrySet = gamingSitUserMap.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            String sitNum = entry.getKey();
            String uId = entry.getValue();
            if (uId.equalsIgnoreCase(userId)) {
                return sitNum;
            }
        }
        return null;
    }

    public void userStandUp(String sitNum, String userId) {
        RedisUtil.delMapField(GameConstants.GROUP_TABLE_USER_KEY + tableId, sitNum);
        RedisUtil.delMapField(GameConstants.GROUP_TABLE_USER_SIT_DOWN_KEY + tableId, userId);
    }

    public void delSelf() {
        RedisUtil.del(GameConstants.GROUP_TABLE_USER_KEY + tableId);
        RedisUtil.del(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tableId);
        RedisUtil.del(GameConstants.GROUP_TABLE_USER_SIT_DOWN_KEY + tableId);
        RedisUtil.del(GameConstants.USER_EVER_JOIN_KEY + tableId);
    }

    public boolean checkUserOnline(String userId) {
        return onlineTableUserIds != null && onlineTableUserIds.contains(userId);
    }
    // 加入在线
    public void joinOnline(String userId) {
        RedisUtil.setMap(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tableId, userId, "");
        // 加入进入记录
        addEverJoin(userId);
    }

    public void delJoinOnline(String userId) {
        RedisUtil.delMapField(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tableId, userId);
    }

    public void delJoinSit(String sitNum) {
        RedisUtil.delMapField(GameConstants.GROUP_TABLE_USER_KEY + tableId, sitNum);
    }

    public void delSitDown(String userId) {
        RedisUtil.delMapField(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tableId, userId);
    }

    public void userSitDown(String userId, String sitNum) {
        RedisUtil.setMap(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tableId, userId, "");
        RedisUtil.expire(GameConstants.GROUP_TABLE_USER_ONLINE_KEY + tableId, 86400);
        RedisUtil.setMap(GameConstants.GROUP_TABLE_USER_KEY + tableId, sitNum, userId);
        RedisUtil.expire(GameConstants.GROUP_TABLE_USER_KEY + tableId, 86400);
        RedisUtil.setMap(GameConstants.GROUP_TABLE_USER_SIT_DOWN_KEY + tableId, userId, "");
        RedisUtil.expire(GameConstants.GROUP_TABLE_USER_SIT_DOWN_KEY + tableId, 86400);
        loadData(this);
    }

    public Map<String, String> getGamingSitUserMap() {
        return gamingSitUserMap;
    }

    public void setGamingSitUserMap(Map<String, String> gamingSitUserMap) {
        this.gamingSitUserMap = gamingSitUserMap;
    }

    public Collection<String> getOnlineTableUserIds() {
        return onlineTableUserIds;
    }

    public void setOnlineTableUserIds(Collection<String> onlineTableUserIds) {
        this.onlineTableUserIds = onlineTableUserIds;
    }

    public Map<String, String> getJoinTableUserMap() {
        return joinTableUserMap;
    }

    public Map<String, String> getNoWaitBlindJoinUserMap(String bigBlindNum) {
        Map<String, String> noWaitBlindJoinUserMap = Maps.newHashMap();
        for(Map.Entry<String, String> entry : joinTableUserMap.entrySet()) {
            String sitNum = entry.getKey();
            String userId = entry.getValue();
            if (waitBlindTableUserMap.containsKey(sitNum) && !sitNum.equalsIgnoreCase(bigBlindNum)) {
                continue;
            }
            noWaitBlindJoinUserMap.put(sitNum, userId);
        }
        return noWaitBlindJoinUserMap;
    }

    // 获取下一个btn位置和盲位
    private String[] getButtonAndBlind( Set<String> joinSits, String oldButton) {
        // button 位置
        String button = null;
        // 小盲注座位号
        String sbSitNum = null;
        // 大盲注座位号
        String bbSitNum = null;
        // 转换进行排序
        TreeSet<String> sitTreeSet = new TreeSet<>(joinSits);
        // 没有button位置，则座位最小号为button位
        if (StringUtils.isBlank(oldButton)) {
            button = sitTreeSet.first();
        }
        else { // 之前存在了button位置，则算出下一位button
            for (String sitNum : sitTreeSet) {
                // 第一个比oldButton大的值
                if (Integer.parseInt(sitNum) > Integer.parseInt(oldButton)) {
                    button = sitNum;
                    break;
                }
            }
            // 如果没有找到则说明 oldButton是最大值，则设置座位队列第一个为button位
            if (button == null) {
                button = sitTreeSet.first();
            }
        }

        // 获取大盲注和小盲注座位号
        for (String sitNum : sitTreeSet) {
            // 比button座位号大
            if (Integer.parseInt(sitNum) > Integer.parseInt(button)) {
                sbSitNum = sitNum;
                break;
            }
        }
        // 小盲注没找到则第一个为小盲注
        if (sbSitNum == null) {
            sbSitNum = sitTreeSet.first();
        }
        // 查找大盲注
        for (String sitNum : sitTreeSet) {
            // 比button座位号大
            if (Integer.parseInt(sitNum) > Integer.parseInt(sbSitNum)) {
                bbSitNum = sitNum;
                break;
            }
        }
        // 大盲注没找到则第一个为大盲注
        if (bbSitNum == null) {
            bbSitNum = sitTreeSet.first();
        }
        if (sitTreeSet.size() == 2) { // 如果只有2个人 btn 位置也是小盲位
            return new String[]{button, button, sbSitNum};
        }
        return new String[]{button, sbSitNum, bbSitNum};
    }

    public String[] getRightButtonAndBlind(String lastBtn) {
        Set<String> joinSits = getJoinTableUserMap().keySet();
        if (joinSits.size() <= 2) { // 只有2个人直接开始,不需要等等
            String[] result = getButtonAndBlind(joinSits, lastBtn);
            // 重置等待的人状态
            waitBlindTableUserMap.clear();
            blindTableUserMap.clear();
            return result;
        }
        if (gamingSitUserMap == null || gamingSitUserMap.size() <= 1) { // 游戏人数不齐则全部拉入游戏
            String[] result = getButtonAndBlind(joinSits, lastBtn);
            // 重置等待的人状态
            waitBlindTableUserMap.clear();
            blindTableUserMap.clear();
            return result;
        }
        // 判断是否只有一个人不是等待的,如果是必须强制拉进来一个人,然后不是等待的人是btn和大盲位
//        if(gamingSitUserMap.size() == 1 && (blindTableUserMap == null || blindTableUserMap.isEmpty())) {
//           Set<Map.Entry<String, String>> entrySet = gamingSitUserMap.entrySet();
//           Map.Entry<String, String> entry = entrySet.iterator().next();
//           String sitNum = entry.getKey();
//           String nextSit = nextSitNum(joinSits, sitNum);
//           return new String[] {sitNum, nextSit, sitNum};
//        }
        Set<String> tmpJoinSet = Sets.newHashSet(joinSits);
        int size = joinSits.size();
        for (int i = 0; i < size; i++) { // 最大循环次数
             String[] result = getButtonAndBlind(tmpJoinSet, lastBtn);
             String btn = result[0];
             String small = result[1];
             String bigBlind = result[2];
             boolean btnIsWait = waitBlindTableUserMap.containsKey(btn); // 是不是等带中的
             boolean smallIsWait = waitBlindTableUserMap.containsKey(small); // 是不是等带中的
             if(smallIsWait && !btnIsWait) { //庄位不是等待,小盲住等待
                tmpJoinSet.remove(small); // 删除小盲注, btn位不改变重新计算
                continue;
             }
             if(!btnIsWait) { // 小盲位不是等待中,庄位也不是等待
                return result;
             }
             lastBtn = btn;
        }
        return null;
    }

    private String nextSitNum(Set<String> sitNums, String currentSitNum) {
        String nextSitNum = null;
        TreeSet<String> sitTreeSet = new TreeSet<>(sitNums);
        for (String sitNum : sitTreeSet) {
            if (Integer.parseInt(sitNum) > Integer.parseInt(currentSitNum)) {
                nextSitNum = sitNum;
                break;
            }
        }
        // 小盲注没找到则第一个为小盲注
        if (nextSitNum == null) {
            nextSitNum = sitTreeSet.first();
        }
        return nextSitNum;
    }

    private void addEverJoin(String userId) {
        RedisUtil.sadd(GameConstants.USER_EVER_JOIN_KEY + tableId, userId);
        RedisUtil.expire(GameConstants.USER_EVER_JOIN_KEY + tableId, 86400);
    }

    public Set<String> userEverJoinSet() { // 曾经进入过游戏的玩家,游戏结束后需要删除的
        return RedisUtil.smembers(GameConstants.USER_EVER_JOIN_KEY + tableId);
    }

    public void setJoinTableUserMap(Map<String, String> joinTableUserMap) {
        this.joinTableUserMap = joinTableUserMap;
    }

    public Set<String> getTableSitDownUsers() {
        return tableSitDownUsers;
    }

    public void setTableSitDownUsers(Set<String> tableSitDownUsers) {
        this.tableSitDownUsers = tableSitDownUsers;
    }

    public Map<String, String> getWaitBlindTableUserMap() {
        return waitBlindTableUserMap;
    }

    public Map<String, String> getBlindTableUserMap() {
        return blindTableUserMap;
    }

    public void setBlindTableUserMap(Map<String, String> blindTableUserMap) {
        this.blindTableUserMap = blindTableUserMap;
    }
}
