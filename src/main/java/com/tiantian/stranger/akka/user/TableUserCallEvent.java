package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserCallEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private String pwd;
    public TableUserCallEvent() {

    }
    public TableUserCallEvent(String tableId, String userId, String pwd) {
        this.tableId = tableId;
        this.userId = userId;
        this.pwd = pwd;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userCall";
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
