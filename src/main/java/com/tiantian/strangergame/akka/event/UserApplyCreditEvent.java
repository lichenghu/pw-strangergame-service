package com.tiantian.strangergame.akka.event;

import akka.actor.ActorRef;

/**
 *
 */
public class UserApplyCreditEvent implements UserEvent {
    private String tableId;
    private String userId;
    private long buyIn;
    private long maxBuyIn;
    private ActorRef clientSender;
    private String masterId;

    public UserApplyCreditEvent(String tableId, String userId, long buyIn, long maxBuyIn,
                                ActorRef clientSender, String masterId) {
        this.tableId = tableId;
        this.userId = userId;
        this.buyIn = buyIn;
        this.maxBuyIn = maxBuyIn;
        this.clientSender = clientSender;
        this.masterId = masterId;
    }

    public String getMasterId() {
        return masterId;
    }

    @Override
    public String userId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }

    public long getBuyIn() {
        return buyIn;
    }

    public String getUserId() {
        return userId;
    }

    public long getMaxBuyIn() {
        return maxBuyIn;
    }

    public ActorRef getClientSender() {
        return clientSender;
    }
}
