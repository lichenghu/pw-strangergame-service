package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class TableOnlineUserCheckHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableOnlineUserCheckHandler.class);
    private static String exitRoomScriptSha = null;
    private static final String EXIT_ROOM_SCRIPT_NAME_PATH = "redisscripts/exit_room.lua";
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String taskId = jsonObject.getString("taskId");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        boolean needCheck = true;
        if (StringUtils.isBlank(tableStatus.getStatus())) {
            needCheck = false;
        }
        String tbTaskId = tableStatus.getUserOnlineTaskId();
        if (StringUtils.isBlank(tbTaskId) || !tbTaskId.equalsIgnoreCase(taskId)) {
            LOG.info("tbTaskId is null or not eq taskId");
            return;
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        System.out.println("OnlineUserIds:" + JSON.toJSONString(userIds));
        if(userIds == null || userIds.size() == 0) {
            LOG.info("userIds is null or userIds size is 0");
            return;
        }
        boolean hasUsersOnline = false;
        if (needCheck) {
            for (String userId : userIds) {
                try {
                    boolean isOnline = RedisUtil.exists(GameConstants.ROUTER_KEY + userId);
                    if (isOnline) {
                        hasUsersOnline = true;
                        continue;
                    }
                    System.out.println("GamingSitUserMap:" + JSON.toJSONString(tableAllUser.getGamingSitUserMap()));
                    // 不在线,判断玩家是否还在游戏里面
                    String sitNum = tableAllUser.checkUserGaming(userId);
                    Set<String> sitDownSet = tableAllUser.getTableSitDownUsers();
                    System.out.println("sitDownSet:" + JSON.toJSONString(sitDownSet));
                    boolean isLastOne = false; //最后一个做在桌子上
                    if (sitDownSet != null && sitDownSet.size() == 1) {
                        isLastOne = sitDownSet.contains(userId);
                    }
                    System.out.println("sitNum:" + sitNum);
                    System.out.println("isLastOne:" + isLastOne);
                    // 不在游戏里面
                    if (StringUtils.isBlank(sitNum) || isLastOne) {
                        TableUser tableUser = TableUser.load(userId);
                        if (!tableUser.isNull()) {
                            exit(tableUser, tableStatus);
                        }
                        if (isLastOne && StringUtils.isNotBlank(sitNum)) {
                            //通知其他人离开
                            // 通知玩家离开
                            JSONObject object = new JSONObject();
                            object.put("inner_id", tableStatus.getInningId() == null ? "" : tableStatus.getInningId());
                            object.put("inner_cnt", tableStatus.getIndexCount() == null ? "" : tableStatus.getIndexCount());
                            object.put("uid", userId);
                            object.put("sn", Integer.parseInt(sitNum));
                            object.put("reason", "");
                            String id = UUID.randomUUID().toString().replace("-", "");
                            userIds.remove(userId);
                            // 如果不捕获异常则会终端下一个任务
                            GameUtils.notifyUsers(object, GameEventType.STAND_UP, userIds, id, tableId);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        // 保存一下
        tableStatus.saveNotAddCnt();
        if (hasUsersOnline) {
            // 开启玩家在线状态监控任务
            GameUtils.triggerUserOnlineCheckTask(tableId, taskId);
        }
    }

    private List<String> exit(TableUser tableUser, TableStatus tableStatus) {
        String userId = tableUser.getUserId();
        String tableId = tableUser.getTableId();
        String sitNum = tableUser.getSitNum();
        String nickName = tableUser.getNickName();
        System.out.println("----table check online ------3--" + tableId);
        if (StringUtils.isNotBlank(tableId)) {
            TableAllUser tableAllUser = TableAllUser.load(tableId);
            if(StringUtils.isNotBlank(sitNum)) {
                System.out.println("----table check online ------4--" + tableId);
               tableAllUser.delJoinSit(sitNum);
               if (tableStatus != null) {
                   tableStatus.setSitBetStatusIfExistsNotFlush(sitNum, GameEventType.FOLD);
               }
            }
            System.out.println("----table check online ------5--" + tableId);
            tableAllUser.delJoinOnline(userId);
            tableAllUser.delSitDown(userId);
        }
        tableUser.delSelf(tableId);
        return Lists.newArrayList(tableId, sitNum, nickName);
    }
}
