package com.tiantian.strangergame.akka.event;

/**
 *
 */
public class TableDbUpdateCntEvent implements TableDbEvent {
    private String tableId;
    private int cnt;

    public TableDbUpdateCntEvent(String tableId, int cnt) {
        this.tableId = tableId;
        this.cnt = cnt;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    public int getCnt() {
        return cnt;
    }
}
