package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 *
 */
public class TableCheckChipsHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String masterId = jsonObject.getString("masterId");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        String innerId = tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount();

        tableStatus.clearNotFlush();
        tableStatus.setCanStart("1");
        tableStatus.saveNotAddCnt();


        GameRecord gameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (gameRecord == null) {
            gameRecord = new GameRecord();
        }
        List<GameRecord.Progress> progresses = gameRecord.getProgresses();


        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getGamingSitUserMap().values();
        List<String[]> list = new ArrayList<>();
        for (String userId : userIds) {
            TableUser tableUser = TableUser.load(userId);
            String userTableId = tableUser.getTableId();
            if (tableId.equalsIgnoreCase(userTableId)) {
                list.add(new String[]{"hdel", GameConstants.USER_GROUP_TABLE_KEY + userId, "operateCount"});
                list.add(new String[]{"hset", GameConstants.USER_GROUP_TABLE_KEY + userId, "betStatus", ""});
                list.add(new String[]{"hset", GameConstants.USER_GROUP_TABLE_KEY + userId, "showCards", ""});
            }
            //判断筹码
            checkUserChips(userId, tableId,
                    tableUser.getSitNum(), tableAllUser, tableStatus.getBuyIn(),
                    innerId, indexCount, progresses, gameRecord.getStartTime());
        }
        if (list.size() > 0) {
            RedisUtil.pipMapCmd(list);
        }
        //     Map<String, String> joinUserMap =  tableAllUser.getJoinTableUserMap();
//        if (joinUserMap != null && joinUserMap.size() >= 2) { // 人数足以开始游戏
//            // 倒计时
//            JSONObject object = new JSONObject();
//            object.put("secs", 3);
//            GameUtils.notifyUser(object, GameEventType.START_COUNT_DOWN, masterId);
        //       }

        //TODO 判断局是否结束,删除 UserChips  包括玩家掉线和退出的时候


        // 发送一个延迟3s的开始, 确定庄家大小盲注任务
        jsonObject.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
        jsonObject.put("tableId", tableId);
        jsonObject.put("masterId", masterId);
        try {
            RedisTaskIface.instance().iface().pushTask(jsonObject.toJSONString(), GameConstants.AFTER_CHECK_CHIPS_DELAYER_TIME);
        } catch (TException e) {
            e.printStackTrace();
        }
        progresses.add(GameRecord.Progress.create("end", "nil", "nil",
                System.currentTimeMillis() - gameRecord.getStartTime()));
        RecordUtils.restLastedRecord(gameRecord, tableId);

    }

    private void checkUserChips(String userId, String tableId,
                                String sitNum, TableAllUser tableAllUser, String buyIn,
                                String innerId, String innerCnt,
                                List<GameRecord.Progress> progresses, long startTime) {
        Collection<String> toUserIds = tableAllUser.getOnlineTableUserIds();
        UserChips userChips = UserChips.load(userId, tableId);
        if (userChips != null) {

            // 强制玩家站起
            TableUser tableUser = TableUser.load(userId);
            /** 判断玩家有没有买入逻辑 **/
            // 玩家申请买入筹码量
            String userBuyInCnt = tableUser.getBuyInCnt();
            if(StringUtils.isNotBlank(userBuyInCnt) && StringUtils.isNotBlank(buyIn)) {
                long buyInChips = 0;
                int cnt = 0;
                try {
                    cnt = Integer.parseInt(userBuyInCnt);
                    buyInChips = cnt * Long.parseLong(buyIn);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (buyInChips > 0) {
                    // 添加买入
                    userChips.addAndFlushChips(buyInChips);
                    tableUser.setBuyInCnt(""); // 清除掉买入量
                    // 同步修改买入等统计记录
//                    FriendCoreUtils.getInstance().addStrangerWinLoseLogBuyInAndCnt(tableId, userId, Long.parseLong(buyIn),
//                            cnt);
                    tableUser.save();
                    // 通知玩家购买
                    JSONObject object1 = new JSONObject();
                    object1.put("uid", userId);
                    object1.put("sn", Integer.parseInt(sitNum));
                    // 带入筹码
                    object1.put("nick_name", tableUser.getNickName());
                    object1.put("buy", buyInChips);
                    String id = UUID.randomUUID().toString().replace("-", "");
                    // 如果不捕获异常则会终端下一个任务
                    GameUtils.notifyUsers(object1, GameEventType.ADD_CHIPS, toUserIds, id, tableId);
                    return;
                }
            }
            /** 判断玩家有没有买入 **/
            long chips = userChips.getChips();
            // 不足一个大盲注带入筹码
            if (chips == 0) {
                // 站起
                tableAllUser.userStandUp(tableUser.getSitNum(), tableUser.getUserId());
                // 站起
                tableUser.forceStandUp();

//              JSONObject object = new JSONObject();
//              GameUtils.notifyUser(object, GameEventType.CHIPS_NOT_ENOUGH, userId);
                // 通知玩家离开
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", innerId);
                object1.put("inner_cnt", innerCnt);
                object1.put("uid", userId);
                object1.put("sn", Integer.parseInt(sitNum));
                object1.put("reason",  GameEventType.CHIPS_NOT_ENOUGH);
                String id = UUID.randomUUID().toString().replace("-", "");
                // 如果不捕获异常则会终端下一个任务
                GameUtils.notifyUsers(object1, GameEventType.STAND_UP, toUserIds, id, tableId);
                progresses.add(GameRecord.Progress.create("stand_up", sitNum, "nil",
                        System.currentTimeMillis() - startTime));
            }

        }

    }

}
