package com.tiantian.strangergame.akka.actor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.tiantian.stranger.akka.event.Event;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.stranger.akka.event.TableUserEvent;
import com.tiantian.strangergame.akka.event.GameOver;
import com.tiantian.strangergame.akka.event.TableDbEvent;
import com.tiantian.strangergame.akka.event.UserEvent;
import com.tiantian.strangergame.akka.event.UserEventResponse;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.manager.model.UserChips;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class TableActor extends UntypedActor {
    static Logger LOG = LoggerFactory.getLogger(TableActor.class);
    private String tableId;

    private Map<String, ActorRef> userActorMap = new ConcurrentHashMap<>();
    private Map<String, ActorRef> tableDbActorMap = new ConcurrentHashMap<>();

    public TableActor(String tableId) {
        this.tableId = tableId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) {
            if(message instanceof TableUserEvent) { // 用户在桌子上的操作
               if (((TableUserEvent)message).isNeedRet()) {
                   Object retObj = Handlers.INSTANCE.executeRet((TableUserEvent) message, getSelf(), getContext(), getSender());
                   getSender().tell(retObj, ActorRef.noSender());
                   return;
               }
               Handlers.INSTANCE.execute((TableUserEvent) message, getSelf(), getContext(), getSender());
            }
            else if (message instanceof TableTaskEvent) { // 定时任务操作
                Handlers.INSTANCE.execute((TableTaskEvent) message, getSelf(), getContext(), getSender());
            }
        }
        else if (message instanceof UserEvent) {  // 用户数据更新
            String userId = ((UserEvent) message).userId();
            ActorRef userActor = userActorMap.get(userId);
            if (userActor == null) {
                userActor = getContext().actorOf(Props.create(UserActor.class, userId)
                        .withDispatcher("userDbDispatcher"), "User_" + userId);
                userActorMap.put(userId, userActor);
            }
            userActor.tell(message, getSelf());
        }
        else if (message instanceof TableDbEvent) { // 桌子上的数据更新
            String tableId = ((TableDbEvent) message).tableId();
            ActorRef tableDbActor = tableDbActorMap.get(tableId);
            if (tableDbActor == null) {
                tableDbActor = getContext().actorOf(Props.create(TableDbActor.class, tableId)
                        .withDispatcher("dbDispatcher"), "TableDb_" + tableId);
                tableDbActorMap.put(tableId, tableDbActor);
            }
            tableDbActor.tell(message, getSelf());
        }
        else {
            unhandled(message);
        }
    }


    public void postStop() {
        destroyTableData();
        userActorMap.clear();
        tableDbActorMap.clear();
        LOG.info("Table Actor Closed, table_id is {}", tableId);
    }

    private void destroyTableData() {
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus != null && !tableStatus.isNull()) {
            tableStatus.delSelf();
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser != null) {
            Set<String> sitUsers = tableAllUser.getTableSitDownUsers();
            tableAllUser.delSelf();
            // 清理UserChips等数据
            if (sitUsers != null && sitUsers.size() > 0) {
                for (String userId : sitUsers) {
                    UserChips userChips = UserChips.load(userId, tableId);
                    if (userChips != null) {
                        userChips.delSelf(tableId);
                    }
                    TableUser tableUser = TableUser.load(userId);
                    if (tableUser != null) {
                        tableUser.delSelf(tableId);
                    }
                }
            }
        }

    }
//
//    private static SupervisorStrategy strategy =
//            new OneForOneStrategy(
//                    1, Duration.create("1 minute"),
//                    t -> {
//                         return SupervisorStrategy.resume();
//                    });
//
//    @Override
//    public SupervisorStrategy supervisorStrategy() {
//        return strategy;
//    }
}
