package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.cache.LocalCache;
import com.tiantian.strangergame.cache.MaintainInfo;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.utils.GameUtils;
import com.tiantian.strangergame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class TableDAndBHandler implements EventHandler<TableTaskEvent> {

    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String randomBtn = jsonObject.getString("btn");
        // 从redis中加载桌子状态数据
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null || tableStatus.isNull()) {
           return;
        }

        GameRecord gameRecord = new GameRecord();
        gameRecord.setGameType("STRANGER");
        gameRecord.setTableId(tableStatus.getTableId());
        gameRecord.setStartTime(System.currentTimeMillis());

        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        long buyIn = Long.parseLong(tableStatus.getBuyIn());
        long smallBlindMoney = Long.parseLong(tableStatus.getSmallBlindMoney());
        long bigBlindMoney = Long.parseLong(tableStatus.getBigBlindMoney());

        String oldButton = tableStatus.getButton();
        if (StringUtils.isNotBlank(randomBtn)) {
            oldButton = randomBtn;
        }
       // String[] results = getButtonAndBlind(tableAllUser.getJoinTableUserMap().keySet(), oldButton);
        String[] results = tableAllUser.getRightButtonAndBlind(oldButton);
        String button = results[0];
        String smallBlindNum = results[1];
        String bigBlindNum = results[2];

        Map<String, String> noWaitJoinUserMap = tableAllUser.getNoWaitBlindJoinUserMap(bigBlindNum);
        String inningId = UUID.randomUUID().toString().replace("-", "");

        gameRecord.setInnerId(inningId);
        // 设置牌局ID
        tableStatus.setInningId(inningId);
        // 设置庄家,小,大盲注
        tableStatus.setButton(button);
        tableStatus.setSmallBlindNum(smallBlindNum);
        tableStatus.setBigBlindNum(bigBlindNum);

        String maxUsersStr = tableStatus.getMaxUsers();
        int maxUsers = 0;
        if (StringUtils.isNotBlank(maxUsersStr)) {
            maxUsers = Integer.parseInt(maxUsersStr);
        }

        gameRecord.setSmallBlind(Integer.parseInt(smallBlindNum));
        gameRecord.setBigBlind(Integer.parseInt(bigBlindNum));
        gameRecord.setDealerNumber(Integer.parseInt(button));
        gameRecord.setTableType(maxUsers);

        // 设置游戏状态为 大小盲注
        tableStatus.setStatus(GameStatus.D_AND_B.name());
        // 添加可以参加游戏的玩家座位号
        tableStatus.addPlayingSits(noWaitJoinUserMap.keySet());
        tableStatus.addPlayingSitsAndId(noWaitJoinUserMap);
        // 设置当前需要下注人座位号
        tableStatus.setCurrentBet(bigBlindNum);
        tableStatus.setSitBetStatusNotFlush(tableStatus.getBigBlindNum(), GameEventType.CALL); // 大盲注默认为call
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");

        // 初始化 玩家的下注日志
        List<UserBetLog> logList = new ArrayList<>();
        Set<Map.Entry<String, String>> joinTableUserEntry = noWaitJoinUserMap.entrySet();
        for (Map.Entry<String, String> entry : joinTableUserEntry) {
            String sitNum = entry.getKey();
            String userId = entry.getValue();
            UserBetLog userBetLog = new UserBetLog();
            userBetLog.setUserId(userId);
            userBetLog.setSitNum(sitNum);
            userBetLog.setRoundChips(0);
            userBetLog.setTotalChips(0);
            logList.add(userBetLog);
        }
        tableStatus.setUsersBetsLog(JSON.toJSONString(logList));
        try {
            List<GameRecord.RecordUser> recordUsers = new ArrayList<>();
            List<String> sitNums = getBetOrderSits(tableAllUser.getJoinTableUserMap().keySet(), bigBlindNum);
            for (String sitNum : sitNums) {
                String userId = tableAllUser.getJoinTableUserMap().get(sitNum);
                GameRecord.RecordUser recordUser = new GameRecord.RecordUser();
                TableUser tbUser = TableUser.load(userId);
                recordUser.setId(userId);
                recordUser.setNickName(tbUser.getNickName());
                recordUser.setAvatarUrl(tbUser.getAvatarUrl());
                recordUser.setSitNumber(sitNum);
                recordUser.setScore(0l);
                UserChips userChips = UserChips.load(userId, tableStatus.getTableId());
                if (userChips != null) {
                    recordUser.setScore(userChips.getChips());
                }
                recordUsers.add(recordUser);
            }
            gameRecord.setRecordUsers(recordUsers);
            List<GameRecord.Progress> progresses = new ArrayList<>();
            progresses.add(GameRecord.Progress.create("initial", "nil", "nil", 0));
            progresses.add(GameRecord.Progress.create("samll_blind", smallBlindNum, tableStatus.getSmallBlindMoney(), 0));
            progresses.add(GameRecord.Progress.create("big_blind", bigBlindNum, tableStatus.getBigBlindMoney(), 0));
            gameRecord.setProgresses(progresses);
            // 保存数据
            RecordUtils.addRecord(gameRecord, tableStatus.getTableId());
        }catch (Exception e) {
            e.printStackTrace();
        }

        // 清空上局的牌
        tableStatus.setCards("");
        // 清空上局玩家的底牌
        tableStatus.setUserCards("");
        // 清空上局桌面的牌
        tableStatus.setDeskCards("");

        long smallBlindChips = 0;
        long bigBlindChips = 0;

        Map<String, String> blindUserMap = tableAllUser.getBlindTableUserMap();
        List<Map<String, Object>> blindBetMaps = new ArrayList<>();
        // 设置玩家大盲注玩家状态为check
        String bigBlindUserId = noWaitJoinUserMap.get(bigBlindNum);
        for (Map.Entry<String, String> entry : joinTableUserEntry) {
             String sitNum = entry.getKey();
             String userId = entry.getValue();
             TableUser tableUser = TableUser.load(userId);
             if (tableUser == null || tableUser.isNull()) {
                 continue;
             }
             tableUser.setStatus(GameConstants.USER_STATUS_GAMING);
             // 判断自动下大盲
             if (blindUserMap.containsKey(sitNum)) {
                 UserChips userChips = UserChips.load(userId, tableId);
                 long betBlindChips = Math.min(userChips.getChips(), bigBlindMoney);
                 if (blindUserMap.containsKey(smallBlindNum)) {
                     smallBlindChips = betBlindChips;
                 }
                 if (blindUserMap.containsKey(bigBlindNum)) {
                     bigBlindChips = betBlindChips;
                 }
                 userChips.reduceAndFlushChips(betBlindChips);
                 tableStatus.userBet(sitNum, betBlindChips);
                 // 判断是不是allin
                 if (userChips.getChips() <= 0) {
                     tableUser.setBetStatus(GameEventType.ALLIN);
                     tableStatus.setSitBetStatusNotFlush(sitNum, GameEventType.ALLIN);
                     sendUserAllin(tableStatus, userId, 0, Integer.parseInt(sitNum),
                             tableAllUser.getOnlineTableUserIds());
                 }
                 else {
                     tableUser.setBetStatus(GameEventType.CHECK);
                 }
                 if (!sitNum.equalsIgnoreCase(smallBlindNum) && !sitNum.equalsIgnoreCase(bigBlindNum)) {
                     // 不是大小盲位置
                     Map<String, Object> map = new HashMap<>();
                     map.put("s", Integer.parseInt(sitNum));
                     map.put("m",  betBlindChips);
                     blindBetMaps.add(map);
                 }
             }
             // 当前位置是小盲注没有下注
             if(smallBlindNum.equalsIgnoreCase(sitNum) && smallBlindChips == 0) {
                UserChips smallUserChips = UserChips.load(userId, tableId);
                smallBlindChips = Math.min(smallUserChips.getChips(), smallBlindMoney);
                smallUserChips.reduceAndFlushChips(smallBlindChips);
                tableStatus.userBet(smallBlindNum, smallBlindChips);
                // 判断是不是allin
                if (smallUserChips.getChips() <= 0) {
                    tableUser.setBetStatus(GameEventType.ALLIN);
                    tableStatus.setSitBetStatusNotFlush(tableStatus.getSmallBlindNum(), GameEventType.ALLIN);
                    sendUserAllin(tableStatus, userId, 0, Integer.parseInt(smallBlindNum),
                             tableAllUser.getOnlineTableUserIds());
                }
             }
            // 当前位置是大盲注没有下注
            if (bigBlindNum.equalsIgnoreCase(sitNum) && bigBlindChips == 0) {
                UserChips bigUserChips = UserChips.load(bigBlindUserId, tableId);
                bigBlindChips = Math.min(bigUserChips.getChips(), bigBlindMoney);
                bigUserChips.reduceAndFlushChips(bigBlindChips);
                tableStatus.userBet(bigBlindNum, bigBlindChips);
                // 判断是不是allin
                if (bigUserChips.getChips() <= 0) {
                    tableUser.setBetStatus(GameEventType.ALLIN);
                    tableStatus.setSitBetStatusNotFlush(tableStatus.getBigBlindNum(), GameEventType.ALLIN);
                    sendUserAllin(tableStatus, bigBlindUserId, 0, Integer.parseInt(bigBlindNum),
                            tableAllUser.getOnlineTableUserIds());
                }
            }
            tableUser.save();
        }
        if (bigBlindMoney == bigBlindChips) {
            tableStatus.setMaxBetInfo(bigBlindNum, bigBlindMoney); //重新设置最大下注位,
        }
        // 保存到redis
        tableStatus.save();
        List<Map<String, Object>> maps = new ArrayList<>();
        for (Map.Entry<String, String> entry : joinTableUserEntry) {
            Map<String, Object> map = new HashMap<>();
            UserChips userChips = UserChips.load(entry.getValue(), tableId);
            map.put("s", Integer.parseInt(entry.getKey()));
            map.put("m", userChips.getChips());
            maps.add(map);
        }

        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("btn", Integer.parseInt(button)); // 庄家位
        object.put("smb", Integer.parseInt(smallBlindNum)); // 小盲注座位号
        object.put("smbm", smallBlindChips); //设置测试小盲注值
        object.put("bgb", Integer.parseInt(bigBlindNum)); // 大盲注座位号
        object.put("bgbm", bigBlindChips); //设置测试大盲注

        object.put("umoney", maps);
        object.put("bets", blindBetMaps);
        // 通知所有玩家
        sendToUsers(object, tableAllUser.getOnlineTableUserIds(), tableId);

        // 立即触发pre_flop
        JSONObject eventParam = new JSONObject();
        eventParam.put("tableId", tableId);
        eventParam.put("inningId", inningId);

        TableTaskEvent tableTaskEvent = new TableTaskEvent();
        tableTaskEvent.setEvent(GameStatus.PRE_FLOP.name());
        tableTaskEvent.setParams(eventParam);
        Handlers.INSTANCE.execute(tableTaskEvent, self, context, sender);

        // 开始检测游戏状态任务
        GameUtils.tableStatusTask(inningId, tableId);
    }

    private static String[] getButtonAndBlind(Set<String> sitNumSet, String oldButton) {
        // button 位置
        String button = null;
        // 小盲注座位号
        String sbSitNum = null;
        // 大盲注座位号
        String bbSitNum = null;
        // 转换进行排序
        TreeSet<String> sitTreeSet = new TreeSet<>(sitNumSet);
        // 没有button位置，则座位最小号为button位
        if (StringUtils.isBlank(oldButton)) {
            button = sitTreeSet.first();
        } else { // 之前存在了button位置，则算出下一位button
            Iterator<String> iterator = sitTreeSet.iterator();
            while (iterator.hasNext()) {
                String sitNum = iterator.next();
                // 第一个比oldButton大的值
                if (Integer.parseInt(sitNum) > Integer.parseInt(oldButton)) {
                    button = sitNum;
                    break;
                }
            }
            // 如果没有找到则说明 oldButton是最大值，则设置座位队列第一个为button位
            if (button == null) {
                button = sitTreeSet.first();
            }
        }
        // 获取大盲注和小盲注座位号
        Iterator<String> sbIterator = sitTreeSet.iterator();
        while (sbIterator.hasNext()) {
            String sitNum = sbIterator.next();
            // 比button座位号大
            if (Integer.parseInt(sitNum) > Integer.parseInt(button)) {
                if (sbSitNum == null) {
                    sbSitNum = sitNum;
                    break;
                }
            }
        }
        // 小盲注没找到则第一个为小盲注
        if (sbSitNum == null) {
            sbSitNum = sitTreeSet.first();
        }
        // 查找大盲注
        Iterator<String> bbIterator = sitTreeSet.iterator();
        while (bbIterator.hasNext()) {
            String sitNum = bbIterator.next();
            // 比button座位号大
            if (Integer.parseInt(sitNum) > Integer.parseInt(sbSitNum)) {
                if (bbSitNum == null) {
                    bbSitNum = sitNum;
                    break;
                }
            }
        }
        // 大盲注没找到则第一个为大盲注
        if (bbSitNum == null) {
            bbSitNum = sitTreeSet.first();
        }
        return new String[]{button, sbSitNum, bbSitNum};
    }

    private void sendToUsers(JSONObject object, Collection<String> userIds, String tableId) {
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.DANDB, userIds, id, tableId);
    }

    private void sendUserAllin(TableStatus tableStatus, String userId, int val, int sitNum, Collection<String> userIds) {
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("uid", userId);
        object.put("sn", sitNum);
        //设置allin的数值
        object.put("val", val);
        object.put("bottom_pool", tableStatus.getTotalPoolMoney());
        object.put("left_chips", 0);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.ALLIN, userIds, id, tableStatus.getTableId());
    }

    private List<String> getBetOrderSits(Set<String> sitNumSets, String bigBlindNum) {
        String tmpSit = bigBlindNum;
        List<String> list = new ArrayList<>();
        for (int i = 0; i < sitNumSets.size(); i++) {
            String next = GameUtils.getNextBetSitNum(sitNumSets, tmpSit, tmpSit);
            tmpSit = next;
            list.add(next);
        }
        return list;
    }

}
