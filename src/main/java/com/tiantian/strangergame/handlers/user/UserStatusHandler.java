package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.google.common.collect.Maps;
import com.tiantian.stranger.akka.user.TableUserStatusEvent;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.model.TableAllUser;
import java.util.Collection;
import java.util.Map;

/**
 *
 */
public class UserStatusHandler implements EventRetHandler<TableUserStatusEvent> {
    @Override
    public Object handler(TableUserStatusEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String tableId = event.tableId();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Map<String, String> joinUsers = tableAllUser.getJoinTableUserMap();
        Collection<String> onlineUserIds = tableAllUser.getOnlineTableUserIds();
        Map<String, String> result = Maps.newHashMap();
        if (joinUsers != null) {
            Collection<String> joinUserIds = joinUsers.values();
            for (String joinUserId : joinUserIds) {
                 onlineUserIds.remove(joinUserId);
                 result.put(joinUserId, "join");
            }
        }
        if (onlineUserIds != null && onlineUserIds.size() > 0) {
            for (String uId : onlineUserIds) {
                result.put(uId, "stand");
            }
        }
        return result;
    }
}
