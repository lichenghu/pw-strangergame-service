package com.tiantian.strangergame.utils;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tiantian.fc.proxy_client.FriendCoreIface;
import com.tiantian.fc.thrift.core.StrangerWinLoseLog;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.strangergame.cache.LocalCache;
import com.tiantian.strangergame.cache.MaintainInfo;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.*;
import com.tiantian.strangergame.manager.texas.PokerManager;
import com.tiantian.strangergame.manager.texas.PokerOuts;
import com.tiantian.system.proxy_client.NotifierIface;
import com.tiantian.system.thrift.notifier.NoticeType;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class GameUtils {
    static Logger LOG = LoggerFactory.getLogger(GameUtils.class);
    /**
     * 获取除一次大小盲注下注的 开始下注座位号,每次都从小盲注开始，如果小盲注弃牌则按顺序下一个
     * 如果当前局开局的时候只有2个人 除了pre-flop圈以后每圈都是从大盲注开始下注
     * @return
     */
    public static String getPerBeginBetSitNum(Set<String> canBetSitNumSets, String smallBlindSitNum,
                                              String bigBlindSitNum, boolean onlyTwoJoin) {
        if (onlyTwoJoin) { // 每次都从大盲注开叫
            return bigBlindSitNum;
        }
        TreeSet<String> treeSet = new TreeSet<>(canBetSitNumSets);
        for(String sitNum : treeSet) {
            if (Integer.parseInt(sitNum) >= Integer.parseInt(smallBlindSitNum)) {
                return sitNum;
            }
        }
        if (treeSet.size() > 0) {
            // 没有找到则第一个
            return treeSet.first();
        }
        return null;
    }

    // 获取下一个下注的座位号
    public static String getNextBetSitNum(Set<String> sitNumSets, String currentBetSitNum, String maxBetSitNum) {
        LOG.info("getNextBetSitNum:sitNumSets"+JSON.toJSONString(sitNumSets) + ";currentBetSitNum:" + currentBetSitNum +";maxBetSitNum:" +maxBetSitNum);
        // 如果能下注的座位列表不包括最大下注，则加入列表中。最大下注座位有可能退出了房间
        boolean maxBetIsExit = false; // 最大下注是否已经退出房间
        if (maxBetSitNum != null && !sitNumSets.contains(maxBetSitNum)) {
            sitNumSets.add(maxBetSitNum);
            maxBetIsExit = true;
        }
        TreeSet<String> treeSet = new TreeSet<>(sitNumSets);
        for (String sitNum : treeSet) {
             if (StringUtils.isNotBlank(currentBetSitNum)
                    && Integer.parseInt(sitNum) > Integer.parseInt(currentBetSitNum)) {
                // 下一个玩家是最大下注者，并且这个最大下注者已经退出房间，则没有需要下注的
                if(sitNum.equalsIgnoreCase(maxBetSitNum) && maxBetIsExit) {
                   return null;
                }
                return sitNum;
             }
        }
        if (treeSet.size() > 0) {
            // 没有找到则第一个
            String sitNum = treeSet.first();
            if (sitNum.equalsIgnoreCase(currentBetSitNum)) {
                return null;
            }
            // 下一个玩家是最大下注者，并且这个最大下注者已经退出房间，则没有需要下注的
            if(sitNum.equalsIgnoreCase(maxBetSitNum) && maxBetIsExit) {
                return null;
            }
            return sitNum;
        }
        return null;
    }

    // 触发检测下注任务
    public static void triggerBetTestTask(String tableId,
                                          String inningId, String sitNum, String betUserId,
                                          int operateCount, String pwd) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TEST_BET);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("sitNum", sitNum);
        object.put("userId", betUserId);
        object.put("operateCount", operateCount + "");
        object.put("pwd", pwd);
        try {
            RedisTaskIface.instance().iface().pushTask(object.toJSONString(), GameConstants.BET_DELAYER_TIME + 2000);
        } catch (TException e) {
            e.printStackTrace();
        }
    }


    // 触发检测下个任务
    public static void triggerNextStatusTask(GameStatus nextStatus,
                                             String tableId,
                                             String inningId) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TEST_STATUS);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("selector", nextStatus.name());
        long delay = GameConstants.STATUS_DELAYER_TIME;
        if (nextStatus.name().equalsIgnoreCase(GameStatus.FINISH.name())) {
            delay = 0;
        }
        try {
            RedisTaskIface.instance().iface().pushTask(object.toJSONString(), delay);
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    // 触发检测下个任务
    public static void triggerNextStatusTask(GameStatus nextStatus,
                                             String tableId,
                                             String inningId,
                                             boolean hasSafe) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TEST_STATUS);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        object.put("selector", nextStatus.name());
        long delay = GameConstants.STATUS_DELAYER_TIME;
        if (nextStatus.name().equalsIgnoreCase(GameStatus.FINISH.name())) {
            delay = 0;
        }
        if (hasSafe) {
            delay = GameConstants.BUY_SAFE_DELAYER_TIME;
        }
        try {
            RedisTaskIface.instance().iface().pushTask(object.toJSONString(), delay);
        } catch (TException e) {
            e.printStackTrace();
        }
    }
    // 发送消息给玩家
    public static void notifyUsers(JSONObject userData, String event,  Collection<String> toUserIds, String id,
                                   String tableId) {
        try {
            if (toUserIds == null) {
                return;
            }
            if (userData != null) {
                // 添加游戏消息来源
                userData.put(GameConstants.MSG_SOURCE, "stranger_scene");
                if (StringUtils.isNotBlank(tableId)) {
                    userData.put(GameConstants.UNIQUE_ID, tableId);
                }
            }
            LOG.info("notifyUsers: notify toUserIds" + JSON.toJSONString(toUserIds));
            LOG.info("notifyUsers: notify userData" + userData.toJSONString());
            String type = NoticeType.GAME.toString();
            String data = NotifierDataUtils.generate(id, type, userData.toJSONString());
            // 传输数据
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put(GameConstants.DATA_STR, data);
            dataMap.put(GameConstants.EVENT, event);
            for (String toUserId : toUserIds) {
                String serverId = RedisUtil.get(GameConstants.ROUTER_KEY + toUserId);
                if (StringUtils.isBlank(serverId)) {
                    continue;
                }
                // 分发到指定的服务器上
                dataMap.put(GameConstants.SERVER_ID, serverId);
                dataMap.put(GameConstants.TO, toUserId);
                try {
                    // 分发到game的队列中
                    NotifierIface.instance().iface().notify(NoticeType.GAME.getValue(), "StrangerGameService", dataMap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void notifyUser(JSONObject userData, String event, String toUserId, String tableId) {
        try {
            if (userData != null) {
                // 添加游戏消息来源
                userData.put(GameConstants.MSG_SOURCE, "stranger_scene");
                if (StringUtils.isNotBlank(tableId)) {
                    userData.put(GameConstants.UNIQUE_ID, tableId);
                }
            }
            LOG.info("notifyUser: notify toUserId" + toUserId);
            LOG.info("notifyUser: notify userData" + userData.toJSONString());
            String id = UUID.randomUUID().toString().replace("-", "");
            String type = NoticeType.GAME.toString();
            String data = NotifierDataUtils.generate(id, type, userData.toJSONString());
            if (data == null) {
                return;
            }
            // 传输数据
            Map<String, String> dataMap = new HashMap<>();
            dataMap.put(GameConstants.DATA_STR, data);
            dataMap.put(GameConstants.EVENT, event);

            String serverId = RedisUtil.get(GameConstants.ROUTER_KEY + toUserId);
            if (StringUtils.isBlank(serverId)) {
                return;
            }
            // 分发到指定的服务器上
            dataMap.put(GameConstants.SERVER_ID, serverId);
            dataMap.put(GameConstants.TO, toUserId);
            // 分发到game的队列中
            NotifierIface.instance().iface().notify(NoticeType.GAME.getValue(), "StrangerGameService", dataMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    public static void notifyUser(JSONArray userData, String event, String toUserId) {
//        try{
//            String id = UUID.randomUUID().toString().replace("-", "");
//            String type = NoticeType.GAME.toString();
//            String data = NotifierDataUtils.generate(id, type, userData.toJSONString());
//            if (data == null) {
//                return;
//            }
//            // 传输数据
//            Map<String, String> dataMap = new HashMap<>();
//            dataMap.put(GameConstants.DATA_STR, data);
//            dataMap.put(GameConstants.EVENT, event);
//
//            String serverId = RedisUtil.get(GameConstants.ROUTER_KEY + toUserId);
//            if (StringUtils.isBlank(serverId)) {
//                return;
//            }
//            // 分发到指定的服务器上
//            dataMap.put(GameConstants.SERVER_ID, serverId);
//            dataMap.put(GameConstants.TO, toUserId);
//            // 分发到game的队列中
//            NotifierUtil.getInstance().notify(NoticeType.GAME.getValue(), "SpingoGameService", dataMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }

    public static GameStatus nextGameStatus(String currentStatus) {
        return GameStatus.next(currentStatus);
    }

    public static GameStatus checkNextStatus(TableStatus tableStatus,
                                             String currentStatus,
                                             String maxBetSitNum,
                                             String currentBet) {

        String nextBetSit = getNextBetSitNum(tableStatus.canBetSits(), currentBet, maxBetSitNum);
        // 先判断pre-flop阶段
        if(GameStatus.PRE_FLOP.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return preFlopStatus(tableStatus, currentStatus, maxBetSitNum, currentBet, nextBetSit);
        }
        // 直接结束结算
        if (tableStatus.onlyOneNotFold()) {
            return GameStatus.FINISH;
        }

        // 全部弃牌
        if (tableStatus.allFold()) {
            return GameStatus.END;
        }

        // 直接进入下一个状态
        if (tableStatus.allAllin() || tableStatus.onlyOneNotAllin() || tableStatus.allFoldOrAllin()) {
            return nextGameStatus(currentStatus);
        }

        // 没有人需要下注，allin和弃牌
        if (nextBetSit == null || nextBetSit.equalsIgnoreCase(maxBetSitNum)) {
            return nextGameStatus(tableStatus.getStatus());
        }
        return null;
    }

    private static GameStatus preFlopStatus(TableStatus tableStatus,
                                     String currentStatus,
                                     String maxBetSitNum,
                                     String currentBet,
                                     String nextBetSit) {
        // 直接结束结算
        if (tableStatus.onlyOneNotFold()) {
            return GameStatus.FINISH;
        }
        // 全部弃牌
        if (tableStatus.allFold()) {
            return GameStatus.END;
        }

        // 直接进入下一个状态
        if (tableStatus.allAllin() || tableStatus.allFoldOrAllin()) {
            return nextGameStatus(currentStatus);
        }

        // 判断下一个是不是大盲注位置
        if (nextBetSit != null && nextBetSit.equalsIgnoreCase(tableStatus.getBigBlindNum())) {
            // 大盲注没有操作过
            UserBetLog userBetLog = tableStatus.getUserBetLog(nextBetSit);
            if (userBetLog != null) {
                long totalChips =  userBetLog.getRoundChips() + userBetLog.getTotalChips();
                if(totalChips <= Long.parseLong(tableStatus.getBigBlindMoney())) {
                    return null;
                }
            } else {
                return null;
            }
        }
        // 判断当前下注的是不是大盲注位
        if(currentBet != null && currentBet.equalsIgnoreCase(tableStatus.getBigBlindNum())) {
            // 判断当前最大下注是不是一个大盲注,表示大盲注位第一次跟注
            if (tableStatus.getMaxBetSitNumChips().equalsIgnoreCase(tableStatus.getBigBlindMoney())) {
                return nextGameStatus(currentStatus);
            }
        }

        if (tableStatus.onlyOneNotAllin()) {
            return nextGameStatus(currentStatus);
        }
        // 没有人需要下注，allin和弃牌
        if (nextBetSit == null || nextBetSit.equalsIgnoreCase(maxBetSitNum)) {
            return nextGameStatus(tableStatus.getStatus());
        }
        return null;
    }


    public static GameStatus beforeCheckNextStatus(String currentStatus,
                                                   TableStatus tableStatus) {
        // 直接进入下一个状态
        if (tableStatus.allAllinExceptFold() || tableStatus.onlyOneNotAllin()) {
            return nextGameStatus(currentStatus);
        }
        // 直接结束结算
        if (tableStatus.onlyOneNotFold()) {
            return GameStatus.FINISH;
        }
        // 全部弃牌
        if (tableStatus.allFold()) {
            return GameStatus.END;
        }
        return null;
    }

    public static void nextStatusTask(GameStatus nextStatus, String tableId, String inningId,
                                      ActorRef self, UntypedActorContext context, ActorRef sender) {
        if (nextStatus != null) {
            // 立即触发下一个
            JSONObject eventParam = new JSONObject();
            eventParam.put("tableId", tableId);
            eventParam.put("inningId", inningId);
            TableTaskEvent event = new TableTaskEvent();
            event.setEvent(nextStatus.name());
            event.setParams(eventParam);
            Handlers.INSTANCE.execute(event, self, context, sender);
        }
    }

    // 通知下一个玩家下注
    public static void noticeNextUserBet(String tableId, TableStatus tableStatus,
                                         String maxBetSitNum, boolean isPerBegin,
                                         String maxBetSitNumChips, TableAllUser tableAllUser,
                                         ActorRef self, UntypedActorContext context,
                                         ActorRef sender) {
        Set<String> canBetSets = tableStatus.canBetSits();
        String smallBlindNum = tableStatus.getSmallBlindNum();
        String beginBetSitNum = "";
        // 判断是否是每轮的开始
        if (isPerBegin) {
            beginBetSitNum = getPerBeginBetSitNum(canBetSets, smallBlindNum, tableStatus.getBigBlindNum(), tableStatus.getUsersCards().size() == 2);
            if (StringUtils.isNotBlank(beginBetSitNum)) {
                tableStatus.setMaxBetSitNum(beginBetSitNum);
                tableStatus.setMaxBetSitNumChips("0");
            } else {
                // 直接进入下个发牌
                GameStatus nextStatus = checkNextStatus(tableStatus, tableStatus.getStatus(), maxBetSitNum, null);
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean hasSafe = GameUtils.checkSafe(tableStatus, tableAllUser);
                    // 保存数据
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    if (hasSafe) {
                        // 触发下一轮的发牌事件
                        GameUtils.triggerNextStatusTask(nextStatus, tableId, tableStatus.getInningId(), true);
                    }
                    else {
                        // 触发下一轮的发牌事件
                        GameUtils.nextStatusTask(nextStatus, tableId, tableStatus.getInningId(), self, context, sender);
                    }

                    return;
                } else {
                    throw new RuntimeException("can not found next GameStatus");
                }
            }
        }
        else {
            beginBetSitNum = getNextBetSitNum(canBetSets, tableStatus.getCurrentBet(), maxBetSitNum);
            if (StringUtils.isBlank(beginBetSitNum)) {
                GameStatus nextStatus = checkNextStatus(tableStatus, tableStatus.getStatus(), maxBetSitNum, null);
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean hasSafe = GameUtils.checkSafe(tableStatus, tableAllUser);
                    // 保存数据
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    if (hasSafe) {
                        // 触发下一轮的发牌事件
                        GameUtils.triggerNextStatusTask(nextStatus, tableId, tableStatus.getInningId(), true);
                    }
                    else {
                        // 触发下一轮的发牌事件
                        GameUtils.nextStatusTask(nextStatus, tableId, tableStatus.getInningId(), self, context, sender);
                    }
                    return;
                } else {
                    throw new RuntimeException("can not found next GameStatus");
                }
            }
            if (StringUtils.isNotBlank(maxBetSitNum)) {
                // 当最大下注的人座位号不为空则直接设置
                tableStatus.setMaxBetSitNum(maxBetSitNum);
            }
            if (StringUtils.isNotBlank(maxBetSitNumChips)) {
                // 设置最大下注
                tableStatus.setMaxBetSitNumChips(maxBetSitNumChips);
            }
        }
        // 设置当前需要下注人座位号
        tableStatus.setCurrentBet(beginBetSitNum);
        // 设置当前需要下注人座位号开始时间
        tableStatus.setCurrentBetTimes( System.currentTimeMillis() + "");
        String indexCount = tableStatus.getIndexCount();
        String pwd = tableStatus.randomPwd();
        // 刷新修改的
        tableStatus.save();

        //通知下注的玩家需要下注
        String betUserId = tableAllUser.getGamingSitUserMap().get(beginBetSitNum);
        JSONObject userData = new JSONObject();
        userData.put("inner_id", tableStatus.getInningId());
        userData.put("inner_cnt", indexCount);
        userData.put("uid", betUserId);
        userData.put("sn", Integer.parseInt(beginBetSitNum));
        userData.put("t", GameConstants.BET_DELAYER_TIME/1000);

        String[] ops = tableStatus.getUserCanOps(betUserId, beginBetSitNum);
        userData.put("c_b", StringUtils.join(ops, ","));
        userData.put("pwd", pwd);

        // 通知下注的玩家进行下注
        notifyUser(userData, GameEventType.BET, betUserId, tableId);


        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", indexCount);
        otherNextObject.put("uid", betUserId);
        otherNextObject.put("sn", Integer.parseInt(beginBetSitNum));
        otherNextObject.put("t", GameConstants.BET_DELAYER_TIME/1000);
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", "");
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        userIds.remove(betUserId);

        String newId = UUID.randomUUID().toString().replace("-", "");
        notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableId);


        TableUser betTableUser = TableUser.load(betUserId);
        if(StringUtils.isBlank(betTableUser.getOperateCount())) {
            betTableUser.setOperateCount("1");
            betTableUser.save();
        }
        // 触发30s的检测任务
        triggerBetTestTask(tableId, tableStatus.getInningId(), beginBetSitNum, betUserId,
                Integer.parseInt(betTableUser.getOperateCount()), pwd);
    }

    // 通知每个玩家的牌行
    public static void noticeCardsLevel(TableStatus tableStatus, TableAllUser tableAllUser) {
        Map<String, String> sitCardsMap = tableStatus.allSitNumsAndCards();
        Map<String, PokerOuts> pokerOutsMap = PokerManager.getUsersPokerOutsList(sitCardsMap,
                                                                                         tableStatus.getDeskCardList());
        Set<Map.Entry<String, PokerOuts>> mapEntrySet = pokerOutsMap.entrySet();
        for (Map.Entry<String, PokerOuts> entry : mapEntrySet) {
             String sitNum = entry.getKey();
             if (tableAllUser.getGamingSitUserMap() == null) {
                 continue;
             }
             String userId = tableAllUser.getGamingSitUserMap().get(sitNum);
             if (StringUtils.isBlank(userId)) {
                 continue;
             }
             PokerOuts pokerOuts = entry.getValue();
             JSONObject userData = new JSONObject();
             userData.put("inner_id", tableStatus.getInningId());
             userData.put("inner_cnt", tableStatus.getIndexCount());
             userData.put("level", pokerOuts.getLevel());
             notifyUser(userData, GameEventType.CARD_LEVEL, userId, tableStatus.getTableId());
        }
    }

    public static void notifyUserPoolInfo(TableStatus tableStatus, TableAllUser tableAllUser) {
        //分池信息没有变化则不发送分池信息
        if (tableStatus.getLastBetPool() != null &&
                tableStatus.getLastBetPool().equalsIgnoreCase(tableStatus.getBetPool())) {
            return;
        }
        List<Long> poolList = tableStatus.betPoolList();
        JSONObject $object = new JSONObject();
        $object.put("inner_id", tableStatus.getInningId());
        $object.put("inner_cnt", tableStatus.getIndexCount());
        $object.put("pool_info", StringUtils.join(poolList, ","));
        String id = UUID.randomUUID().toString().replace("-", "");
        notifyUsers($object, GameEventType.POOL_INFO, tableAllUser.getOnlineTableUserIds(), id, tableStatus.getTableId());
    }

    // 清除掉掉线并站起状态的玩家
    public static void clearStandUpOnlineUsers(TableAllUser tableAllUser, String tableId) {
        Collection<String> onlineTableUserIds = tableAllUser.getOnlineTableUserIds();
        if (onlineTableUserIds != null && onlineTableUserIds.size() > 0) {
            for (String userId : onlineTableUserIds) {
                boolean isOnline = RedisUtil.exists(GameConstants.ROUTER_KEY + userId);
                if (isOnline) {
                    continue;
                }
                //不在线，判断游戏状态是否是站起
                TableUser tableUser = TableUser.load(userId);
                if (tableUser != null) {
                    // 判断是否是站起状态
                    if ("standing".equalsIgnoreCase(tableUser.getStatus())) {
                        // 删除该桌子下的在线玩家
                        tableAllUser.delJoinOnline(userId);
                        tableUser.delSelf(tableId);
                    }
                }
            }
        }
    }

    public static void tableStatusTask(String inningId ,String tableId) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TABLE_STATUS_CHECK);
        object.put("tableId", tableId);
        object.put("inningId", inningId);
        try {
            RedisTaskIface.instance().iface().pushTask(object.toJSONString(), GameConstants.TABLE_STATUS_DELAYER_TIME);
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    public static void triggerUserOnlineCheckTask(String tableId, String taskId) {
        JSONObject object = new JSONObject();
        object.put(GameConstants.TASK_EVENT, GameConstants.TABLE_ONLINE_USER_CHECK);
        object.put("tableId", tableId);
        object.put("taskId", taskId);
        try {
            RedisTaskIface.instance().iface().pushTask(object.toJSONString(), GameConstants.TABLE_ONLINE_USER_DELAYER_TIME);
        } catch (TException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkSafe(TableStatus tableStatus, TableAllUser tableAllUser) {
       // Map<String, Map<String, Object>> buySafeInf = tableStatus.buySafe();
        Object[] buySafeInfResult = tableStatus.buySafe();
        Map<String, String> gamingUsersMap = tableAllUser.getGamingSitUserMap();
        Collection<String> onlineUserIds = tableAllUser.getOnlineTableUserIds();

        if (buySafeInfResult != null) {
            Map<String, Object> noticeInf = (Map<String, Object>) buySafeInfResult[0];
            if (noticeInf != null) {
                String type = (String) noticeInf.get("type");
                List<String> sits = ( List<String>) noticeInf.get("sitNums");
                JSONObject object2 = new JSONObject();
                object2.put("inner_id", tableStatus.getInningId());
                object2.put("inner_cnt", tableStatus.getIndexCount());
                object2.put("type", type);
                List<String> userIds = Lists.newArrayList();
                for (String sit : sits) {
                     String userId = gamingUsersMap.get(sit);
                     userIds.add(userId);
                }
                notifyUsers(object2, GameEventType.SAFE_NOTICE, userIds, UUID.randomUUID().toString().replace("-", ""),
                        tableStatus.getTableId());
                return false;
            }
            Map<String, Map<String, Object>> buySafeInf = (Map<String, Map<String, Object>>)buySafeInfResult[1];
            List<Map<String, Object>> safeInfos = new ArrayList<>();
            Set<Map.Entry<String, Map<String, Object>>> entrySet = buySafeInf.entrySet();
            for (Map.Entry<String, Map<String, Object>> entry : entrySet) {
                String sitNum = entry.getKey();
                String userId = gamingUsersMap.get(sitNum);
                if (StringUtils.isBlank(userId)) {
                    continue;
                }
                Map<String, Object> outsMap = entry.getValue();

                Map<String, Object> outsSafeInf = Maps.newHashMap();
                outsSafeInf.put("uid", userId);
                outsSafeInf.put("sn", Integer.valueOf(sitNum));
                outsSafeInf.put("outs", outsMap.get("outs") == null ? Lists.newArrayList() : outsMap.get("outs"));
                outsSafeInf.put("price", outsMap.get("price") == null ? Lists.newArrayList() : outsMap.get("price"));
                outsSafeInf.put("left_secs", (GameConstants.BUY_SAFE_DELAYER_TIME / 1000l) - 4);
                long leftSafe = 0;
                try {
                    StrangerWinLoseLog strangerWinLoseLog = FriendCoreIface.instance().iface().getUserStrangerGroupWinLoseLog(tableStatus.getTableId(), userId);
                    if (strangerWinLoseLog != null) {
                        leftSafe = strangerWinLoseLog.getMoneyLeft();
                    }
                }
                catch (Exception e) {
                   e.printStackTrace();
                }
                outsSafeInf.put("left_safe", leftSafe);
                safeInfos.add(outsSafeInf);
            }
            noticeUserBuySafe(onlineUserIds, tableStatus.getInningId(),
                    tableStatus.getIndexCount(), safeInfos, tableStatus.getTableId());
            return true;
        }
        return false;
    }

    public static void noticeUserBuySafe(Collection<String> allUserIds,
                                         String inningId, String indexCount,
                                         List<Map<String, Object>> safeInfo,
                                         String tableId) {
        for (String userId : allUserIds) {
            List<Map<String, Object>> userSafeList = Lists.newArrayList();
            for (Map<String, Object> map : safeInfo) {
                 Map<String, Object> safeMap = Maps.newConcurrentMap();
                 String uid = (String) map.get("uid");
                 Integer sn = (Integer)map.get("sn");
                 Object outs = map.get("outs");
                 Object price = map.get("price");
                 Object leftSafe = map.get("left_safe");
                 Long leftSecs = (Long) map.get("left_secs");
                 safeMap.put("sn", sn);
                 safeMap.put("left_secs", leftSecs);
                 if (uid.equalsIgnoreCase(userId)) {
                     safeMap.put("outs", outs);
                     safeMap.put("price", price);
                 }
                 else {
                     safeMap.put("outs", Lists.newArrayList());
                     safeMap.put("price", Lists.newArrayList());
                 }
                safeMap.put("left_safe", leftSafe);
                userSafeList.add(safeMap);
            }
            JSONObject object2 = new JSONObject();
            object2.put("inner_id", inningId);
            object2.put("inner_cnt", indexCount);
            object2.put("t", (GameConstants.BET_DELAYER_TIME / 1000) * 2);
            object2.put("user_safe", userSafeList);
            notifyUser(object2, GameEventType.BUY_SAFE, userId, tableId);
        }

    }
    // 检查维护信息
    public static boolean checkMaintainInfo(TableStatus tableStatus, TableAllUser tableAllUser) {
        MaintainInfo maintainInfo = LocalCache.getAppStatus();
        String beginMaintainTimes = tableStatus.getNextNoticeMaintainTimes() == null ? "" : tableStatus.getNextNoticeMaintainTimes();
        if (maintainInfo != null) {
            long beginDate = maintainInfo.getBeginDate();
            if(System.currentTimeMillis()  >= beginDate) {
                noticeStartMainInfo(tableStatus.getInningId(), tableStatus.getIndexCount(),
                        tableAllUser.getOnlineTableUserIds(), tableStatus.getTableId());
                return true;
            }

            String lastDate = tableStatus.getNextNoticeMaintainTimes();
            if (StringUtils.isBlank(lastDate)) {
                String timeStr = DateUtils.getFitTimes((beginDate - System.currentTimeMillis()) / 1000);
                if (timeStr != null) {
                    String msg = "朋友场将在" + timeStr + "后关闭服务器进行维护";
                    noticeMainInfo(tableStatus.getInningId(), tableStatus.getIndexCount(),
                            tableAllUser.getOnlineTableUserIds(), msg, tableStatus.getTableId());
                }
                long next = DateUtils.getNextFitTime(beginDate, System.currentTimeMillis());
                tableStatus.setNextNoticeMaintainTimes(next + "");
            }
            else {
                long lastTimes = Long.parseLong(lastDate);
                if (System.currentTimeMillis() >= lastTimes) {
                    // 相差分钟
                    long minTime = (System.currentTimeMillis() - lastTimes) / 60000;
                    String timeStr = DateUtils.getFitTimes((beginDate - (minTime < 1 ? lastTimes : System.currentTimeMillis())) / 1000);
                    if (timeStr != null) {
                        String msg = "朋友场将在" + timeStr + "后维护";
                        noticeMainInfo(tableStatus.getInningId(), tableStatus.getIndexCount(),
                                tableAllUser.getOnlineTableUserIds(), msg, tableStatus.getTableId());
                    }
                    long next = DateUtils.getNextFitTime(beginDate, System.currentTimeMillis());
                    tableStatus.setNextNoticeMaintainTimes(next + "");
                }
            }
        }
        else {
            tableStatus.setNextNoticeMaintainTimes("");
        }
        if (!beginMaintainTimes.equalsIgnoreCase(tableStatus.getNextNoticeMaintainTimes())) {
            tableStatus.saveNotAddCnt(); // 不相等需要更新
        }
        return false;
    }

    public static boolean checkMaintainInfoToOne(TableStatus tableStatus, String userId) {
        MaintainInfo maintainInfo = LocalCache.getAppStatus();
        if (maintainInfo != null) {
            long beginDate = maintainInfo.getBeginDate();
            if (System.currentTimeMillis() >= beginDate) {
                noticeStartMainInfo(tableStatus.getInningId(), tableStatus.getIndexCount(), Lists.newArrayList(userId),
                        tableStatus.getTableId());
                return true;
            }
        }
        return false;
    }

    private static void noticeMainInfo(String  inningId, String indexCount, Collection<String> userIds, String msg, String tableId) {
        JSONObject object2 = new JSONObject();
        object2.put("inner_id", inningId);
        object2.put("inner_cnt", indexCount);
        object2.put("msg", msg);
        notifyUsers(object2, GameEventType.NOTICE, userIds, UUID.randomUUID().toString().replace("-", ""), tableId);
    }

    private static void noticeStartMainInfo(String  inningId, String indexCount, Collection<String> userIds, String tableId) {
        JSONObject object2 = new JSONObject();
        object2.put("inner_id", inningId == null ? "" : inningId);
        object2.put("inner_cnt", indexCount == null ? "" : indexCount);
        object2.put("info", "系统正在维护");
        notifyUsers(object2, GameEventType.MAINTAIN, userIds, UUID.randomUUID().toString().replace("-", ""), tableId);
    }
}
