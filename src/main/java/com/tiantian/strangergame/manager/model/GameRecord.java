package com.tiantian.strangergame.manager.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class GameRecord {
    private String gameType;
    private String tableId;
    private String innerId;
    private long startTime;
    private int dealerNumber;
    private int smallBlind;
    private int bigBlind;
    private int utgNumber;
    private int tableType;
    private List<RecordUser> recordUsers = new ArrayList<>();
    private List<Progress> progresses = new ArrayList<>();
    private Map<String, String> showCardsSits = new HashMap<>();
    public static class Progress {
        private String opration;
        private String oprator;
        private String value;
        private long time;

        public static Progress create(String opration, String oprator,  String value,  long time) {
            Progress progress = new Progress();
            progress.setOpration(opration);
            progress.setOprator(oprator);
            progress.setValue(value);
            progress.setTime(time);
            return progress;
        }

        public String getOpration() {
            return opration;
        }

        public void setOpration(String opration) {
            this.opration = opration;
        }

        public String getOprator() {
            return oprator;
        }

        public void setOprator(String oprator) {
            this.oprator = oprator;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public long getTime() {
            return time;
        }

        public void setTime(long time) {
            this.time = time;
        }
    }
    public static class RecordUser {
        private String id;
        private String nickName;
        private String avatarUrl;
        private String sitNumber;
        private long score;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getSitNumber() {
            return sitNumber;
        }

        public void setSitNumber(String sitNumber) {
            this.sitNumber = sitNumber;
        }

        public long getScore() {
            return score;
        }

        public void setScore(long score) {
            this.score = score;
        }
    }

    public String getGameType() {
        return gameType;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public int getDealerNumber() {
        return dealerNumber;
    }

    public void setDealerNumber(int dealerNumber) {
        this.dealerNumber = dealerNumber;
    }

    public int getSmallBlind() {
        return smallBlind;
    }

    public void setSmallBlind(int smallBlind) {
        this.smallBlind = smallBlind;
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public void setBigBlind(int bigBlind) {
        this.bigBlind = bigBlind;
    }

    public int getUtgNumber() {
        return utgNumber;
    }

    public void setUtgNumber(int utgNumber) {
        this.utgNumber = utgNumber;
    }

    public int getTableType() {
        return tableType;
    }

    public void setTableType(int tableType) {
        this.tableType = tableType;
    }

    public List<RecordUser> getRecordUsers() {
        return recordUsers;
    }

    public void setRecordUsers(List<RecordUser> recordUsers) {
        this.recordUsers = recordUsers;
    }

    public List<Progress> getProgresses() {
        return progresses;
    }

    public void setProgresses(List<Progress> progresses) {
        this.progresses = progresses;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getInnerId() {
        return innerId;
    }

    public void setInnerId(String innerId) {
        this.innerId = innerId;
    }

    public Map<String, String> getShowCardsSits() {
        return showCardsSits;
    }

    public void setShowCardsSits(Map<String, String> showCardsSits) {
        this.showCardsSits = showCardsSits;
    }

    public void addSitShowCardSit(String sitNums, String card) {
        showCardsSits.put(sitNums, card);
    }
}
