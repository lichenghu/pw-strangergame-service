package com.tiantian.strangergame.handlers;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.event.Event;
import com.tiantian.strangergame.handlers.task.*;
import com.tiantian.strangergame.handlers.user.*;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameStatus;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class Handlers {

    public static Handlers INSTANCE = new Handlers();
    private static  Map<String, EventHandler> eventHandlers;
    private static Map<String, EventRetHandler> eventRetHandlers;

    private Handlers() {
        eventHandlers = new ConcurrentHashMap<>();
        eventRetHandlers = new ConcurrentHashMap<>();
        init();
    }

    private void registerHandler(String name, EventHandler handler) {
        eventHandlers.put(name, handler);
    }

    private void registerRetHandler(String name, EventRetHandler handler) {
        eventRetHandlers.put(name, handler);
    }

    private void init() {

        registerHandler(GameStatus.BEGIN.name(), new TableBeginHandler());
        registerHandler(GameStatus.D_AND_B.name(), new TableDAndBHandler());
        registerHandler(GameStatus.PRE_FLOP.name(), new TablePreFlopHandler());
        registerHandler(GameStatus.FLOP.name(), new TableFlopHandler());
        registerHandler(GameStatus.TURN.name(), new TableTurnHandler());
        registerHandler(GameStatus.RIVER.name(), new TableRiverHandler());
        registerHandler(GameStatus.END.name(), new TableEndHandler());
        registerHandler(GameStatus.FINISH.name(), new TableFinishHandler());

        registerHandler(GameConstants.TABLE_STATUS_CHECK, new TableCheckStatusHandler());
        registerHandler(GameConstants.CHECK_CHIPS, new TableCheckChipsHandler());
        registerHandler(GameConstants.TEST_STATUS, new TableTestStatusHandler());

        registerHandler(GameConstants.TEST_BET, new TableTestBetHandler());
        registerHandler(GameConstants.USER_RAISE, new UserRaiseHandler());
        registerHandler(GameConstants.USER_FAST_RAISE, new UserFastRaiseHandler());
        registerHandler(GameConstants.USER_ALLIN, new UserAllinHandler());
        registerHandler(GameConstants.USER_CALL, new UserCallHandler());
        registerHandler(GameConstants.USER_CHECK, new UserCheckHandler());
        registerHandler(GameConstants.USER_FOLD, new UserFoldHandler());
        registerHandler(GameConstants.USER_JOIN, new UserJoinHandler());

        registerHandler(GameConstants.ADD_TIMES, new UserAddTimesHandler());
        registerHandler(GameConstants.SHOW_CARDS, new UserShowCardsHandler());
        registerHandler(GameConstants.TABLE_ONLINE_USER_CHECK, new TableOnlineUserCheckHandler());
        registerHandler(GameConstants.TEST_LAST_USER, new TableLastUserCheckHandler());

        registerHandler(GameConstants.AGREE_BUY_IN, new UserAgreeCreditMoneyHandler());
        registerHandler(GameConstants.APPLY_BUY_IN, new UserApplyCreditHandler());
        registerHandler(GameConstants.USER_BUY_SAFE, new UserBuySafeHandler());
        registerHandler(GameConstants.LEADER_ADD_MONEY, new LeaderAddUserCreditHandler());
        registerHandler(GameConstants.USER_CHANGE_CHIPS, new UserExchangeChipsHandler());
        registerHandler(GameConstants.USER_CHANGE_CHIPS_OK, new UserExchangeChipsOkHandler());



        registerRetHandler(GameConstants.TABLE_INFO, new UserTableInfoHandler());
        registerRetHandler(GameConstants.USER_EXIT, new UserExitHandler());
        registerRetHandler(GameConstants.START_GAME, new UserStartGameHandler());
        registerRetHandler(GameConstants.USER_STAND_UP, new UserStandUpHandler());
        registerRetHandler(GameConstants.USER_STATUS, new UserStatusHandler());
        registerRetHandler(GameConstants.USER_SIT_DOWN, new UserSitDownHandler());
        registerRetHandler(GameConstants.CHANGE_SIT_DOWN_TYPE, new UserChangeSitDownTypeHandler());
        registerRetHandler(GameConstants.CLOSE_GAME, new UserCloseGameHandler());
        registerHandler(GameConstants.EMOJI, new UserEmojiHandler());
    }

    public void execute(Event tableEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String type = tableEvent.event();
        if (StringUtils.isBlank(type)) {
            throw new RuntimeException("event type can not be null");
        }
        EventHandler handler = eventHandlers.get(type);
        if (handler == null) {
            throw new RuntimeException("unregister event type :" + type);
        }
        try {
            handler.handler(tableEvent, self, context, sender);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public Object executeRet(Event tableEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String type = tableEvent.event();
        if (StringUtils.isBlank(type)) {
            throw new RuntimeException("event type can not be null");
        }

        EventRetHandler handler = eventRetHandlers.get(type);
        if (handler == null) {
            throw new RuntimeException("unregister ret event type :" + type);
        }
        try {
            return handler.handler(tableEvent, self, context, sender);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
}
