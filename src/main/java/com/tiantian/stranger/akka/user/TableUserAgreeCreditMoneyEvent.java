package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

import java.io.Serializable;

/**
 *
 */
public class TableUserAgreeCreditMoneyEvent extends TableUserEvent {
    private String tableId;
    private String userId;

    public TableUserAgreeCreditMoneyEvent(String tableId, String userId) {
        this.tableId = tableId;
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "agreeCreditMoney";
    }

    public String getUserId() {
        return userId;
    }

    public static class Response implements Serializable {
        private int status;
        private String message;

        public Response(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
