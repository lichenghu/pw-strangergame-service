package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.user.TableUserStandUpEvent;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.EventRetHandler;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.manager.model.TableUser;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserStandUpHandler implements EventRetHandler<TableUserStandUpEvent> {
    @Override
    public Object handler(TableUserStandUpEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        // 处理玩家站起逻辑
        TableUser tableUser = TableUser.load(userId);
        if (tableUser == null || StringUtils.isBlank(tableUser.getTableId())) {
            return true;
        }
        String betStatus = tableUser.getBetStatus();
        if (GameEventType.ALLIN.equalsIgnoreCase(betStatus)) { // allin 不能站起
            return false;
        }

        String tableId = tableUser.getTableId();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus != null && tableStatus.needShowAllCards() &&
                         StringUtils.isNotBlank(betStatus) && !GameEventType.FOLD.equals(betStatus)) { // 亮牌不能站起
            return false;
        }
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();

        String sitNum = tableUser.getSitNum();
        if(StringUtils.isBlank(sitNum)) {
           return true;
        }
        // 站起
        tableAllUser.userStandUp(tableUser.getSitNum(), tableUser.getUserId());
        tableUser.forceStandUp();

        if(tableStatus != null) {
            // 玩家弃牌
            tableStatus.setSitBetStatusIfExistsNotFlush(sitNum, GameEventType.FOLD);
            tableStatus.saveNotAddCnt();
        }

        if (tableStatus == null) { // 游戏未开始的时候
            // 通知玩家离开
            JSONObject object = new JSONObject();
            object.put("inner_id",  "");
            object.put("inner_cnt",  "");
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(sitNum));
            object.put("reason",  "");
            String id = UUID.randomUUID().toString().replace("-", "");
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUsers(object, GameEventType.STAND_UP, userIds, id, tableId);
            return true;
        }
        // 通知玩家离开
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("uid", userId);
        object.put("sn", Integer.parseInt(sitNum));
        object.put("reason",  "");
        String id = UUID.randomUUID().toString().replace("-", "");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUsers(object, GameEventType.STAND_UP, userIds, id, tableId);

        if (sitNum.equalsIgnoreCase(tableStatus.getCurrentBet())) {
            GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                    sitNum);
            if (nextStatus != null) {
                tableStatus.roundBetEnd();
                boolean needShow = tableStatus.needShowAllCards();
                boolean hasSafe = false;
                if (needShow) { // 需要显示所有的手牌
                    List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                    JSONObject object2 = new JSONObject();
                    object2.put("inner_id", tableStatus.getInningId());
                    object2.put("inner_cnt", tableStatus.getIndexCount());
                    object2.put("all_cards", cardsList);
                    String id2 = UUID.randomUUID().toString().replace("-", "");
                    GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                    // 保险
                    hasSafe = GameUtils.checkSafe(tableStatus, tableAllUser);
                }
                tableStatus.save();
                // 通知玩家的池信息
                GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                // 触发下一轮的发牌事件
                GameUtils.triggerNextStatusTask(nextStatus, tableId, tableStatus.getInningId(), hasSafe);
                return true;
            }
            //通知下一个玩家下注
            GameUtils.noticeNextUserBet(tableId, tableStatus, null, false, null, tableAllUser, self, context, sender);
        } else {
            //判断是只剩一人没有弃牌
            if(tableStatus.onlyOneNotFold()) {
                tableStatus.roundBetEnd();
                tableStatus.save();
                // 触发下一轮的发牌事件
                GameUtils.triggerNextStatusTask(GameStatus.FINISH, tableId, tableStatus.getInningId());
            } else {
                tableStatus.save();
            }
        }
        return true;
    }
}
