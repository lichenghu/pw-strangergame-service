package com.tiantian.strangergame.cache;

import com.tiantian.strangergame.data.redis.RedisUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class CacheUtils {
    private static final String MAINTAIN_INFO_KEY = "maintain_info:";

    public static List<MaintainInfo> getAllMaintainInfo() {
        List<MaintainInfo> maintainInfos = RedisUtil.getObjectArrays(MAINTAIN_INFO_KEY, MaintainInfo.class);
        if (maintainInfos == null) {
            return new ArrayList<>();
        }
        return maintainInfos;
    }
}
