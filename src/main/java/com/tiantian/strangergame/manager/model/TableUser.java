package com.tiantian.strangergame.manager.model;

import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import org.apache.commons.lang.StringUtils;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class TableUser extends ModeBase {
    @FieldIgnore
    private static Map<String, Field> filedMap =  new ConcurrentHashMap<>();
    @FieldIgnore
    private static AtomicBoolean LOAD_FLAG = new AtomicBoolean(false);
    @FieldIgnore
    private String userId;
    private String roomId;
    private String nickName;
    private String avatarUrl;
    private String gender;
    private String tableId;
    private String status;
    private String betStatus;
    private String sitNum;
    private String operateCount;
    private String notOperateCount;
    // 玩家下注思考时间
    private String totalSecs;
    // 上次更新的操作时间戳
    private String lastUpdateTimes;
    // 是否延时 1是 0 否
    private String hasAddTimes;
    // 玩家买入个数 几个买入
    private String buyInCnt;
    // 玩家剩余的保险金
    private String leftSafe;

    private TableUser() {
        super();
    }

    public Map<String, Field> fieldMap(){
        return filedMap;
    }
    protected boolean hasLoad(){
        return LOAD_FLAG.compareAndSet(false, true);
    }
    public static TableUser load(String userId) {
        TableUser tableUser = new TableUser();
        tableUser.userId = userId;
        boolean ret =  tableUser.loadModel();
        if (!ret) { // 已经删除了
            return new TableUser();
        }
        return tableUser;
    }
    public static TableUser init(String userId, String tableId) {
        TableUser tableUser = new TableUser();
        tableUser.userId = userId;
        tableUser.tableId = tableId;
        return tableUser;
    }
    public boolean isNull() {
        return StringUtils.isBlank(tableId);
    }

    public void expire() {
        RedisUtil.expire(key(), 86400);
    }

    public boolean save() {
        if (tableId == null) {
            throw new RuntimeException("tableId can not be null");
        }
       return super.save();
    }

    @Override
    public String key() {
        return GameConstants.USER_GROUP_TABLE_KEY + userId;
    }

    public void delSelf(String inTableId) {
       if (inTableId != null && inTableId.equalsIgnoreCase(this.tableId)) {
           RedisUtil.del(key());
       }
    }

    public void forceStandUp() {
        setSitNum(null);
        setStatus("standing");
        if (StringUtils.isNotBlank(betStatus)) { // 游戏还没开始
            setBetStatus(GameEventType.FOLD);
        }
        setOperateCount("");
        setNotOperateCount("");
        setLastUpdateTimes(System.currentTimeMillis() + "");
        setTotalSecs(GameConstants.BET_DELAYER_TIME / 1000 + "");
        save();
    }

    public boolean isStandUp() {
        return StringUtils.isEmpty(sitNum) || "standing".equalsIgnoreCase(status);
    }

    public void addOneOperateCount() {
        if (StringUtils.isBlank(operateCount)) {
            operateCount = "1";
        }
        operateCount = (Integer.parseInt(operateCount) + 1) + "";
    }

    public void clearNotOperateCount() {
        notOperateCount = "0";
        // 重置15秒
        totalSecs = GameConstants.BET_DELAYER_TIME / 1000 + "";
    }

    public void initTotalSecs() {
        // 重置15秒
        totalSecs = GameConstants.BET_DELAYER_TIME / 1000 + "";
    }

    public void addOneDelayTime() { // 增加一次延时
        totalSecs = 2 * GameConstants.BET_DELAYER_TIME / 1000 + "";
    }

    public String getTableId() {
        return tableId;
    }

//    public void setTableId(String tableId) {
//        this.tableId = tableId;
//     }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
     }

    public String getSitNum() {
        return sitNum;
    }

    public void setSitNum(String sitNum) {
        this.sitNum = sitNum;
     }

    public String getOperateCount() {
        return operateCount;
    }

    public void setOperateCount(String operateCount) {
        this.operateCount = operateCount;
     }

    public String getBetStatus() {
        return betStatus;
    }

    public void setBetStatus(String betStatus) {
        this.betStatus = betStatus;
     }

    public String getNotOperateCount() {
        return notOperateCount;
    }

    public void setNotOperateCount(String notOperateCount) {
        this.notOperateCount = notOperateCount;
     }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
     }

    public String getLastUpdateTimes() {
        return lastUpdateTimes;
    }

    public void setLastUpdateTimes(String lastUpdateTimes) {
        this.lastUpdateTimes = lastUpdateTimes;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getTotalSecs() {
        return totalSecs;
    }

    public void setTotalSecs(String totalSecs) {
        this.totalSecs = totalSecs;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBuyInCnt() {
        return buyInCnt;
    }

    public void setBuyInCnt(String buyInCnt) {
        this.buyInCnt = buyInCnt;
    }

    public String getHasAddTimes() {
        return hasAddTimes;
    }

    public void setHasAddTimes(String hasAddTimes) {
        this.hasAddTimes = hasAddTimes;
    }

    public String getLeftSafe() {
        return leftSafe;
    }

    public void setLeftSafe(String leftSafe) {
        this.leftSafe = leftSafe;
    }
}
