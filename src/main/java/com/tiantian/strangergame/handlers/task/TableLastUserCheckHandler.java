package com.tiantian.strangergame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.stranger.akka.event.TableTaskEvent;
import com.tiantian.stranger.akka.user.TableUserExitEvent;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.handlers.EventHandler;
import com.tiantian.strangergame.handlers.Handlers;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.model.TableAllUser;
import com.tiantian.strangergame.manager.model.TableStatus;
import com.tiantian.strangergame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 *
 */
public class TableLastUserCheckHandler implements EventHandler<TableTaskEvent> {
    static Logger LOG = LoggerFactory.getLogger(TableLastUserCheckHandler.class);
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");//获取桌子玩家人数
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null || tableStatus.isNull() || StringUtils.isBlank(tableStatus.getStatus())) {
            return;
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if(tableAllUser.getJoinTableUserMap() != null) {
            Set<Map.Entry<String, String>> entrySet = tableAllUser.getJoinTableUserMap().entrySet();
            if (entrySet != null && entrySet.size() == 1) {
                // 判断玩家是否在线
                for (Map.Entry<String, String> entry : entrySet) {
                    String userId = entry.getValue();
                    boolean isOnline = RedisUtil.exists(GameConstants.ROUTER_KEY + userId);
                    // 不在线
                    if (!isOnline) {
                        // 退出操作
                        Handlers.INSTANCE.executeRet(new TableUserExitEvent(userId, tableId, null, null), self, context, sender);
                    }
                }
            }
        }
    }
}
