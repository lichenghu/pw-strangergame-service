package com.tiantian.strangergame.manager.model;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tiantian.strangergame.data.redis.RedisUtil;
import com.tiantian.strangergame.manager.constants.GameConstants;
import com.tiantian.strangergame.manager.constants.GameEventType;
import com.tiantian.strangergame.manager.constants.GameStatus;
import com.tiantian.strangergame.manager.texas.Poker;
import com.tiantian.strangergame.manager.texas.PokerManager;
import com.tiantian.strangergame.manager.texas.PokerOuts;
import com.tiantian.strangergame.utils.OddsUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
public class TableStatus extends ModeBase {
    @FieldIgnore
    static Logger LOG = LoggerFactory.getLogger(TableStatus.class);
    @FieldIgnore
    private static Map<String, Field> filedMap =  new ConcurrentHashMap<>();
    @FieldIgnore
    private static AtomicBoolean LOAD_FLAG = new AtomicBoolean(false);
    @FieldIgnore
    private static final String SCRIPT_PATH = "redisscripts/table_status_save.lua";
    @FieldIgnore
    private static String SCRIPT_SHA = "";
    @FieldIgnore
    private String tableId;
    private String version;
    // 玩家状态任务ID
    private String userOnlineTaskId;
    // 每局ID
    private String inningId;
    // 一个买入该买入不能清除
    private String buyIn;
    // 庄家座位号
    private String button;
    // 小盲注 座位号
    private String smallBlindNum;
    // 大盲注 座位号
    private String bigBlindNum;
    //小盲注
    private String smallBlindMoney;
    // 大盲注
    private String bigBlindMoney;
    // 游戏状态
    private String status;
    // 玩家下注状态 {sitNum:status}
    private String sitBetStatus;
    // 玩家上一次下注状态，分池的时候需要 {sitNum:status}
    private String lastSitBetStatus;
    // 当前需要下注人座位号
    private String currentBet;
    // 当前需要下注人座位号的开始时间戳
    private String currentBetTimes;
    // 当前需要下注人最大的人座位号(玩家加注的时候会改变)
    private String maxBetSitNum;
    // 当前需要下注人最大的人座位号所下的筹码(玩家加注的时候会改变)
    private String maxBetSitNumChips;
    // 当前需要下注人最大的人座位号所下增加的筹码
    private String maxBetSitNumAddChips;

    // 玩家当轮下注的记录格式: key(座位号) : value ({userId :xxx ,roundChips:100, totalChips:1000})
    private String usersBetsLog;
    // 玩家下注统计日志 key (座位号) : value 字符串逗号分隔(1.0,2.5,0.5)
    private String userBetRaiseLog;
    // 牌
    private String cards;
    // 玩家的底牌
    private String userCards;
    // 桌面的牌
    private String deskCards;
    // 下注池, key :sitNum , value : bet 的list
    private String betPool;
    // 上一次下注池不持久化到redis
    @FieldIgnore
    private String lastBetPool;
    // 上次更新的操作时间戳
    private String lastUpdateTimes;
    // 更新计数
    private String indexCount;

    private String needDelay;
    // 玩家的 座位号和userId 的map集合 key : sitNum, value userId
    private String sitUserIds;
    // 房主ID
    private String masterId;
    // 游戏是否暂停 1是
    private String stopping;
    // 游戏结束时间
    private String endTimes;
    // 房主是否可以点击开始 1是 0否
    private String canStart;
    private String pwd;
    // 每回合的第一个下注的筹码
    private String firstBetChips = "0"; // 默认值为0
    private String safe; // 是否是保险局 0 否 1是
    //  Map<String, Map<String, Object>> outsMap
    private String outsMapStr; //

    private String userSafeWin; //玩家的保险盈利 Map 格式 key : userId, val : win
    private String nextNoticeMaintainTimes; // 下一次推送时间
    private String maxUsers; // 桌子最多人数

    public static TableStatus load(String tableId) {
        TableStatus tableStatus = new TableStatus();
        tableStatus.tableId = tableId;
        boolean result = tableStatus.loadModel();
        if (!result) {
            return null;
        }
        return tableStatus;
    }

    public static TableStatus init(String tableId) {
        TableStatus tableStatus = new TableStatus();
        tableStatus.tableId = tableId;
        return tableStatus;
    }

    public static TableStatus newNull() {
        return new TableStatus();
    }

    public boolean isNull() {
        return StringUtils.isBlank(tableId);
    }


    protected boolean hasLoad(){
        return LOAD_FLAG.compareAndSet(false, true);
    }
    public boolean save() {
        if (tableId == null) {
            throw new RuntimeException("tableId can not be null");
        }
        setLastUpdateTimes(System.currentTimeMillis() + "");
        if (StringUtils.isBlank(indexCount)) {
            indexCount = "1";
        }
        setIndexCount((Integer.parseInt(indexCount) + 1) + "");
        return super.save();
    }

    public boolean saveNotAddCnt() {
        if (tableId == null) {
            throw new RuntimeException("tableId can not be null");
        }
        setLastUpdateTimes(System.currentTimeMillis() + "");
        return super.save();
    }

    @Override
    public String key() {
        return GameConstants.GROUP_TABLE_STATUS_KEY + tableId;
    }

    private TableStatus() {
        super();
    }

    public Map<String, Field> fieldMap(){
        return filedMap;
    }

    public boolean clearNotFlush() {
        setVersion("1");
        // 设置牌局ID
        setInningId("");
        setSmallBlindNum("");
        setBigBlindNum("");
        // 设置游戏状态为 大小盲注
        setStatus(GameStatus.READY.name());
        setSitBetStatus("");
        setLastSitBetStatus("");
        // 设置当前需要下注人座位号
        setCurrentBet("");
        // 当前需要下注人座位号的开始时间戳
        setCurrentBetTimes("");
        // 设置当前需要下注人最大的人座位号(玩家加注的时候会改变)
        setMaxBetSitNum("");
        // 设置最大下注
        setMaxBetSitNumChips("");

//        setSmallBlindMoney("0");
//
//        setBigBlindMoney("0");

        setBetPool("");

        setUsersBetsLog("");
        // 清空上局的牌
        setCards("");
        // 清空上局玩家的底牌
        setUserCards("");
        // 清空上局桌面的牌
        setDeskCards("");

        setIndexCount("1");
        setNeedDelay("");
        setSitUserIds("");
        setStopping("");
        setCanStart("0"); // 不能开始
        setPwd("");
        setFirstBetChips("0");
        setMaxBetSitNumAddChips("0");
        setUserBetRaiseLog("");
        setUserSafeWin("");
        // 保存到redis
        return true;
//        return save();
    }

    public void addPlayingSits(Collection<String> allSits) {
        if (allSits != null && allSits.size() > 0) {
            Map<String, String> map = new HashMap<>();
            for(String sit : allSits) {
                map.put(sit, "");
            }
            sitBetStatus = JSON.toJSONString(map);
        }

    }

    public void addPlayingSitsAndId(Map<String, String> sitAndUserIdMap) {
        if (sitAndUserIdMap != null) {
            sitUserIds = JSON.toJSONString(sitAndUserIdMap);
        } else {
            sitUserIds = "";
        }
    }

    // 获取可以下注的玩家集合，如果集合的大小为0则表示只有allin和弃牌
    public Set<String> canBetSits() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<String> set = new HashSet<>();
            Set<Map.Entry<String, String>> entries = map.entrySet();
            // 没有弃牌和allin
            for (Map.Entry<String, String> entry : entries) {
                if (!GameEventType.FOLD.equalsIgnoreCase(entry.getValue()) &&
                        !GameEventType.ALLIN.equalsIgnoreCase(entry.getValue())) {
                    set.add(entry.getKey());
                }
            }
            return set;
        }
        return null;
    }

    public void setSitBetStatusNotFlush(String sitNum, String status) {
        Map<String, String> map = null;
        if (StringUtils.isNotBlank(sitBetStatus)) {
            map = JSON.parseObject(sitBetStatus, Map.class);
        } else {
            map = new HashMap<>();
            map.put(sitNum, status);
        }
        map.put(sitNum, status);
        sitBetStatus = JSON.toJSONString(map);
    }

    public void checkDelay() {
        // 没人需要下注了
        if (allAllin() || onlyOneNotAllin() || allFoldOrAllin()) {
            if (StringUtils.isBlank(needDelay)) {
                if (GameStatus.TURN.name().equalsIgnoreCase(status)) {
                    setNeedDelay("1");
                } else if(GameStatus.FLOP.name().equalsIgnoreCase(status)) {
                    setNeedDelay("2");
                }
                else if(GameStatus.PRE_FLOP.name().equalsIgnoreCase(status)) {
                    setNeedDelay("3");
                }
            }
        }
    }

    public void setSitBetStatusIfExistsNotFlush(String sitNum, String status) {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            if (map.containsKey(sitNum)) {
                map.put(sitNum, status);
                sitBetStatus = JSON.toJSONString(map);
            }
        }
    }

    public boolean isPreFlop() {
        // 必须是pre_flop
        return GameStatus.PRE_FLOP.name().equalsIgnoreCase(status);
    }

    // 判断是否有玩家加过注
    public boolean hasUserRaise() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            if (entrySet == null || entrySet.size() == 0) {
                return false;
            }
            for (Map.Entry<String, String> entry : entrySet) {
                String userStatus = entry.getValue();
                if (GameEventType.RAISE.equalsIgnoreCase(userStatus)
                        || GameEventType.ALLIN.equalsIgnoreCase(userStatus)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 判断是否只有一个人没有弃牌
     * @return
     */
    public boolean onlyOneNotFold() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            if(valueCollection.size() == 1) {
                return true;
            }
            int index = 0;
            for (String status : valueCollection) {
                if (GameEventType.FOLD.equalsIgnoreCase(status)) {
                    index ++;
                }
            }
            if (valueCollection.size() > 1 && index == valueCollection.size() - 1) {
                return true;
            }
        }
        return false;
    }

    public boolean onlyOneNotAllin() { // 该结果中有fold 结果也是false
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            int index = 0;
            int allinIndex = 0;
            String notAllinSit = "";
            int notFoldCount = 0; //筛选掉弃牌的人计数
            for (Map.Entry<String, String> entry : entrySet) {
                String userSitNum = entry.getKey();
                String userStatus = entry.getValue();
                if (GameEventType.FOLD.equalsIgnoreCase(userStatus)) {
                    continue;
                }
                notFoldCount++;
                if (GameEventType.RAISE.equalsIgnoreCase(userStatus) ||
                        GameEventType.CHECK.equalsIgnoreCase(userStatus) ||
                        GameEventType.CALL.equalsIgnoreCase(userStatus)) {
                    index ++;
                    notAllinSit = userSitNum;
                }
                if (GameEventType.ALLIN.equalsIgnoreCase(userStatus)) {
                    allinIndex ++;
                }
            }
            if (index == 1 && allinIndex == notFoldCount - 1) {
                // 判断notAllinSit下注
                List<UserBetLog> allUserBetLogs = getUsersBetsLogList();
                UserBetLog notAllinBetLog =  getUserBetLog(notAllinSit);
                if (notAllinBetLog == null) {
                    return false;
                }
                for (UserBetLog  userBetLog : allUserBetLogs) {
                    if (userBetLog.getRoundChips() + userBetLog.getTotalChips() >
                            notAllinBetLog.getRoundChips() + notAllinBetLog.getTotalChips()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    // 判断所有人是否已经allin, 有fold 返回也是false
    public boolean allAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.ALLIN.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public boolean allAllinExceptFold() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.FOLD.equalsIgnoreCase(status)
                        && !GameEventType.ALLIN.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public int notFoldCount() {
        int count = 0;
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.FOLD.equalsIgnoreCase(status)) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean allFoldOrAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.ALLIN.equalsIgnoreCase(status)
                        && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    //没有fold的全部allin
    public boolean notFoldAllAllin() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                // 玩家没有fold 并且没有allin
                if(!GameEventType.FOLD.equalsIgnoreCase(status) &&
                        !GameEventType.ALLIN.equalsIgnoreCase(status)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean allFold() {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Collection<String> valueCollection = map.values();
            for (String status : valueCollection) {
                if (!GameEventType.FOLD.equalsIgnoreCase(status)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public List<String> allFoldSitNumList() {
        List<String> foldSitList = new ArrayList<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String val = mapEntry.getValue();
                if (GameEventType.FOLD.equalsIgnoreCase(val)) {
                    foldSitList.add(mapEntry.getKey());
                }
            }
        }
        return foldSitList;
    }
    // 没有弃牌的玩家userId
    public Set<String> notFoldUserIds() {
        Set<String> set = new HashSet<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            Map<String, String> sitAndUserId = getSitBetUserIdMap();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String val = mapEntry.getValue();
                String sit = mapEntry.getKey();
                if (!GameEventType.FOLD.equalsIgnoreCase(val)) {
                    String userId = sitAndUserId.get(sit);
                    if (StringUtils.isNotBlank(sit)) {
                        set.add(userId);
                    }
                }
            }
        }
        return set;
    }

    // 获取所有未弃牌的座位和底牌信息
    public Map<String, String> allNotFoldSitNumsAndCards() {
        Map<String, String> resultMap = new HashMap<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> userCardsMap =  JSON.parseObject(userCards, Map.class);
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String val = mapEntry.getValue();
                if (!GameEventType.FOLD.equalsIgnoreCase(val)) {
                    String cards = userCardsMap.get(mapEntry.getKey());
                    if (cards != null) {
                        resultMap.put(mapEntry.getKey(), cards);
                    }
                }
            }
        }
        return resultMap;
    }

    // 获取所有的座位和底牌信息
    public Map<String, String> allSitNumsAndCards() {
        Map<String, String> resultMap = new HashMap<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> userCardsMap =  JSON.parseObject(userCards, Map.class);
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String cards = userCardsMap.get(mapEntry.getKey());
                if (cards != null) {
                    resultMap.put(mapEntry.getKey(), cards);
                }
            }
        }
        return resultMap;
    }

    // 玩家下注
    public void userBet(String sitNum, long addChips) {
        if (addChips <= 0) {
            return;
        }
        if (StringUtils.isBlank(usersBetsLog)) {
            return;
        }
        try {
            List<UserBetLog> userBetLogs = getUsersBetsLogList();
            // 判断是否是第一个人在没回合下注
            boolean isFirstBet = checkIsFirstBet(userBetLogs);
            if(isFirstBet) {
                firstBetChips = Math.max(Long.parseLong(bigBlindMoney) , addChips) + "";
            }
            boolean isMaxBets = true;
            UserBetLog userBetLog = null;
            for (UserBetLog $userBetLog : userBetLogs) {
                if ($userBetLog.getSitNum().equalsIgnoreCase(sitNum)) {
                    userBetLog = $userBetLog;
                    break;
                }
            }
            if (userBetLog == null) {
                return;
            }
            long roundChips = userBetLog.getRoundChips();
            userBetLog.setRoundChips(Math.abs(addChips) + roundChips);

            //判断是否是最大下注
            for (UserBetLog $userBetLog : userBetLogs) {
                if ($userBetLog.getRoundChips() >= userBetLog.getRoundChips() &&
                        !$userBetLog.getSitNum().equalsIgnoreCase(userBetLog.getSitNum())) {
                    isMaxBets = false;
                }
            }
            if (isMaxBets) {
                setMaxBetSitNum(sitNum);
                setMaxBetSitNumChips(userBetLog.getRoundChips() + "");
                if (isFirstBet) {
                    setMaxBetSitNumAddChips(maxBetSitNumChips);
                } else {
                    long raise = Long.parseLong(maxBetSitNumChips) - Long.parseLong(firstBetChips);
                    setMaxBetSitNumAddChips(Math.max(Long.parseLong(bigBlindMoney), raise) + "");
                }
            }
            usersBetsLog = JSON.toJSONString(userBetLogs);
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }
    // 直接设置最大下注
    public void setMaxBetInfo(String maxSitNum, long betSitNumChips) {
        setMaxBetSitNum(maxSitNum);
        setMaxBetSitNumChips(betSitNumChips + "");
        setMaxBetSitNumAddChips(maxBetSitNumChips);
    }

    public void updateUserBetRaiseLog(String sitNum, long addChips) {
        // 计算底池
        long total = getTotalPoolMoney();
        Map<String, String> userBetLogMap = null;
        if (userBetRaiseLog != null) {
            userBetLogMap = (Map<String, String>) JSON.parse(userBetRaiseLog);
        }
        if (userBetLogMap == null) {
            userBetLogMap = new HashMap<>();
        }
        if (total > 0 && userBetLogMap != null && StringUtils.isNotBlank(sitNum)) {
            String raiseStr = userBetLogMap.get(sitNum);
            java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
            String result = df.format((double) addChips / (double) total);
            if (StringUtils.isBlank(raiseStr)) {
                raiseStr = result;
            } else {
                raiseStr += ("," + result);
            }
            userBetLogMap.put(sitNum, raiseStr);
            userBetRaiseLog = JSON.toJSONString(userBetLogMap);
        }
    }

    public Map<String, String> userBetRaiseLogMap() {
        Map<String, String> userBetLogMap = null;
        if (userBetRaiseLog != null) {
            userBetLogMap = (Map<String, String>) JSON.parse(userBetRaiseLog);
        } else {
            userBetLogMap = new HashMap<>();
        }
        return userBetLogMap;
    }

    private boolean checkIsFirstBet(List<UserBetLog> userBetLogs) {
        for (UserBetLog $userBetLog : userBetLogs) {
            if ($userBetLog.getRoundChips() > 0)  {
                return false;
            }
        }
        return true;
    }

    public void roundBetEnd() {
        //置空 玩家操作验证
        pwd = "";
        if (StringUtils.isNotBlank(usersBetsLog)) {
            try {

                List<UserBetLog> userBetLogs = getUsersBetsLogList();
                checkPool(userBetLogs);
                for (UserBetLog $userBetLog : userBetLogs) {
                    if ($userBetLog.getRoundChips() > 0) {
                        long total = $userBetLog.getTotalChips();
                        $userBetLog.setTotalChips(total + $userBetLog.getRoundChips());
                        $userBetLog.setRoundChips(0);
                    }
                }
                usersBetsLog = JSON.toJSONString(userBetLogs);
                firstBetChips = "0"; // 重置每次回合的第一个人下注
                maxBetSitNumAddChips = "0";
                // 重置没有 fold allin 玩家的状态
                if (StringUtils.isNotBlank(sitBetStatus)) {
                    // 记录这次的下注状态
                    lastSitBetStatus = sitBetStatus;
                    // 只有一个人没有弃牌,则不用重置状态
                    if (onlyOneNotAllin()) {
                        return;
                    }
                    Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
                    Set<Map.Entry<String, String>> entrySet = map.entrySet();
                    if (entrySet != null) {
                        // 判断是否只有一个人没有allin

                        for(Map.Entry<String, String> entry : entrySet) {
                            String value = entry.getValue();
                            if (!value.equalsIgnoreCase(GameEventType.FOLD)
                                    && !value.equalsIgnoreCase(GameEventType.ALLIN)) {
                                entry.setValue("");
                            }
                        }
                    }
                    sitBetStatus = JSON.toJSONString(map);
                }
            }  catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // 获取最大的前一个加注额度
    public long getMaxPreRaise() {
        if (StringUtils.isBlank(maxBetSitNumAddChips)) {
            return 0;
        }
        return Long.parseLong(maxBetSitNumAddChips);
    }

    public boolean needShowAllCards() {
        // 大于1个以上的人全部allin，或者 只有一个人没allin其他的都allin(大于1)或弃牌
        if (notFoldAllAllin() || onlyOneNotAllin()) {
            // 判断没有弃牌的人数大于1
            if (notFoldCount() > 1) {
                return true;
            }
        }
        return false;
    }

    public List<Map<String, Object>> userNotFoldCards() {
        Map<String, String> allUserCards = getUsersCards();
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            for (Map.Entry<String, String> entry : entrySet) {
                String sitNum = entry.getKey();
                String status = entry.getValue();
                if (!GameEventType.FOLD.equalsIgnoreCase(status)) {
                    String cards = allUserCards.get(sitNum);
                    Map<String, Object> oneCards = new HashMap<>();
                    oneCards.put("sn", Integer.parseInt(sitNum));
                    oneCards.put("hand_cards", cards);
                    resultList.add(oneCards);
                }
            }
        }
        return resultList;
    }
    // 分池
    @Deprecated
    public void splitPool(List<UserBetLog> userBetLogs) {
        // 先记录当前的分池信息
        lastBetPool = betPool;
        // 只剩一个人没弃牌
        if(onlyOneNotFold()) {
            addToLastPool(userBetLogs);
            return;
        }
        // 需要分池有人allin
        if (checkSplitPool(userBetLogs)) {
            TreeMap<String, Long> allBetsMap = getUserRoundChips(userBetLogs);
            if(allBetsMap.size() == 0) {
                return;
            }
            List<Map.Entry<String, Long>> entryList = getAllBetsEntryList(allBetsMap);
            while (entryList.size() > 0) {
                Map.Entry<String, Long> betEntry = entryList.remove(0);
                Long userChips = betEntry.getValue();
                if (userChips == null || userChips.longValue() <= 0) {
                    continue;
                }
            }
        }


    }
    // 把玩家下注合并到最后一个池子里面
    private void addToLastPool(List<UserBetLog> userBetLogs) {
        List<Map<String, String>> mapList = getBetPoolMapList();
        if (mapList == null) {
            mapList = new ArrayList<>();
        }
        Map<String, String> nearMap = null;
        boolean needAddToList = false;
        if (mapList.size() > 0) {
            nearMap = mapList.get(mapList.size() - 1);
        }
        else {
            nearMap = new HashMap<>();
            needAddToList = true;
        }
        // 玩家下注日志
        for (UserBetLog ubl : userBetLogs) {
            if (ubl.getRoundChips() <= 0) {
                continue;
            }
            String sitNum = ubl.getSitNum();
            String userBet = nearMap.get(sitNum);
            if (StringUtils.isNotBlank(userBet)) {
                userBet = (Long.parseLong(userBet) + ubl.getRoundChips()) + "";
            }
            else {
                userBet = ubl.getRoundChips() + "";
            }
            nearMap.put(sitNum, userBet);
        }
        if (needAddToList) {
            mapList.add(nearMap);
        }
        betPool = JSON.toJSONString(mapList);
    }

    // 判断是否需要分池
    private boolean checkSplitPool(List<UserBetLog> userBetLogs) {
        Map<String, String> betStatusMap = getSitBetStatusMap();
        for (UserBetLog $userBetLog : userBetLogs) {
            String sitNum = $userBetLog.getSitNum();
            if (StringUtils.isBlank(sitNum)) {
                continue;
            }
            String status = betStatusMap.get(sitNum);
            if(GameEventType.ALLIN.equalsIgnoreCase(status)) {
                return true;
            }
        }
        return false;
    }

    private TreeMap<String, Long> getUserRoundChips(List<UserBetLog> userBetLogs) {
        TreeMap<String, Long> allBetsMap = new TreeMap<>();
        for (UserBetLog $userBetLog : userBetLogs) {
            long roundChips = $userBetLog.getRoundChips();
            if (roundChips > 0) {
                String sitNum = $userBetLog.getSitNum();
                if (StringUtils.isBlank(sitNum)) {
                    continue;
                }
                allBetsMap.put(sitNum, roundChips);
            }
        }
        return allBetsMap;
    }

    private List<Map.Entry<String, Long>> getAllBetsEntryList(TreeMap<String, Long> allBetsMap) {
        List<Map.Entry<String, Long>> allBetsList = new ArrayList(allBetsMap.entrySet());
        // 根据value从小到大排序
        Collections.sort(allBetsList, (o1, o2) -> {
            long result = o1.getValue() - o2.getValue();
            if(result > 0) {
                return 1;
            } else if (result < 0) {
                return -1;
            } else {
                return 0;
            }
        });
        return allBetsList;
    }

    //TODO 大盲注和小盲注问题，分池时候allin 小于一个小忙注问题
    public void checkPool(List<UserBetLog> userBetLogs) {
        // 先记录当前的分池信息
        lastBetPool = betPool;
        String tmpLastBetPool = lastBetPool;
        // 只剩一个人没弃牌
        if(onlyOneNotFold()) {
            oneNotFoldCheckPool(userBetLogs);
            return;
        }

        if (StringUtils.isBlank(sitBetStatus)) {
            return;
        }
        Map<String, String> betsStatusMap = JSON.parseObject(sitBetStatus, Map.class);
        boolean hasAllin = false;
        Map<String, String> allBetsMap = new TreeMap<>();
        for (UserBetLog $userBetLog : userBetLogs) {
            long roundChips = $userBetLog.getRoundChips();
            if (roundChips > 0) {
                String sitNum = $userBetLog.getSitNum();
                String status = betsStatusMap.get(sitNum);
                // 1, 5 弃牌  2,40   3,30 allin 4 40 根
                if(GameEventType.ALLIN.equalsIgnoreCase(status)) {
                    hasAllin = true;
                }
                allBetsMap.put(sitNum, roundChips + "");
            }
        }
        if(allBetsMap.size() == 0) {
            return;
        }
        // 分池
//        if(hasAllin) {
        if (allBetsMap.size() > 0) {
            List<Map.Entry<String, String>> allBetsList = new ArrayList(allBetsMap.entrySet());
            // 根据value从小到大排序
            Collections.sort(allBetsList, (o1, o2) -> {
                long result = Long.parseLong(o1.getValue()) - Long.parseLong(o2.getValue());
                if(result > 0) {
                    return 1;
                } else if (result < 0) {
                    return -1;
                } else {
                    return 0;
                }
            });
            List<Map<String, String>> poolList = null;
            if (StringUtils.isBlank(betPool)) {
                poolList = new ArrayList<>();
            } else {
                poolList = JSON.parseObject(betPool, List.class);
            }

            // 每次下注回合结束后第一次分池是否需要合并
            boolean needMerger = false;
            Map<String, String> tmpPoolMap = new HashMap<>();
            while (allBetsList.size() > 0) {
                // 最新的一个池子
                Map<String, String> lastPoolMap = null;
                if (poolList.size() > 0) {
                    lastPoolMap = poolList.get(poolList.size() - 1);
                }
                // 判断本次分池是否需要和上一个池子合并
                needMerger = checkNewPoolMerger(lastPoolMap, allBetsList);

                int betListSize = allBetsList.size();
                Map.Entry<String, String> betEntry = allBetsList.remove(0);
                if (betListSize > 0) {
                    long beginPool = Long.parseLong(betEntry.getValue());
                    if (beginPool <= 0) {
                        continue;
                    }
                    Map<String, String> map = new HashMap<>();
                    //先放入自己
                    map.put(betEntry.getKey(), betEntry.getValue());
                    for (Map.Entry<String, String> userBetEntry : allBetsList) {
                        long userBet = Long.parseLong(userBetEntry.getValue());
                        map.put(userBetEntry.getKey(), beginPool + "");
                        userBetEntry.setValue((userBet - beginPool) + "");
                    }

                    // 把当前分出来的筹码累加入到临时的池子
                    for(Map.Entry<String, String> entry : map.entrySet()) {
                        String sitNum = entry.getKey();
                        long bet =  Long.parseLong(entry.getValue());
                        if (bet > 0) {
                            String tmpBet = tmpPoolMap.get(sitNum);
                            String newBet = bet + "";
                            if (StringUtils.isNotBlank(tmpBet)) {
                                newBet = (bet + Long.parseLong(tmpBet)) + "";
                            }
                            tmpPoolMap.put(sitNum, newBet);
                        }
                    }
                    if (!needMerger) { // 不需要把池子merger到上一个回合的池中
                        Map<String ,String> copySplitPoolMap = new HashMap<>();
                        copySplitPoolMap.putAll(tmpPoolMap);
                        poolList.add(copySplitPoolMap);
                    }
                    else {
                        // 把当前分池合并到上个的最后一个池子
                        for(Map.Entry<String, String> entry : tmpPoolMap.entrySet()) {
                            long mapVal = Long.parseLong(lastPoolMap.get(entry.getKey()));
                            if (mapVal > 0) {
                                mapVal +=  Long.parseLong(entry.getValue());
                            }
                            lastPoolMap.put(entry.getKey(), mapVal + "");
                        }
                    }
                    //清除掉数据
                    tmpPoolMap.clear();
                }
            }
            if (tmpPoolMap.size() > 0) {
                poolList.add(tmpPoolMap);
            }
            betPool = JSON.toJSONString(poolList);
        }
    }

    // 确认底池合并
    private boolean checkPoolMerger(Map<String, String> lastBetPoolMap) {
        if (StringUtils.isBlank(lastSitBetStatus)) {
            return true;
        }
        // 上一次玩家下注
        Map<String, String> lastSitBetStatusMap = JSON.parseObject(lastSitBetStatus, Map.class);
        Set<String> sitNums = lastBetPoolMap.keySet();
        for (String sitNum : sitNums) {
            String status = lastSitBetStatusMap.get(sitNum);
            // 上次的池子里面有allin的本次分池不能合并到上次池子里
            if (GameEventType.ALLIN.equalsIgnoreCase(status)) {
                return false;
            }
        }
        return true;
    }

    // 确认底池合并,上次池子里面下注的人数量和这次是否相同
    private boolean checkNewPoolMerger(Map<String, String> lastBetPoolMap,
                                       List<Map.Entry<String, String>> currentBetList) {

        if (lastBetPoolMap == null) {
            // 上次池子没有人下注，表示第一次，不需要有合并
            return false;
        }
        // 当前还有下注筹码的人数
        int betCnt = 0;
        int currentBetCnt = 0;
        for (Map.Entry<String, String> entry : currentBetList) {
            String sit = entry.getKey();
            String status = getSitBetStatusBySitNum(sit);
            // 排除弃牌的
            if (GameEventType.FOLD.equalsIgnoreCase(status)) { // 筛选掉弃牌
                continue;
            }
            currentBetCnt ++;
        }

        Set<Map.Entry<String, String>> entrySet = lastBetPoolMap.entrySet();
        for (Map.Entry<String, String> betEntry : entrySet) {
            String bet = betEntry.getValue();
            String sit = betEntry.getKey();
            String status = getSitBetStatusBySitNum(sit);

            // 排除弃牌的
            if (GameEventType.FOLD.equalsIgnoreCase(status)) {
                continue;
            }
            if (!"0".equalsIgnoreCase(bet)) {
                betCnt++;
            }
        }
        // 这次池子里面下注的人数和上次池子里面的人数相等则需要合并
        if (betCnt == currentBetCnt) {
            return true;
        }
        return false;
    }

    public void oneNotFoldCheckPool(List<UserBetLog> userBetLogs) {
        List<Map<String, String>> mapList = null;
        if(StringUtils.isNotBlank(betPool)) {
            mapList = JSON.parseObject(betPool, List.class);
        } else {
            mapList = new ArrayList<>();
        }
        Map<String, String> bottomBetMap = null;
        if (mapList.size() > 0) {
            int index = mapList.size() - 1;
            bottomBetMap = mapList.get(index);
            for (UserBetLog userBetLog : userBetLogs) {
                if (userBetLog.getRoundChips() > 0) {
                    Long userBet = Long.parseLong(bottomBetMap.get(userBetLog.getSitNum()));
                    if (userBet != null) {
                        userBet += userBetLog.getRoundChips();
                    }
                    bottomBetMap.put(userBetLog.getSitNum(), userBet + "");
                }
            }
        } else {
            bottomBetMap = new HashMap<>();
            for (UserBetLog userBetLog : userBetLogs) {
                if (userBetLog.getRoundChips() > 0) {
                    bottomBetMap.put(userBetLog.getSitNum(), userBetLog.getRoundChips() + "");
                }
            }
            mapList.add(bottomBetMap);
        }
        betPool = JSON.toJSONString(mapList);
    }

    public Map<String, String> getUsersCards() {
        Map<String, String> mapCards = new HashMap<>();
        if (StringUtils.isNotBlank(userCards)) {
            mapCards = JSON.parseObject(userCards, Map.class);
        }
        return mapCards;
    }

    public String getUserHandCards(String sitNum) {
        Map<String, String> usersCards = getUsersCards();
        if (usersCards != null) {
            return usersCards.get(sitNum);
        }
        return null;
    }

    public List<String> getDeskCardList() {
        if (StringUtils.isNotBlank(deskCards)) {
            return JSON.parseArray(deskCards, String.class);
        }
        return null;
    }

    public List<Poker> getLeftCardList() {
        if (StringUtils.isNotBlank(cards)) {
            return JSON.parseArray(cards, Poker.class);
        }
        return null;
    }

    public List<Long> betPoolList() {
        List<Long> list = new ArrayList<>();
        if (StringUtils.isNotBlank(betPool)) {
            List<Map<String, String>> mapList = JSON.parseObject(betPool, List.class);
            for (Map<String, String> map : mapList) {
                Collection<String> values =  map.values();
                long totalVal = 0;
                for (String value : values) {
                    totalVal += Long.parseLong(value);
                }
                list.add(totalVal);
            }
        }
        return list;
    }

    public List<Map<String, String>> getBetPoolMapList() {
        if (StringUtils.isNotBlank(betPool)) {
            return JSON.parseObject(betPool, List.class);
        }
        return null;
    }

    public UserBetLog getUserBetLog(String sitNum) {
        if (StringUtils.isBlank(sitNum)) {
            return null;
        }
        List<UserBetLog> userBetLogs = getUsersBetsLogList();
        for (UserBetLog userBetLog : userBetLogs) {
            if (userBetLog.getSitNum().equalsIgnoreCase(sitNum)) {
                return userBetLog;
            }
        }
        return null;
    }

    public void delSelf() {
        RedisUtil.del(key());
    }

    public List<UserBetLog> getUsersBetsLogList() {
        if (StringUtils.isNotBlank(usersBetsLog)) {
            return JSON.parseArray(usersBetsLog, UserBetLog.class);
        }
        return new ArrayList<>();
    }

    private String getFastRaise(long userLeftChips, long needAdd) {
        // 没人加过注玩家可以， 三倍大盲, 五倍大盲
        if (isPreFlop() && !hasUserRaise()) {
            return "110000"; //最后一位表示是否加过注0 否 1是
        }
        long totalPoolMoney = getTotalPoolMoney();
        long halfPool = totalPoolMoney / 2;
        long mod1 = halfPool % Long.parseLong(bigBlindMoney);
        if(mod1 != 0) {
            halfPool = halfPool - mod1 + Long.parseLong(bigBlindMoney);
        }
        String fastRaise = "00";
        if (halfPool > userLeftChips || needAdd > halfPool){
            fastRaise += "0";
        }
        else {
            fastRaise += "1";
        }
        long twoThirdsPool = (totalPoolMoney * 2) / 3;
        long mod2 = twoThirdsPool % Long.parseLong(bigBlindMoney);
        if(mod2 != 0) {
            twoThirdsPool = twoThirdsPool - mod2 + Long.parseLong(bigBlindMoney);
        }
        if (twoThirdsPool > userLeftChips || needAdd > twoThirdsPool){
            fastRaise += "0";
        }
        else {
            fastRaise += "1";
        }
        long onePool = totalPoolMoney;
        if (onePool > userLeftChips || needAdd > onePool){
            fastRaise += "0";
        }
        else {
            fastRaise += "1";
        }
        fastRaise += "1";
        return fastRaise;
    }

    // 获取玩家能够就行的操作 四位操作符 1 表示可以 0 表示不可以  弃牌,看牌,跟注,加注,全押
    public String[] getUserCanOps(String userId, String currentBetSitNum) {
        UserChips userChips = UserChips.load(userId, tableId);
        if (userChips == null) {
            return new String[] { "00000", "0", "0", "0", "000000"};
        }
        long userLeftChips = userChips.getChips();
        // 玩家可以进行的操作
        long maxBetSitChips = Long.parseLong(maxBetSitNumChips);
        //TODO 5.14 修改最小大盲注问题 注释下面代码
        // 每局开始的时候maxBetSitChips都是为0，所以这里要做一下判断
//        if (maxBetSitChips != 0) {
//            maxBetSitChips = Math.max(Long.parseLong(bigBlindMoney), maxBetSitChips);
//        }
        // 计算已经下注额
        long userAlreadyBet = 0;
        // 设置call的值
        UserBetLog userBetLog = getUserBetLog(currentBetSitNum);
        if (userBetLog != null) {
            userAlreadyBet = userBetLog.getRoundChips();
        }
        long callChips = maxBetSitChips - userAlreadyBet;
        long bigBlind = Long.parseLong(bigBlindMoney);

        //最大加注额等于一个大盲注，则下一个加注人的最少加注为2个大盲注
        long minRaise = 0;
        if (maxBetSitChips == bigBlind) {
            minRaise = maxBetSitChips * 2;
        }
        // 玩家已经下的筹码
        long userAddChips = maxBetSitChips - callChips;

        //大盲注在pre-flop下注
        if(GameStatus.PRE_FLOP.name().equalsIgnoreCase(status)) {
            // 当前下注的是
            if (currentBetSitNum.equalsIgnoreCase(maxBetSitNum)
                    && maxBetSitNum.equalsIgnoreCase(bigBlindNum)) {
                String fastRaise = getFastRaise(userLeftChips, Long.parseLong(bigBlindMoney));
                return new String[] {"11011",  bigBlindMoney, "0", userChips.getChips() + "", fastRaise};
            }
        }
        if (maxBetSitChips == 0 ) {// 没有任何人下注
            // 最小加注
            minRaise = bigBlind;
            long needAdd = minRaise - userAddChips;
            String fastRaise = getFastRaise(userLeftChips, needAdd);
            // TODO print日志
            if (needAdd % Long.parseLong(smallBlindMoney) != 0) {
                System.out.println("===errprint:==1==" + JSON.toJSONString(this));
            }
            return new String[] {"11011", Math.max(Long.parseLong(bigBlindMoney), needAdd) + "", "0", userChips.getChips() + "", fastRaise};
        }
        if (maxBetSitChips >= userLeftChips + userAlreadyBet) {// 最大下注大于或等于用户剩下的筹码与以下筹码之和，玩家只能放弃和allin
            return new String[] { "10001", "0", "0", userChips.getChips() + "", "000001"};
        }
        if (minRaise == 0) {
            //  minRaise = maxBetSitChips * 2 - bigBlind;
            minRaise = maxBetSitChips + getMaxPreRaise();
        }

        // 判断当前玩家是否是最后一个没有allin的 玩家只需要跟注
        if (checkOnlyCurrentNotAllin(currentBetSitNum)) {
            if (callChips == 0) {
                return new String[]{"11001", "0",  "0", userChips.getChips() + "", "000001"};
            }
            return new String[]{"10100", "0", callChips + "", userChips.getChips() + "", "000001"};
        }

        if (minRaise > userLeftChips) { // 玩家剩余筹码小于最小加注，玩家只能是跟注或者allin
            if (callChips == 0) {
                return new String[]{"11001", "0",  "0", userChips.getChips() + "", "000001"};
            }
            return new String[] { "10101", "0", callChips + "", userChips.getChips() + "", "000001"};
        }

        long needAdd = minRaise - userAddChips;
        String fastRaise = getFastRaise(userLeftChips, needAdd);
        // TODO print日志
        if (needAdd % Long.parseLong(smallBlindMoney) != 0) {
            System.out.println("===errprint:===2=" + JSON.toJSONString(this));
        }
        if (callChips == 0) {
            return new String[]{"11011", Math.max(Long.parseLong(bigBlindMoney), needAdd)  + "", "0", userChips.getChips() + "", fastRaise};
        }
        return new String[]{"10111", Math.max(Long.parseLong(bigBlindMoney), needAdd)  + "", callChips + "", userChips.getChips() + "", fastRaise};
    }

    // 判断是否只是该玩家没有allin
    private boolean checkOnlyCurrentNotAllin(String sitNum) {
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            String userStatus = map.get(sitNum);
            if (!GameEventType.FOLD.equalsIgnoreCase(userStatus)) {
                Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
                for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                    String userSitNum = mapEntry.getKey();
                    String val = mapEntry.getValue();
                    // 发现除了当前玩家以外的人没有allin
                    if (!GameEventType.FOLD.equalsIgnoreCase(val) &&
                            !userSitNum.equalsIgnoreCase(sitNum)
                            && !GameEventType.ALLIN.equalsIgnoreCase(val)) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public long getTotalPoolMoney() {
        List<UserBetLog> userBetLogList = getUsersBetsLogList();
        if (userBetLogList != null && userBetLogList.size() > 0) {
            long totalPool = 0;
            for (UserBetLog userBetLog : userBetLogList) {
                if (userBetLog != null) {
                    long total = userBetLog.getTotalChips();
                    long round = userBetLog.getRoundChips();
                    totalPool += (total + round);
                }
            }
            return totalPool;
        }
        return 0;
    }

    public List<Map<String, Object>> allUserBetStatus() {
        List<Map<String, Object >> resultList = new ArrayList<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> entrySet = map.entrySet();
            boolean needShow = needShowAllCards();
            for (Map.Entry<String, String> entry : entrySet) {
                String sitNum = entry.getKey();
                String status = entry.getValue();
                Map<String, Object> $map = new HashMap<>();
                $map.put("sn", Integer.valueOf(sitNum));
                String otherUserCards = "";
                if (needShow && StringUtils.isNotBlank(status) && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                    otherUserCards = getUserHandCards(sitNum);
                }
                $map.put("hand_cards", otherUserCards);
                $map.put("status", status);
                resultList.add($map);
            }
        }
        return resultList;
    }

    // 返还玩家的筹码
    public void rebatesChips() {
        List<UserBetLog> userBetLogList = getUsersBetsLogList();
        if (userBetLogList != null && userBetLogList.size() > 0) {
            for (UserBetLog userBetLog : userBetLogList) {
                long totalChips = userBetLog.getTotalChips() + userBetLog.getRoundChips();
                String userId = userBetLog.getUserId();
                UserChips userChips = UserChips.load(userId, tableId);
                if (userChips == null) {
                    String key = GameConstants.USER_GROUP_GAME_KEY + userId;
                    RedisUtil.setMap(key, tableId, JSON.toJSONString(new UserChips(userId, tableId, totalChips)));
                } else {
                    userChips.addAndFlushChips(totalChips);
                }
            }
            usersBetsLog = "";
        }
    }

    public Map<String, String> getSitBetStatusMap() {
        if (StringUtils.isBlank(sitBetStatus)) {
            return null;
        }
        return JSON.parseObject(sitBetStatus, Map.class);
    }

    public Map<String, String> getSitBetUserIdMap() {
        if (StringUtils.isBlank(sitUserIds)) {
            return null;
        }
        return JSON.parseObject(sitUserIds, Map.class);
    }

    public String getSitBetStatusBySitNum(String sitNum) {
        Map<String, String> sitBetMap = getSitBetStatusMap();
        if (sitBetMap != null) {
            return sitBetMap.get(sitNum);
        }
        return null;
    }

    public String randomPwd() {
        String id = UUID.randomUUID().toString().replace("-", "");
        pwd = id;
        return id;
    }

    private boolean checkPwd(String userPwd) {
        if (StringUtils.isNotBlank(pwd) && StringUtils.isNotBlank(userPwd)) {
            return pwd.equalsIgnoreCase(userPwd);
        }
        return false;
    }

    private boolean checkSit(String userSit) {
        if (StringUtils.isNotBlank(currentBet) && StringUtils.isNotBlank(userSit)) {
            return currentBet.equalsIgnoreCase(userSit);
        }
        return false;
    }

    public boolean checkPwdAndSit(String userPwd, String userSit) {
        return checkPwd(userPwd) && checkSit(userSit);
    }

    public void stopGame() {
        setStopping("1");
    }

    public void startGame() {
        setStopping("");
    }

    public boolean isStopping() {
        return "1".equalsIgnoreCase(stopping);
    }

    public String getInningId() {
        if (inningId == null) {
            return "";
        }
        return inningId;
    }

    public Map<String, String> allFoldSitAndUserIds() {
        Map<String, String> retMap = new HashMap<>();
        if (StringUtils.isNotBlank(sitBetStatus)) {
            Map<String, String> map = JSON.parseObject(sitBetStatus, Map.class);
            Set<Map.Entry<String, String>> mapEntrySet = map.entrySet();
            Map<String, String> sitAndUserId = getSitBetUserIdMap();
            for (Map.Entry<String, String> mapEntry : mapEntrySet) {
                String val = mapEntry.getValue();
                String sit = mapEntry.getKey();
                if (GameEventType.FOLD.equalsIgnoreCase(val)) {
                    String userId = sitAndUserId.get(sit);
                    if (StringUtils.isNotBlank(sit)) {
                        retMap.put(sit, userId);
                    }
                }
            }
        }
        return retMap;
    }

    public boolean hasSafe() { // 是否有保险功能
        return "1".equalsIgnoreCase(safe);
    }

    // 判断是否需要进行买保险
    // Map<String, Map<String, Object>>
    public Object[] buySafe() {
        if (!hasSafe()) {
            return null;
        }
        if (!needShowAllCards()) {
            return null;
        }
        if (GameStatus.RIVER.name().equalsIgnoreCase(status)) { // 已经是最后一圈了
            return null;
        }
        // 判断最大玩家牌
        Map<String, String> sitAndCards = allNotFoldSitNumsAndCards();
        List<Map.Entry<String, PokerOuts>> entryList = PokerManager.getWinnerPokerOutsList(sitAndCards, getDeskCardList());
        if (entryList.size() == sitAndCards.size()) { //都是最大的不用买保险
            Map<String, Object> noticeMap = Maps.newHashMap();
            noticeMap.put("type", "no_outs");
            noticeMap.put("sitNums", Lists.newArrayList(sitAndCards.keySet()));
            return new Object[]{noticeMap, null};
        }
        // 根据池子来算
        List<Map<String, String>> betPoolMapList = getBetPoolMapList();
        Map<String, Map<String, Object>> outsMap = Maps.newHashMap();
        List<String> winSits = Lists.newArrayList();
        for (Map.Entry<String, PokerOuts> entry : entryList) {
            String sitNum = entry.getKey();
            winSits.add(sitNum);
        }
        int outsCnt = 0;
        for (Map.Entry<String, PokerOuts> entry : entryList) {
             List<Map<String, String>> usersBetPools = Lists.newArrayList();
             String sitNum = entry.getKey();
             for (Map<String, String> perPoolMap : betPoolMapList) {
                  // 当前玩家在这个池子里并且池子里面的玩家最少2人
                  if (perPoolMap.containsKey(sitNum) && perPoolMap.size() > 1) {
                      usersBetPools.add(perPoolMap);
                  }
             }
             System.out.println("usersBetPools====" + JSON.toJSONString(usersBetPools));
             System.out.println("outsMap====" + JSON.toJSONString(outsMap));
             outsCnt = calculateOuts(usersBetPools, sitNum, outsMap, winSits);
        }
        if(outsCnt ==  0) { // 永远比最小的大
           Map<String, Object> noticeMap = Maps.newHashMap();
           noticeMap.put("type", "drawing_dead");
           noticeMap.put("sitNums", winSits);
           return new Object[]{noticeMap, null};
        }
        if (outsCnt > 15) {
            Map<String, Object> noticeMap = Maps.newHashMap();
            noticeMap.put("type", "outs_gt_15");
            noticeMap.put("sitNums", winSits);
            return new Object[]{noticeMap, null};
        }
        outsMapStr = JSON.toJSONString(outsMap);
        //TODO
        setCurrentBet("");
        setCurrentBetTimes("");

        //判断延时
        if (GameStatus.FLOP.name().equalsIgnoreCase(status)) {
            setNeedDelay("1");
        } else if(GameStatus.TURN.name().equalsIgnoreCase(status)) {
            setNeedDelay("");
        }
        return new Object[]{null, outsMap};
    }

    private int calculateOuts(List<Map<String, String>> usersBetPools, String sitNum,
                              Map<String, Map<String, Object>> outsMap, List<String> winSits) {
        long oddsVal1 = 0;
        long input1 = 0 ; // 得到保投入
        long oddsVal2 = 0;
        long input2 = 0; // 得到均衡
        long oddsVal3 = 0;
        long input3 = 0; // 得到盈利
        Set<String> allOuts = new HashSet<>();
        for (Map<String, String> betMap : usersBetPools) {
             Set<Map.Entry<String, String>> betEntrySet = betMap.entrySet();
             Map<String, String> smallPokerOuts = Maps.newHashMap();
             long totalPerPoolBets = 0;
             long currentUserBet = 0; // 当前的池子里面玩家的下注
             for (Map.Entry<String, String> entry : betEntrySet) {
                  String sn = entry.getKey();
                  Long bet = Long.parseLong(entry.getValue());
                  if (sitNum.equalsIgnoreCase(sn)) {
                      currentUserBet = bet;
                  }
                  // 玩家没弃牌
                 String status = getSitBetStatusBySitNum(sn);
                 // 排除弃牌的
                 if (StringUtils.isNotBlank(status) && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                     smallPokerOuts.put(sn, getUserHandCards(sn));
                 }
                 totalPerPoolBets += bet;
             }
             Set<String> currentPoolUserSit = betMap.keySet(); //当前池子里坐下玩家座位号
             if(winSits.containsAll(currentPoolUserSit)) {
                continue; // 都是最大的 当前池子不能买保险
             }
            // 获取当前池子有几个最大牌的玩家
             int poolWinsSize = 0;
             for (String sn : currentPoolUserSit) {
                  if (winSits.contains(sn)) {
                      poolWinsSize ++;
                  }
             }
             poolWinsSize = Math.min(1, poolWinsSize);
             System.out.println("totalPerPoolBets:" + totalPerPoolBets);
             System.out.println("smallPokerOuts:" + JSON.toJSONString(smallPokerOuts));
             String userCards = getUserHandCards(sitNum);

             Collection<String> smallUserCards = smallPokerOuts.values();
             List<String> allSmallCards = Lists.newArrayList(smallUserCards); // copy一份
             for (String otherCards : smallUserCards) {
                  Set<String> outsSet = PokerManager.calculateOuts(userCards, otherCards, getDeskCardList(),
                          allSmallCards);
                  allOuts.addAll(outsSet);
             }
             System.out.println("allOuts:" +  JSON.toJSONString(allOuts));
             if(allOuts.size() > 15) {
                return allOuts.size();
             }
             if (allOuts.size() <= 15 && allOuts.size() > 0) { // outs 必须小于15才能买保险
                 // 获取赔率表
                 String index = allOuts.size() + "";
                 float odds = OddsUtils.getOdds(index) - 1.0f;//赔上本金
                 long canGetMaxBets = totalPerPoolBets / poolWinsSize; // 当前玩家能够从池子里面获取到最多筹码, 有其他玩家相同的牌则需要分成N等分,如果当前池子都是一样则不能买保险
                 // 保本
                 long $oddsVal1 = currentUserBet;
                 long $input1 = (long) Math.ceil($oddsVal1 / odds) ; // 得到保投入
                 long $oddsVal2 = ((canGetMaxBets - currentUserBet) / 2 + currentUserBet);
                 long $input2 =  (long) Math.ceil($oddsVal2 / odds); // 得到均衡
                 long $oddsVal3 = canGetMaxBets;
                 long $input3 = (long) Math.ceil($oddsVal3 / odds); // 得到盈利
                 System.out.println("status=======" + status);
                 if (GameStatus.PRE_FLOP.name().equalsIgnoreCase(status)) { // 需要买3张牌
                     $input1 = 3 * $input1;
                     $input2 = 3 * $input2;
                     $input3 = 3 * $input3;
                 }
                 oddsVal1 += ($oddsVal1 + $input1);
                 input1 += $input1;
                 oddsVal2 += ($oddsVal2 + $input2);
                 input2 += $input2;
                 oddsVal3 += ($oddsVal3 + $input3);
                 input3 += $input3;
                 System.out.println("$oddsVal1:" + $oddsVal1);
                 System.out.println("$oddsVal2:" + $oddsVal2);
                 System.out.println("$oddsVal3:" + $oddsVal3);
                 System.out.println("$input1:" + $input1);
                 System.out.println("$input2:" + $input2);
                 System.out.println("$input3:" + $input3);
             }
        }
       //TODO 计算买保险价格 如果价格大于下注 则不能买保险

        System.out.println("oddsVal1:" + oddsVal1);
        System.out.println("oddsVal2:" + oddsVal2);
        System.out.println("oddsVal3:" + oddsVal3);
        System.out.println("input1:" + input1);
        System.out.println("input2:" + input2);
        System.out.println("input3:" + input3);
        Map<String, Object> userSafeInfo = Maps.newHashMap();
        List<String> outs = Lists.newArrayList(allOuts);
        Collections.sort(outs, (o1, o2) -> {
            String o1Value = o1.substring(1);
            String o2Value = o2.substring(1);
            int ret = o2Value.compareTo(o1Value);
            if (ret == 0) {
                ret = o2.compareTo(o1);
            }
            return ret;
        });
        userSafeInfo.put("game_status", getStatus());
        userSafeInfo.put("id", UUID.randomUUID().toString().replace(",", ""));
        userSafeInfo.put("buy_status", "0"); //0 未购买, 1购买第一档 2购买第二档 3第三档
        userSafeInfo.put("user_op", "0"); //0 未操作, 1已操作,用来判断流程
        userSafeInfo.put("outs", outs);
        userSafeInfo.put("price", new String[] {input1 +"," + oddsVal1, input2 +"," + oddsVal2, input3 + "," + oddsVal3});
        userSafeInfo.put("times", System.currentTimeMillis());
        if(input1 == 0 && input2 == 0 && input3 == 0) {
           return 0;
        }
        outsMap.put(sitNum, userSafeInfo);
        return allOuts.size();
    }

    public Map<String, Map<String, Object>> getOutsMap() {
        if (StringUtils.isBlank(outsMapStr)) {
            return null;
        }
        return (Map<String, Map<String, Object>>)JSON.parseObject(outsMapStr, Map.class);
    }

    public Map<String, Integer> userSafeWinMap() {
        if (StringUtils.isBlank(userSafeWin)) {
            return Maps.newHashMap();
        }
        return ( Map<String, Integer>)JSON.parseObject(userSafeWin, Map.class);
    }

    public void addUserSafeWin(String userId, int win) {
        if (StringUtils.isBlank(userId) || win <= 0) {
            return;
        }
        Map<String, Integer> winMap = userSafeWinMap();
        if (winMap.containsKey(userId)) {
            int oldWin = winMap.get(userId);
            winMap.put(userId, oldWin + win);
        }
        else {
            winMap.put(userId, win);
        }
        userSafeWin = JSON.toJSONString(winMap);
    }

    public String getTableId() {
        return tableId;
    }

    public void setInningId(String inningId) {
        this.inningId = inningId;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    public String getSmallBlindNum() {
        return smallBlindNum;
    }

    public void setSmallBlindNum(String smallBlindNum) {
        this.smallBlindNum = smallBlindNum;
    }

    public String getBigBlindNum() {
        return bigBlindNum;
    }

    public void setBigBlindNum(String bigBlindNum) {
        this.bigBlindNum = bigBlindNum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentBet() {
        return currentBet;
    }

    public void setCurrentBet(String currentBet) {
        this.currentBet = currentBet;
    }

    public String getMaxBetSitNum() {
        return maxBetSitNum;
    }

    public void setMaxBetSitNum(String maxBetSitNum) {
        this.maxBetSitNum = maxBetSitNum;
    }

    public String getCards() {
        return cards;
    }

    public void setCards(String cards) {
        this.cards = cards;
    }

    public String getUserCards() {
        return userCards;
    }

    public void setUserCards(String userCards) {
        this.userCards = userCards;
    }

    public String getDeskCards() {
        return deskCards;
    }

    public void setDeskCards(String deskCards) {
        this.deskCards = deskCards;
    }

    public String getLastUpdateTimes() {
        return lastUpdateTimes;
    }

    public void setLastUpdateTimes(String lastUpdateTimes) {
        this.lastUpdateTimes = lastUpdateTimes;
    }

    public String getMaxBetSitNumChips() {
        return maxBetSitNumChips;
    }

    public void setMaxBetSitNumChips(String maxBetSitNumChips) {
        this.maxBetSitNumChips = maxBetSitNumChips;
    }

    public String getCurrentBetTimes() {
        return currentBetTimes;
    }

    public void setCurrentBetTimes(String currentBetTimes) {
        this.currentBetTimes = currentBetTimes;
    }

    public String getSitBetStatus() {
        return sitBetStatus;
    }

    public void setSitBetStatus(String sitBetStatus) {
        this.sitBetStatus = sitBetStatus;
    }

    public String getBigBlindMoney() {
        return bigBlindMoney;
    }

    public void setBigBlindMoney(String bigBlindMoney) {
        this.bigBlindMoney = bigBlindMoney;
    }

    public String getSmallBlindMoney() {
        return smallBlindMoney;
    }

    public void setSmallBlindMoney(String smallBlindMoney) {
        this.smallBlindMoney = smallBlindMoney;
    }

    public String getUsersBetsLog() {
        return usersBetsLog;
    }

    public void setUsersBetsLog(String usersBetsLog) {
        this.usersBetsLog = usersBetsLog;
    }

    public String getBetPool() {
        return betPool;
    }

    public void setBetPool(String betPool) {
        this.betPool = betPool;
    }

    public String getIndexCount() {
        if (StringUtils.isBlank(indexCount)) {
            indexCount = "1";
        }
        return indexCount;
    }

    public void setIndexCount(String indexCount) {
        this.indexCount = indexCount;
    }

    public String getLastBetPool() {
        return lastBetPool;
    }

    public void setLastBetPool(String lastBetPool) {
        this.lastBetPool = lastBetPool;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLastSitBetStatus() {
        return lastSitBetStatus;
    }

    public void setLastSitBetStatus(String lastSitBetStatus) {
        this.lastSitBetStatus = lastSitBetStatus;
    }

    public String getUserOnlineTaskId() {
        return userOnlineTaskId;
    }

    public void setUserOnlineTaskId(String userOnlineTaskId) {
        this.userOnlineTaskId = userOnlineTaskId;
    }

    public String getNeedDelay() {
        return needDelay;
    }

    public void setNeedDelay(String needDelay) {
        this.needDelay = needDelay;
    }

    public String getBuyIn() {
        return buyIn;
    }

    public void setBuyIn(String buyIn) {
        this.buyIn = buyIn;
    }

    public String getSitUserIds() {
        return sitUserIds;
    }

    public void setSitUserIds(String sitUserIds) {
        this.sitUserIds = sitUserIds;
    }

    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getStopping() {
        return stopping;
    }

    public void setStopping(String stopping) {
        this.stopping = stopping;
    }

    public String getEndTimes() {
        return endTimes;
    }

    public void setEndTimes(String endTimes) {
        this.endTimes = endTimes;
    }

    public String getCanStart() {
        return canStart;
    }

    public void setCanStart(String canStart) {
        this.canStart = canStart;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getFirstBetChips() {
        return firstBetChips;
    }

    public void setFirstBetChips(String firstBetChips) {
        this.firstBetChips = firstBetChips;
    }

    public String getMaxBetSitNumAddChips() {
        return maxBetSitNumAddChips;
    }

    public void setMaxBetSitNumAddChips(String maxBetSitNumAddChips) {
        this.maxBetSitNumAddChips = maxBetSitNumAddChips;
    }

    public String getUserBetRaiseLog() {
        return userBetRaiseLog;
    }

    public void setUserBetRaiseLog(String userBetRaiseLog) {
        this.userBetRaiseLog = userBetRaiseLog;
    }

    public String getSafe() {
        return safe;
    }

    public void setSafe(String safe) {
        this.safe = safe;
    }

    public String getOutsMapStr() {
        return outsMapStr;
    }

    public void setOutsMapStr(String outsMapStr) {
        this.outsMapStr = outsMapStr;
    }

    public String getUserSafeWin() {
        return userSafeWin;
    }

    public void setUserSafeWin(String userSafeWin) {
        this.userSafeWin = userSafeWin;
    }

    public String getNextNoticeMaintainTimes() {
        return nextNoticeMaintainTimes;
    }

    public void setNextNoticeMaintainTimes(String nextNoticeMaintainTimes) {
        this.nextNoticeMaintainTimes = nextNoticeMaintainTimes;
    }

    public String getMaxUsers() {
        return maxUsers;
    }

    public void setMaxUsers(String maxUsers) {
        this.maxUsers = maxUsers;
    }
}
