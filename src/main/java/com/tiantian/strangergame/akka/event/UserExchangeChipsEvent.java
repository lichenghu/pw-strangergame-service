package com.tiantian.strangergame.akka.event;

import akka.actor.ActorRef;

/**
 *
 */
public class UserExchangeChipsEvent implements UserEvent {
    private String userId;
    private String tableId;
    private long buyInCnt;
    private long oneBuyIn;
    private ActorRef clientSender;

    public UserExchangeChipsEvent(String userId, String tableId, long buyInCnt, long oneBuyIn, ActorRef clientSender) {
        this.userId = userId;
        this.tableId = tableId;
        this.buyInCnt = buyInCnt;
        this.oneBuyIn = oneBuyIn;
        this.clientSender = clientSender;
    }

    @Override
    public String userId() {
        return userId;
    }

    public ActorRef getClientSender() {
        return clientSender;
    }

    public String getTableId() {
        return tableId;
    }

    public long getBuyInCnt() {
        return buyInCnt;
    }

    public long getOneBuyIn() {
        return oneBuyIn;
    }
}
