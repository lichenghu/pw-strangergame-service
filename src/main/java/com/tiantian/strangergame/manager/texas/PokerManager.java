package com.tiantian.strangergame.manager.texas;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class PokerManager {
    /**
     * 随机产生牌
     * @return
     */
    public static List<Poker> initPokers() {
        List<Poker> pokerList = createNewPokers(null);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
//        Collections.shuffle(pokerList);
        for(int a = 0; a < 256 ; a++) {
            Random r = new Random();
            int rint1 = r.nextInt(52);
            int rint2 = r.nextInt(52);
            Collections.swap(pokerList, rint1, rint2);
        }
        return pokerList;
    }

    private static List<Poker> createNewPokers(List<String> excepts) {

        List<Poker> pokerList = new ArrayList<>();
        for (int i = 2; i <= 53; i++) {
            Poker poker = Poker.create(i);
            if (excepts != null && excepts.size() > 0 && excepts.contains(poker.getShortPoker())) {
                continue;
            }
            pokerList.add(poker);
        }
        return pokerList;
    }

    public static List<Map.Entry<String, PokerOuts>> getWinnerPokerOutsList( Map<String, String> userCards,  List<String> deskCards) {
        Set<Map.Entry<String, String>> mapSet = userCards.entrySet();
        Map<String, PokerOuts> outsMap = new HashMap<>();
        for (Map.Entry<String, String> entry : mapSet) {
             String sitNum = entry.getKey();
             String[] cards = entry.getValue().split(",");
             List<Poker> pokerList = new ArrayList<>();
             Poker poker1 = Poker.create(cards[0]);
             pokerList.add(poker1);
             Poker poker2 = Poker.create(cards[1]);
             pokerList.add(poker2);
             if (deskCards != null && deskCards.size() > 0) {
                 for (String card : deskCards) {
                      Poker poker = Poker.create(card);
                      pokerList.add(poker);
                 }
             }
             PokerOuts pokerOuts = loadPokerOuts(pokerList);
             outsMap.put(sitNum, pokerOuts);
        }
        // 排序
        List<Map.Entry<String, PokerOuts>> list = new ArrayList<>(outsMap.entrySet());
        // 倒序
        Collections.sort(list, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));

        Map.Entry<String, PokerOuts> tmpEntry = null;
        List<Map.Entry<String, PokerOuts>> returnList = new ArrayList<>();
        boolean isOver = false;
        while (list.size() > 0 && !isOver) {
            Map.Entry<String, PokerOuts> entry = list.remove(0);
            if (tmpEntry != null) {
                if (entry.getValue().compareTo(tmpEntry.getValue()) == 0) {
                    returnList.add(entry);
                    tmpEntry = entry;
                } else {
                    isOver = true;
                }
            } else {
                returnList.add(entry);
                tmpEntry = entry;
            }
        }
        return returnList;
    }

    public static Set<String> calculateOuts(String userCards, String otherUserCards, List<String> deskCards,
                                            Collection<String> notFoldCards) {
        if (deskCards == null) {
            deskCards = Lists.newArrayList();
        }
        List<String> exceptCards = Lists.newArrayList();
        if(deskCards.size() > 0) {
           for(String cardsStr : deskCards) {
               String[] cards = cardsStr.split(",");
               for (String card : cards) {
                    if (StringUtils.isNotBlank(card)) {
                        exceptCards.add(card);
                    }
               }
           }
        }
        if(notFoldCards != null && notFoldCards.size() > 0) {
           for(String cardsStr : notFoldCards) {
               String[] cards = cardsStr.split(",");
               for (String card : cards) {
                    if (StringUtils.isNotBlank(card)) {
                        exceptCards.add(card);
                    }
               }
           }
        }
        List<Poker> leftPokers = createNewPokers(exceptCards);
        Set<String> outsSet = new HashSet<>();
        for (Poker leftCard : leftPokers) {
             deskCards.add(leftCard.getShortPoker());
             PokerOuts outs1 = getUserPokerOuts(userCards, deskCards);
             PokerOuts outs2 = getUserPokerOuts(otherUserCards, deskCards);
             if (outs1.compareTo(outs2) < 0) {
                 outsSet.add(leftCard.getShortPoker());
             }
             deskCards.remove(deskCards.size() - 1); // 移除掉最后一个
        }
        return outsSet;
    }

//    public static void main(String[] args) {
//        String user = "S2,S13";
//        String others = "C2,C13";
//        Set<String> set = calculateOuts(user, others, null, Lists.newArrayList(user, others));
//        List<String> outs = Lists.newArrayList(set);
//        Collections.sort(outs, (o1, o2) -> {
//            String o1Value = o1.substring(1);
//            String o2Value = o2.substring(1);
//            int ret = o2Value.compareTo(o1Value);
//            if (ret == 0) {
//                ret = o2.compareTo(o1);
//            }
//            return ret;
//        });
//        System.out.println(JSON.toJSONString(outs));
//    }

    public static PokerOuts getUserPokerOuts(String userCards, List<String> deskCards) {
            String[] cards = userCards.split(",");
            List<Poker> pokerList = new ArrayList<>();
            Poker poker1 = Poker.create(cards[0]);
            pokerList.add(poker1);
            Poker poker2 = Poker.create(cards[1]);
            pokerList.add(poker2);
            if (deskCards != null && deskCards.size() > 0) {
                for (String card : deskCards) {
                    Poker poker = Poker.create(card);
                    pokerList.add(poker);
                }
            }
            return loadPokerOuts(pokerList);
    }

    public static Map<String, PokerOuts> getUsersPokerOutsList(Map<String, String> userCards, List<String> deskCards) {
        Set<Map.Entry<String, String>> mapSet = userCards.entrySet();
        Map<String, PokerOuts> outsMap = new HashMap<>();
        for (Map.Entry<String, String> entry : mapSet) {
            String sitNum = entry.getKey();
            String[] cards = entry.getValue().split(",");
            List<Poker> pokerList = new ArrayList<>();
            Poker poker1 = Poker.create(cards[0]);
            pokerList.add(poker1);
            Poker poker2 = Poker.create(cards[1]);
            pokerList.add(poker2);
            if (deskCards != null && deskCards.size() > 0) {
                for (String card : deskCards) {
                    Poker poker = Poker.create(card);
                    pokerList.add(poker);
                }
            }
            PokerOuts pokerOuts = loadPokerOuts(pokerList);
            outsMap.put(sitNum, pokerOuts);
        }
        return outsMap;
    }

    public static PokerOuts getPokerOuts(String cardStr, List<String> deskCards) {
        if (StringUtils.isBlank(cardStr)) {
            return null;
        }
        String[] cards = cardStr.split(",");
        List<Poker> pokerList = new ArrayList<>();
        Poker poker1 = Poker.create(cards[0]);
        pokerList.add(poker1);
        Poker poker2 = Poker.create(cards[1]);
        pokerList.add(poker2);
        if (deskCards != null && deskCards.size() > 0) {
            for (String card : deskCards) {
                Poker poker = Poker.create(card);
                pokerList.add(poker);
            }
        }
        return loadPokerOuts(pokerList);
    }

    private static PokerOuts loadPokerOuts(List<Poker> pokerList) {
        PokerRecognizer recognizer = new PokerRecognizer();
        recognizer.load(pokerList);
        PokerOuts outs = recognizer.getMaxOuts();
        return outs;
    }

    public static void main(String[] args) {
        Map<String, String> map = Maps.newHashMap();
        map.put("1", "D4,H5");
        map.put("2","S7,D11");
        List list = getWinnerPokerOutsList(map, Lists.newArrayList("D3", "C8", "S5", "C10"));
        System.out.println(list.size());
    }
}
