package com.tiantian.strangergame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.stranger.akka.user.TableUserApplyCreditEvent;
import com.tiantian.strangergame.akka.event.UserApplyCreditEvent;
import com.tiantian.strangergame.handlers.EventHandler;

/**
 *
 */
public class UserApplyCreditHandler implements EventHandler<TableUserApplyCreditEvent> {
    @Override
    public void handler(TableUserApplyCreditEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
       String tableId = event.tableId();
       String userId = event.getUserId();
       long buyIn =  event.getBuyIn();
       long maxBuyIn = event.getMaxBuyIn();
       String masterId = event.getMasterId();
       self.tell(new UserApplyCreditEvent(tableId, userId, buyIn, maxBuyIn, sender, masterId), ActorRef.noSender());
    }
}
