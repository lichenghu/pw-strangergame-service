package com.tiantian.stranger.akka.serialzers;

import akka.serialization.JSerializer;
import org.msgpack.MessagePack;

import java.io.IOException;

/**
 *
 */
public class MsgpackSerialzer extends JSerializer {
    private static final MessagePack messagePack = new MessagePack();

    @Override
    public Object fromBinaryJava(byte[] bytes, Class<?> manifest) {
        try {
            return messagePack.read(bytes, manifest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int identifier() {
        return 19861026;
    }

    @Override
    public byte[] toBinary(Object o) {
        // Serialize
        try {
            return messagePack.write(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    @Override
    public boolean includeManifest() {
        return true;
    }
}
