package com.tiantian.stranger.akka.user;

import com.tiantian.stranger.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserStatusEvent extends TableUserEvent {
    
    private String tableId;
    public TableUserStatusEvent() {
    }
    public TableUserStatusEvent(String tableId) {
        super(true);
        this.tableId = tableId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userStatus";
    }

    public String getTableId() {
        return tableId;
    }
}
